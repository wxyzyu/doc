## [下载Nodejs](https://nodejs.org/en/download/)

![image-20200902180005331](Centos7%E5%AE%89%E8%A3%85Nodejs.assets/image-20200902180005331.png) 

```shell
shell> wget https://nodejs.org/dist/v12.18.3/node-v12.18.3-linux-x64.tar.xz
```

## 安装Nodejs

```shell
shell> xz -d node-v12.18.3-linux-x64.tar.xz
shell> tar -xf node-v12.18.3-linux-x64.tar
shell> cd node-v12.18.3-linux-x64/
shell> su
shell> ln -s /opt/module/node-v12.18.3-linux-x64/bin/node /usr/bin/node
shell> ln -s /opt/module/node-v12.18.3-linux-x64/bin/npm /usr/bin/npm
```

## 设置npm国内镜像

```
shell> npm config get registry
shell> npm config set registry https://registry.npm.taobao.org
```

