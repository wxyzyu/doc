## Linux配置hosts文件

- 配置/etc/hosts,预先添加实验机器地址

```
192.168.8.101   server1
192.168.8.102   server2
192.168.8.103   server3
192.168.8.104   server4
192.168.8.105   server5
192.168.8.106   server6
192.168.8.107   server7
192.168.8.108   server8
192.168.8.109   server9
192.168.8.110   server10
```

- 重启网络

```
service network restart
```





