[TOC]

# 在VMware安装centos 7 并配置固定ip

## 配置静态ip

- 查看自己机器本地网络名称

```
ifconfig
```

![image-20200523131420597](Centos%207%20%E9%85%8D%E7%BD%AE%E5%9B%BA%E5%AE%9AIP.assets/image-20200523131420597.png) 

- 找到网络配置文件

```
ll /etc/sysconfig/network-scripts/
```

![image-20200523131507301](Centos%207%20%E9%85%8D%E7%BD%AE%E5%9B%BA%E5%AE%9AIP.assets/image-20200523131507301.png) 

- 修改配置文件

```
vim /etc/sysconfig/network-scripts/ifcfg-ens33
```

![image-20200523130545175](Centos%207%20%E9%85%8D%E7%BD%AE%E5%9B%BA%E5%AE%9AIP.assets/image-20200523130545175.png) 

- 主要修改一下几项

```properties
#与你本机名称一致
DEVICE=eth33 
TYPE=Ethernet
ONBOOT=yes
#静态ip
BOOTPROTO=static
NAME=ens33
#指定本机ip
IPADDR=192.168.8.101
PREFIX=24
#指定网关，跟虚拟机配置一致
GATEWAY=192.168.8.2
DNS1=114.114.114.114
```

- 重启网络

```
service network restart
```

至此静态ip配置完成，因为是实验机器，所以关闭防火墙，生产环境不要这样做！

## 实验环境配置

- 查看防火墙状态并关闭防火墙

```
firewall-cmd --state
```

- 关闭防火墙

```
systemctl stop firewalld.service
```

- 关闭selinux

```
vi /etc/selinux/config
```

```properties
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disable
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected. 
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

## 验证

- 同内网的机器ping新建的虚拟机

```
ping 192.168.8.101

Pinging 192.168.8.101 with 32 bytes of data:
Reply from 192.168.8.101: bytes=32 time<1ms TTL=64
Reply from 192.168.8.101: bytes=32 time<1ms TTL=64
Reply from 192.168.8.101: bytes=32 time<1ms TTL=64
Reply from 192.168.8.101: bytes=32 time<1ms TTL=64
```

## 问题解决

- 如果是在VMware安装的，需要确定VMware NAT Service已启动
- 如果使用Xshell在启动VMware NAT Service前就尝试连接，启动后可能会依旧连接不上，可以关闭Xshell重开