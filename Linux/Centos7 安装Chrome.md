## 下载

```
https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
```

## 安装

```shell
shell> sudo rpm -ivh google-chrome-stable_current_x86_64.rpm 
warning: google-chrome-stable_current_x86_64.rpm: Header V4 DSA/SHA1 Signature, key ID 7fac5991: NOKEY
error: Failed dependencies:
	libappindicator3.so.1()(64bit) is needed by google-chrome-stable-85.0.4183.102-1.x86_64
	liberation-fonts is needed by google-chrome-stable-85.0.4183.102-1.x86_64
	libvulkan.so.1()(64bit) is needed by google-chrome-stable-85.0.4183.102-1.x86_64
## 依次安装确实依赖
shell> yum provides libappindicator3.so.1
shell> sudo yum install libappindicator-gtk3
shell> sudo rpm -ivh google-chrome-stable_current_x86_64.rpm
shell> 
```



