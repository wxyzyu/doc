## 查看外部是否可以访问ip或者端口

```shell
shell> ping 192.168.8.11
shell telnet 192.168.8.11 3306
```

## 修改防火墙配置

```shell
#查看防火墙规则
shell> firewall-cmd --list-all 
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33
  sources: 
  services: dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
	
#查看端口是否开放
shell> firewall-cmd --query-port=3306/tcp
no
#永久添加可访问端口和协议
shell> firewall-cmd --permanent --add-port=3306/tcp
success
#重启防火墙
shell> firewall-cmd --reload
success

#除去端口
#shell> firewall-cmd --permanent --remove-port=8080/tcp
#shell> firewall-cmd --reload
```

- 也可以直接修改firewall的配置文件

```shell
shell> vi /etc/firewalld/zones/public.xml
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>Public</short>
  <description>For use in public areas. You do not trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.</description>
  <service name="ssh"/>
  <service name="dhcpv6-client"/>
  <port protocol="tcp" port="3306"/>
</zone>

#三种方式重新加载配置
shell> service firewalld restart
shell> firewall-cmd --reload
shell> systemctl  restart firewalld
```
