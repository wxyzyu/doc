## NFS主机配置

```shell
# 安装软件
$ yum install -y nfs-utils

# 配置共享目录
$ vim /etc/exports
/home/nfs/ 192.168.30.0/24(rw,sync,no_root_squash,fsid=0)

# 启动服务
$ systemctl enable rpcbind.service
$ systemctl enable nfs-server.service
$ systemctl start rpcbind.service
$ systemctl start nfs-server.service


$ rpcinfo -p
   program vers proto   port  service
    100000    4   tcp    111  portmapper
    100000    3   tcp    111  portmapper
    100000    2   tcp    111  portmapper
    100000    4   udp    111  portmapper
    100000    3   udp    111  portmapper
    100000    2   udp    111  portmapper
    100005    1   udp  20048  mountd
    100005    1   tcp  20048  mountd
    100005    2   udp  20048  mountd
    100005    2   tcp  20048  mountd
    100024    1   udp  40616  status
    100005    3   udp  20048  mountd
    100005    3   tcp  20048  mountd
    100024    1   tcp  59402  status
    100003    3   tcp   2049  nfs
    100003    4   tcp   2049  nfs
    100227    3   tcp   2049  nfs_acl
    100003    3   udp   2049  nfs
    100003    4   udp   2049  nfs
    100227    3   udp   2049  nfs_acl
    100021    1   udp  52753  nlockmgr
    100021    3   udp  52753  nlockmgr
    100021    4   udp  52753  nlockmgr
    100021    1   tcp  35578  nlockmgr
    100021    3   tcp  35578  nlockmgr
    100021    4   tcp  35578  nlockmgr
# 查看共享目录
$ exportfs -r
$ exportfs
/home/nfs       192.168.30.0/24
```

## NFS从机配置

```shell
# 从机只启动客户端，不要启动服务器
$ yum install -y nfs-utils
$ systemctl enable rpcbind.service
$ systemctl start rpcbind.service

# 检查是否能看到NFS服务器
$ showmount -e 192.168.30.204
Export list for 192.168.30.204:
/home/nfs 192.168.30.0/24

# 挂载
$ mkdir /home/nfs
$ mount -t nfs 192.168.30.204:/home/nfs /home/nfs

$ df -h
```

