## 创建用户

```shell
shell> useradd test
# 创建用户并制定给一个组
shell> useradd -g groupname username
shell> passwd test
Changing password for user test.
New password: 
Retype new password: 
passwd: all authentication tokens updated successfully.
```

## 删除用户

```shell
shell> userdel -r test
```

## 查看系统中所有用户

```shell
# Linux cut命令用于显示每行从开头算起 num1 到 num2 的文字。
# -b ：以字节为单位进行分割。这些字节位置将忽略多字节字符边界，除非也指定了 -n 标志。
# -c ：以字符为单位进行分割。
# -d ：自定义分隔符，默认为制表符。
# -f ：与-d一起使用，指定显示哪个区域。
# -n ：取消分割多字节字符。仅和 -b 标志一起使用。如果字符的最后一个字节落在由 -b 标志的 List 参数指示的
# 范围之内，该字符将被写出；否则，该字符将被排除
shell> cut -d : -f 1 /etc/passwd
```

## 查看可以登录用户

```shell
# grep -v 或 --invert-match : 显示不包含匹配文本的所有行。
shell> cat /etc/passwd | grep -v /sbin/nologin | cut -d : -f 1
```

## 创建组

```shell
shell> groupadd test
```

## 删除组

```shell
shell> groupdel test
```

## 查看可以登录的用户组

```shell
shell> cat /etc/group | grep -v /sbin/nologin | cut -d : -f 1
```

## 将用户添加到一个组中

```shell
shell> usermod -g groupname username
```

## 查看用户的组

```shell
# 不指定username则显示当前登录用户的组信息
shell> groups username
```

## 将文件共享给一个组

```shell
shell> chgrp -R groupname /test/dir/
# 2 打开 setGID 位，意味着新创建的子文件继承与目录相同的组，新创建的子目录继承父目录的 setGID 位
# 7 - 为所有者提供 rwx 权限
# 给组 rwx 权限
# 为其他人提供 rx 权限
shell> chmod -R 2775 /test/dir/
```

## 将用户移出某个组

```bash
shell> gpasswd groupname -d username
```

## NFS挂载失败

```shell
$ showmount -e  192.168.30.204
rpc mount export: RPC: Timed out

# 因为其他节点都正常，切尝试了所有办法也无法修复,重启网络解决问题。。。
$ service network restart
$ showmount -e  192.168.30.204
Export list for 192.168.30.204:
/home/nfs 192.168.30.0/24
```

## IPV4

```shell
# 1.临时开启，（写入内存，在内存中开启）
$ echo "1" > /proc/sys/net/ipv4/ip_forward

# 2.永久开启，（写入内核）
$  vi /etc/sysctl.conf
# sysctl settings are defined through files in
# /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
#
# Vendors settings live in /usr/lib/sysctl.d/.
# To override a whole file, create a new file with the same in
# /etc/sysctl.d/ and put new settings there. To override
# only specific settings, add a file with a lexically later
# name in /etc/sysctl.d/ and put new settings there.
#
# For more information, see sysctl.conf(5) and sysctl.d(5).
net.ipv4.ip_forward = 1

$ sysctl -p
net.ipv4.ip_forward = 1

$ cat /proc/sys/net/ipv4/ip_forward
1
```

