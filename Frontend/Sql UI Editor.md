| 项目             | [jQuery QueryBuilder](https://querybuilder.js.org/index.html) | [react-awesome-query-builder](https://github.com/ukrbublik/react-awesome-query-builder) |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 演示链接         | https://querybuilder.js.org/demo.html                        | https://ukrbublik.github.io/react-awesome-query-builder/     |
| 条件拖动重新分组 | 支持                                                         | 支持                                                         |
| convert from SQL | 支持                                                         | 不支持                                                       |
| 自定义插件       | [支持](https://querybuilder.js.org/dev/plugins.html)         | 不支持                                                       |

