## 问题：namespace状态一直为Terminating，导致无法重新部署

```shell
$ kubectl get ns
NAME              STATUS        AGE
default           Active        44h
ingress-nginx     Active        26h
istio-system      Terminating   22h
knative-serving   Terminating   22h
kube-node-lease   Active        44h
kube-public       Active        44h
kube-system       Active        44h
kuboard           Active        23h

```

## 尝试删除

```shell
$ kubectl delete ns istio-system --force --grace-period=0
warning: Immediate deletion does not wait for confirmation that the running resource has been terminated. The resource may continue to run on the cluster indefinitely.
namespace "istio-system" force deleted

```

## 修改对应namespace的配置

```shell
$ kubectl get ns istio-system -o json > deleteistio.json
$ vi deleteistio.json

    "apiVersion": "v1",
    "kind": "Namespace",
    "metadata": {
        "annotations": {
            "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"v1\",\"kind\":\"Namespace\",\"metadata\":{\"annotations\":{},\"name\":\"istio-system\"}}\n"
        },
        "creationTimestamp": "2021-10-28T08:00:59Z",
        "deletionTimestamp": "2021-10-29T05:20:20Z",
        "labels": {
            "kubernetes.io/metadata.name": "istio-system"
        },
        "name": "istio-system",
        "resourceVersion": "193332",
        "uid": "5889d48c-61bf-4f9f-93ca-7dff1aa7ef52"
    },
    "spec": {
        # 清空
    },
    "status": {
        "conditions": [
            {
                "lastTransitionTime": "2021-10-29T05:20:25Z",
                "message": "Discovery failed for some groups, 1 failing: unable to retrieve the complete list of server APIs: metrics.k8s.io/v1beta1: the server is currently unable to handle the request",
                "reason": "DiscoveryFailed",
                "status": "True",
                "type": "NamespaceDeletionDiscoveryFailure"
            },
            {
                "lastTransitionTime": "2021-10-29T05:20:25Z",
                "message": "All legacy kube types successfully parsed",
                "reason": "ParsedGroupVersions",
                "status": "False",
                "type": "NamespaceDeletionGroupVersionParsingFailure"
            },
            {
                "lastTransitionTime": "2021-10-29T05:20:25Z",
                "message": "All content successfully deleted, may be waiting on finalization",
                "reason": "ContentDeleted",
                "status": "False",
                "type": "NamespaceDeletionContentFailure"
            },
            {
                "lastTransitionTime": "2021-10-29T05:20:25Z",
                "message": "All content successfully removed",
                "reason": "ContentRemoved",
                "status": "False",
                "type": "NamespaceContentRemaining"
            },
            {
                "lastTransitionTime": "2021-10-29T05:20:25Z",
                "message": "All content-preserving finalizers finished",
                "reason": "ContentHasNoFinalizers",
                "status": "False",
                "type": "NamespaceFinalizersRemaining"
            }
        ],
        "phase": "Terminating"
    }
}

$ curl -k -H "Content-Type: application/json" -X PUT --data-binary @deleteistio.json http://127.0.0.1:8080/api/v1/namespaces/istio-system/finalize

curl: (7) Failed connect to 127.0.0.1:8080; Connection refused
# 在新终端启动代理
$ kubectl proxy --port=8081
# 返回当前终端执行以下语句
$ curl -k -H "Content-Type: application/json" -X PUT --data-binary @deleteistio.json http://127.0.0.1:8080/api/v1/namespaces/istio-system/finalize
$ kubectl get ns
NAME              STATUS        AGE
default           Active        44h
ingress-nginx     Active        26h
knative-serving   Terminating   22h
kube-node-lease   Active        44h
kube-public       Active        44h
kube-system       Active        44h
kuboard           Active        24h
```

