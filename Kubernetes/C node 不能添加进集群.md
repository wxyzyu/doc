## kubeadm join 提示不能解析主机

```shell
$ kubeadm join 192.168.30.200:6443 --token tr8ycu.np0bmltxmu73ymqv         --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6  --node-name worknode40
[preflight] Running pre-flight checks
[WARNING Hostname]: hostname "worknode40" could not be reached
[WARNING Hostname]: hostname "worknode40": lookup worknode40 on 222.222.222.222:53: no such host

$ hostnamectl set-hostname worknode40
$ echo worknode40 > /etc/hostname
$ hostname
worknode40

$ vi /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 worknode40
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

```

## kubeadm join 卡住

```shell
$ kubeadm join 192.168.30.200:6443 --token  ozuy9c.hyktc12eei1kkuk3        --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6  --node-name worknode40
[preflight] Running pre-flight checks

$ kubeadm join 192.168.30.200:6443 --token  ozuy9c.hyktc12eei1kkuk3        --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6  --node-name worknode40 --v=5
[preflight] Running pre-flight checks
...

I1029 16:08:47.201435   41145 checks.go:520] running all checks
I1029 16:08:47.328997   41145 checks.go:403] checking whether the given node name is valid and reachable using net.LookupHost
I1029 16:08:47.329252   41145 checks.go:618] validating kubelet version
I1029 16:08:47.389426   41145 checks.go:132] validating if the "kubelet" service is enabled and active
I1029 16:08:47.397807   41145 checks.go:205] validating availability of port 10250
I1029 16:08:47.398019   41145 checks.go:282] validating the existence of file /etc/kubernetes/pki/ca.crt
I1029 16:08:47.398051   41145 checks.go:432] validating if the connectivity type is via proxy or direct
I1029 16:08:47.398128   41145 join.go:475] [preflight] Discovering cluster-info
I1029 16:08:47.398185   41145 token.go:80] [discovery] Created cluster-info discovery client, requesting info from "192.168.30.200:6443"
I1029 16:08:47.420525   41145 token.go:223] [discovery] The cluster-info ConfigMap does not yet contain a JWS signature for token ID "ozuy9c", will try again
I1029 16:08:53.371358   41145 token.go:223] [discovery] The cluster-info ConfigMap does not yet contain a JWS signature for token ID "ozuy9c", will try again
I1029 16:08:59.806634   41145 token.go:223] [discovery] The cluster-info ConfigMap does not yet contain a JWS signature for token ID "ozuy9c", will try again


# 去主节点执行
$ kubeadm token list
# 如果为空则表示token过期
$ kubeadm token create --print-join-command --v=5
tr8ycu.np0bmltxmu73ymqv

$ kubeadm token list
TOKEN                     TTL         EXPIRES                USAGES                   DESCRIPTION                                                EXTRA GROUPS
tr8ycu.np0bmltxmu73ymqv   23h         2021-10-30T08:11:00Z   authentication,signing   <none>                                                     system:bootstrappers:kubeadm:default-node-token

$ kubeadm join 192.168.30.200:6443 --token tr8ycu.np0bmltxmu73ymqv         --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6  --node-name worknode40 --v=5

```

