```shell
kubeadm reset -f \
& rm -rf &HOME/.kube/ \
& yum install -y ipvsadm \
& ipvsadm --clear \
& modprobe -r ipip \
& docker volume rm etcd \
& rm -r /var/etcd/backups/* \
& yum remove -y kubelet kubeadm kubectl \
& rm /run/flannel/subnet.env -f \
& rm -rf /var/etcd \
& rm -rf /var/lib/kubelet/ \
& rm -rf /var/lib/rancher/ \
& rm -rf /run/kubernetes/ \
& rm /var/lib/kubelet/* -rf \
& rm /etc/kubernetes/* -rf \
& rm /var/lib/rancher/* -rf \
& rm /var/lib/etcd/* -rf \
& rm /var/lib/cni/* -rf \
& iptables -F && iptables -t nat -F \
& ip link del flannel.1 \
& ip link del kube-ipvs0 \
& ip link del dummy0 \
& ip link del cni0 \
& service network restart 





yum install -y kubelet kubeadm kubectl \
& sudo systemctl enable --now kubelet


yum --showduplicates list kubeadm |  | sort -r



yum install  kubelet-1.21.7 kubeadm-1.21.7 kubectl-1.21.7  -y

sudo systemctl enable --now kubelet

kubeadm init \
--image-repository registry.cn-hangzhou.aliyuncs.com/google_containers \
--kubernetes-version v1.21.7 \
--service-cidr=10.1.0.0/16 \
--pod-network-cidr=10.244.0.0/16 \
--node-name=master
```

