```shell
$ mkdir -p /etc/systemd/system/docker.service.d
$ vi /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=socks5://serverip:1080"
Environment="HTTPS_PROXY=socks5://serverip:1080"
Environment="NO_PROXY=.svc,.default,.local,.cluster.local,localhost,127.0.0.0,10.1.16.0/24,10.244.0.0/24,.domain.com"

$ systemctl daemon-reload
$ systemctl show --property=Environment docker
Environment=HTTP_PROXY=socks5://serverip:1080 HTTPS_PROXY=socks5://serverip:1080 NO_PROXY=.svc,.default,.local,.cluster.local,localhost,127.0.0.0,10.1.16.0/24,10.244.0.0/24,.domain.com

$ systemctl restart docker

```

