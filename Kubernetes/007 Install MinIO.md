## Install krew

1. Make sure that `git` is installed.

2. Run this command to download and install `krew`:

   ```sh
   (
     set -x; cd "$(mktemp -d)" &&
     OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
     ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
     KREW="krew-${OS}_${ARCH}" &&
     curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
     tar zxvf "${KREW}.tar.gz" &&
     ./"${KREW}" install krew
   )
   ```

3. Add the `$HOME/.krew/bin` directory to your PATH environment variable. To do this, update your `.bashrc` or `.zshrc` file and append the following line:

   ```sh
   export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
   ```

   and restart your shell.

4. Run `kubectl krew` to check the installation.

## Install the MinIO Operator

Run the following commands to install the MinIO Operator and Plugin using the Kubernetes `krew` plugin manager:

```shell
$ kubectl krew update
$ kubectl krew install minio
```

Run the following command to verify installation of the plugin:

```shell
$ kubectl minio version
```

Run the following command to initialize the Operator:

```shell
$ kubectl minio init
```

Run the following command to verify the status of the Operator:

```shell
$ kubectl get pods -n minio-operator
NAME                              READY   STATUS    RESTARTS   AGE
console-c66f79f9f-dmp4c           1/1     Running   0          25m
minio-operator-7588ccd99f-8vmrf   1/1     Running   0          25m
minio-operator-7588ccd99f-c4rcx   1/1     Running   0          25m

```

## Access the Operator Console

Run the following command to create a local proxy to the MinIO Operator Console:

```
kubectl minio proxy -n minio-operator
```

The output resembles the following:

```
kubectl minio proxy
Starting port forward of the Console UI.

To connect open a browser and go to http://localhost:9090

Current JWT to login: TOKENSTRING
```

Open your browser to the provided address and use the JWT token to log in to the Operator Console.