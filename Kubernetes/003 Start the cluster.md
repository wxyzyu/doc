## 禁用防火墙

```
$ systemctl disable firewalld & systemctl stop firewalld
```

## 初始化主节点

```shell
# $ kubeadm init --image-repository registry.cn-hangzhou.aliyuncs.com/google_containers
$ kubeadm init \
--image-repository registry.cn-hangzhou.aliyuncs.com/google_containers \
--kubernetes-version v1.22.3 \
--service-cidr=10.1.0.0/16 \
--pod-network-cidr=10.244.0.0/16 \
--node-name=master

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:


kubeadm join 192.168.30.200:6443 --token ozuy9c.hyktc12eei1kkuk3 \
        --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6
```

```shell
$ echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> ~/.bash_profile
$ source ~/.bash_profile
```



## 添加网络插件

```shell
$ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
$ cat /run/flannel/subnet.env
FLANNEL_NETWORK=10.244.0.0/16
FLANNEL_SUBNET=10.241.0.1/24
FLANNEL_MTU=1450
FLANNEL_IPMASQ=true

```



## 在各个工作节点添加节点到集群

```shell
$ kubeadm join 192.168.30.200:6443 --token ozuy9c.hyktc12eei1kkuk3 \
        --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6  --node-name worknode202

$ kubeadm join 192.168.30.200:6443 --token ozuy9c.hyktc12eei1kkuk3 \
        --discovery-token-ca-cert-hash sha256:c915f8398697d054bb5625816d705c901a84340c5797fcda331d4de3063c05f6  --node-name worknode204
```

## 在主节点查看节点状态

```shell
$ kubectl get nodes
NAME                    STATUS   ROLES                  AGE     VERSION
localhost.localdomain   Ready    control-plane,master   16m     v1.22.2
worknode202             Ready    <none>                 7m44s   v1.22.2
worknode204             Ready    <none>                 7m10s   v1.22.2
```



## 测试

```shell
$ vi deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80

$ kubectl apply -f deployment.yaml

$ kubectl describe deployment nginx-deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Wed, 27 Oct 2021 18:18:28 +0800
Labels:                 <none>
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=nginx
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.14.2
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-66b6c48dd5 (2/2 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  97s   deployment-controller  Scaled up replica set nginx-deployment-66b6c48dd5 to 2

$ kubectl describe replicaset nginx-deployment-66b6c48dd5
Name:           nginx-deployment-66b6c48dd5
Namespace:      default
Selector:       app=nginx,pod-template-hash=66b6c48dd5
Labels:         app=nginx
                pod-template-hash=66b6c48dd5
Annotations:    deployment.kubernetes.io/desired-replicas: 2
                deployment.kubernetes.io/max-replicas: 3
                deployment.kubernetes.io/revision: 1
Controlled By:  Deployment/nginx-deployment
Replicas:       2 current / 2 desired
Pods Status:    2 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginx
           pod-template-hash=66b6c48dd5
  Containers:
   nginx:
    Image:        nginx:1.14.2
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  3m2s  replicaset-controller  Created pod: nginx-deployment-66b6c48dd5-7mlb2
  Normal  SuccessfulCreate  3m2s  replicaset-controller  Created pod: nginx-deployment-66b6c48dd5-g72hm

$ kubectl describe pod nginx-deployment-66b6c48dd5-7mlb2
Name:         nginx-deployment-66b6c48dd5-7mlb2
Namespace:    default
Priority:     0
Node:         worknode202/192.168.30.202
Start Time:   Wed, 27 Oct 2021 18:18:29 +0800
Labels:       app=nginx
              pod-template-hash=66b6c48dd5
Annotations:  <none>
Status:       Running
IP:           10.244.1.2
IPs:
  IP:           10.244.1.2
Controlled By:  ReplicaSet/nginx-deployment-66b6c48dd5
Containers:
  nginx:
    Container ID:   docker://a4ad0c7e2bd49da3886c19654b790aed77611ce9f76f752b672f1da343412482
    Image:          nginx:1.14.2
    Image ID:       docker-pullable://nginx@sha256:f7988fb6c02e0ce69257d9bd9cf37ae20a60f1df7563c3a2a6abe24160306b8d
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Wed, 27 Oct 2021 18:18:48 +0800
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-xtq84 (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  kube-api-access-xtq84:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  3m43s  default-scheduler  Successfully assigned default/nginx-deployment-66b6c48dd5-7mlb2 to worknode202
  Normal  Pulling    3m41s  kubelet            Pulling image "nginx:1.14.2"
  Normal  Pulled     3m23s  kubelet            Successfully pulled image "nginx:1.14.2" in 17.92138492s
  Normal  Created    3m23s  kubelet            Created container nginx
  Normal  Started    3m23s  kubelet            Started container nginx

```

```shell
$ kubectl get services kube-dns --namespace=kube-system
NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
kube-dns   ClusterIP   10.1.0.10    <none>        53/UDP,53/TCP,9153/TCP   23m
$ kubectl run curl --image=radial/busyboxplus:curl -i --tty
[ root@curl:/ ]$ nslookup nginx-deployment
Server:    10.1.0.10
Address 1: 10.1.0.10 kube-dns.kube-system.svc.cluster.local

Name:      nginx-deployment
Address 1: 10.1.113.147 nginx-deployment.default.svc.cluster.local


[ root@curl:/ ]$ curl nginx-deployment.default.svc.cluster.local
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```

```shell
$  kubectl get services nginx-deployment
NAME               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
nginx-deployment   ClusterIP   10.1.113.147   <none>        80/TCP    17m

$ curl 10.1.113.147
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

