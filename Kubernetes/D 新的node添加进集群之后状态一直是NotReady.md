```shell
$ journalctl -f -u kubelet

Oct 29 19:59:54 worknode40 kubelet[18874]: I1029 19:59:54.039634   18874 cni.go:204] "Error validating CNI config list" configList="{\n  \"name\": \"cbr0\",\n  \"cniVersion\": \"0.3.1\",\n  \"plugins\": [\n    {\n      \"type\": \"flannel\",\n      \"delegate\": {\n        \"hairpinMode\": true,\n        \"isDefaultGateway\": true\n      }\n    },\n    {\n      \"type\": \"portmap\",\n      \"capabilities\": {\n        \"portMappings\": true\n      }\n    }\n  ]\n}\n" err="[[/opt/cni/bin/portmap]: not found]"
Oct 29 19:59:54 worknode40 kubelet[18874]: I1029 19:59:54.039679   18874 cni.go:239] "Unable to update cni config" err="no valid networks found in /etc/cni/net.d"


# 可以看到提示文件没有找到

# 从正常节点复制过来这个目录下的文件就可以了
```

