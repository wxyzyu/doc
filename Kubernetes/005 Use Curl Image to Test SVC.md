## Pod定义

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: curl
  namespace: default
spec:
  containers:
  - name: curl
    image: rancher/curl
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
  restartPolicy: Always
```

## 创建Pod

```shell
$ kubectl apply -f curl.yaml
```

## 访问服务

```shell
$ kubectl exec -it  curl -n default -- curl   http://hello-example.default.svc.cluster.local
Hello First!
```



## 也可以使用如下方式

```shell
$ kubectl run curl --image=radial/busyboxplus:curl -i --tty --rm
```

