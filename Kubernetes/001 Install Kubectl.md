## [Install kubectl binary with curl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

1. ### Download the latest release with the command:

   ```bash
   $ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
   100   154  100   154    0     0    205      0 --:--:-- --:--:-- --:--:--   205
   100 44.7M  100 44.7M    0     0  9922k      0  0:00:04  0:00:04 --:--:-- 11.8M
   ```

   > **Note:**
   >
   > To download a specific version, replace the `$(curl -L -s https://dl.k8s.io/release/stable.txt)` portion of the command with the specific version.
   >
   > For example, to download version v1.22.0 on Linux, type:
   >
   > ```bash
   > $ curl -LO https://dl.k8s.io/release/v1.22.0/bin/linux/amd64/kubectl
   > ```

2. ### Validate the binary (optional)

   Download the kubectl checksum file:

   ```bash
   $ curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
   100   154  100   154    0     0    224      0 --:--:-- --:--:-- --:--:--   224
   100    64  100    64    0     0     53      0  0:00:01  0:00:01 --:--:--    53
   ```

   Validate the kubectl binary against the checksum file:

   ```bash
   $ echo "$(<kubectl.sha256) kubectl" | sha256sum --check
   ```

   If valid, the output is:

   ```console
   kubectl: OK
   ```

   If the check fails, `sha256` exits with nonzero status and prints output similar to:

   ```bash
   kubectl: FAILED
   sha256sum: WARNING: 1 computed checksum did NOT match
   ```

   > **Note:** Download the same version of the binary and checksum.

3. ### Install kubectl

   ```bash
   $ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
   ```

   > **Note:**
   >
   > If you do not have root access on the target system, you can still install kubectl to the `~/.local/bin` directory:
   >
   > ```bash
   > $ chmod +x kubectl
   > $ mkdir -p ~/.local/bin/kubectl
   > $ mv ./kubectl ~/.local/bin/kubectl
   > # and then add ~/.local/bin/kubectl to $PATH
   > ```

4. ### Test to ensure the version you installed is up-to-date:

   ```bash
   $ kubectl version --client
   Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:38:50Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
   ```

