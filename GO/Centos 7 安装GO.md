## 	[下载go](https://golang.google.cn/dl/)

```shell
shell> cd /opt/module
shell> wget https://dl.google.com/go/go1.15.2.linux-amd64.tar.gz
```

## 安装go

```shell
shell> tar -zxvf go1.15.2.linux-amd64.tar.gz
shell> cd go
shell> pwd
/opt/module/go
shell> su
shell> vim /etc/profile
# 在最后添加
export GO_HOME=/opt/module/go
export PATH=$PATH:$GO_HOME/bin
shell> exit
shell> source /etc/profile
shell> go version
go version go1.15.2 linux/amd64
```

