## 安装PostgreSQL 12 

- 参考https://www.postgresql.org/download/linux/redhat/

```
# Install the repository RPM:
shell> yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

# Install PostgreSQL:
shell> yum install postgresql12-server

# Optionally initialize the database and enable automatic start:
/usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl enable postgresql-12
systemctl start postgresql-12
```

## 验证

```shell
#PostgreSQL会创建postgres账户，先切换到这个账户
shell> su – postgres
#然后执行psql  等同于 psql -U postgres
shell> psql
psql (12.3)
Type "help" for help.

postgres=# select version();
                                                 version                                                 
---------------------------------------------------------------------------------------------------------
 PostgreSQL 12.3 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-39), 64-bit
(1 row)

postgres=# 
```

## 修改密码

```shell
postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of 
-----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}

postgres=# \password postgres
Enter new password: 
Enter it again: 
postgres=# 
```

## 允许外网访问

```shell
shell> su - postgres
-bash-4.2$ cd /var/lib/pgsql/12/data/
-bash-4.2$ vim postgresql.conf
...
listen_addresses = '*'
#listen_addresses = 'localhost'         # what IP address(es) to listen on;
                                        # comma-separated list of addresses;
                                        # defaults to 'localhost'; use '*' for all
                                        # (change requires restart)
...

-bash-4.2$ vim pg_hba.conf 
...
# "local" is for Unix domain socket connections only
local   all             all                                     password
# IPv4 local connections:
host    all             all             127.0.0.1/32            password
host    all             all             192.168.8.0/24          password
...

-bash-4.2$ exit
shell> service postgresql-12 restart
```

## 防火墙设置

- 参考Centos 7 firewall 配置