## 环境

- Centos 7
- PostgreSQL 12

## 安装pg_jieba

- 编译pg_jieba

- https://github.com/jaiminpan/pg_jieba

```shell
shell> cd /opt/module
shell> git clone https://github.com/jaiminpan/pg_jieba
shell> cd pg_jieba
shell> git submodule update --init --recursive
shell> mkdir build
shell> cd build
shell> cmake ..
-- The C compiler identification is unknown
-- The CXX compiler identification is unknown
CMake Error: your C compiler: "CMAKE_C_COMPILER-NOTFOUND" was not found.   Please set CMAKE_C_COMPILER to a valid compiler path or name.
CMake Error: your CXX compiler: "CMAKE_CXX_COMPILER-NOTFOUND" was not found.   Please set CMAKE_CXX_COMPILER to a valid compiler path or name.
-- Setting pg_jieba build type - 
CMake Warning at /usr/share/cmake/Modules/FindPostgreSQL.cmake:109 (message):
  Couldn't determine PostgreSQL configuration.
Call Stack (most recent call first):
  /usr/share/cmake/Modules/FindPostgreSQL.cmake:125 (fail_if)
  CMakeLists.txt:11 (find_package)


CMake Error at /usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:108 (message):
  Could NOT find PostgreSQL (missing: PostgreSQL_LIBRARY_DIRS
  PostgreSQL_CONFIG_DIR PostgreSQL_INCLUDE_DIRS PostgreSQL_LIBRARIES)
Call Stack (most recent call first):
  /usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:315 (_FPHSA_FAILURE_MESSAGE)
  /usr/share/cmake/Modules/FindPostgreSQL.cmake:173 (find_package_handle_standard_args)
  CMakeLists.txt:11 (find_package)


-- Configuring incomplete, errors occurred!
See also "/opt/module/pgjieba/pg_jieba/build/CMakeFiles/CMakeOutput.log".
See also "/opt/module/pgjieba/pg_jieba/build/CMakeFiles/CMakeError.log".
```

```shell
# 解决报错
shell> sudo yum install centos-release-scl-rh
shell> sudo yum install postgresql12-devel
shell> pg_config
-bash: pg_config: command not found
shell> sudo vim /etc/profile
...
export PATH=$PATH:/usr/pgsql-12/bin
...
source /etc/profile
[wxyzyu@hadoop10 bin]$ pg_config
BINDIR = /usr/pgsql-12/bin
DOCDIR = /usr/pgsql-12/doc
HTMLDIR = /usr/pgsql-12/doc/html
INCLUDEDIR = /usr/pgsql-12/include
PKGINCLUDEDIR = /usr/pgsql-12/include
INCLUDEDIR-SERVER = /usr/pgsql-12/include/server
LIBDIR = /usr/pgsql-12/lib
PKGLIBDIR = /usr/pgsql-12/lib
LOCALEDIR = /usr/pgsql-12/share/locale
MANDIR = /usr/pgsql-12/share/man
SHAREDIR = /usr/pgsql-12/share
SYSCONFDIR = /etc/sysconfig/pgsql
PGXS = /usr/pgsql-12/lib/pgxs/src/makefiles/pgxs.mk
CONFIGURE = '--enable-rpath' '--prefix=/usr/pgsql-12' '--includedir=/usr/pgsql-12/include' '--libdir=/usr/pgsql-12/lib' '--mandir=/usr/pgsql-12/share/man' '--datadir=/usr/pgsql-12/share' '--with-icu' '--with-llvm' '--with-perl' '--with-python' '--with-tcl' '--with-tclconfig=/usr/lib64' '--with-openssl' '--with-pam' '--with-gssapi' '--with-includes=/usr/include' '--with-libraries=/usr/lib64' '--enable-nls' '--enable-dtrace' '--with-uuid=e2fs' '--with-libxml' '--with-libxslt' '--with-ldap' '--with-selinux' '--with-systemd' '--with-system-tzdata=/usr/share/zoneinfo' '--sysconfdir=/etc/sysconfig/pgsql' '--docdir=/usr/pgsql-12/doc' '--htmldir=/usr/pgsql-12/doc/html' 'CFLAGS=-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic' 'LDFLAGS=-Wl,--as-needed' 'LLVM_CONFIG=/usr/lib64/llvm5.0/bin/llvm-config' 'CLANG=/opt/rh/llvm-toolset-7/root/usr/bin/clang' 'PKG_CONFIG_PATH=:/usr/lib64/pkgconfig:/usr/share/pkgconfig' 'PYTHON=/usr/bin/python2'
CC = gcc -std=gnu99
CPPFLAGS = -D_GNU_SOURCE -I/usr/include/libxml2 -I/usr/include
CFLAGS = -Wall -Wmissing-prototypes -Wpointer-arith -Wdeclaration-after-statement -Werror=vla -Wendif-labels -Wmissing-format-attribute -Wformat-security -fno-strict-aliasing -fwrapv -fexcess-precision=standard -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic
CFLAGS_SL = -fPIC
LDFLAGS = -Wl,--as-needed -L/usr/lib64/llvm5.0/lib -L/usr/lib64 -Wl,--as-needed -Wl,-rpath,'/usr/pgsql-12/lib',--enable-new-dtags
LDFLAGS_EX = 
LDFLAGS_SL = 
LIBS = -lpgcommon -lpgport -lpthread -lselinux -lxslt -lxml2 -lpam -lssl -lcrypto -lgssapi_krb5 -lz -lreadline -lrt -lcrypt -ldl -lm 
VERSION = PostgreSQL 12.3
```

```shell
# 解决错误
-- The CXX compiler identification is unknown
CMake Error: your C compiler: "CMAKE_C_COMPILER-NOTFOUND" was not found.   Please set CMAKE_C_COMPILER to a valid compiler path or name.
CMake Error: your CXX compiler: "CMAKE_CXX_COMPILER-NOTFOUND" was not found.   Please set CMAKE_CXX_COMPILER to a valid compiler path or name.
shell> sudo yum install gcc
shell> sudo yum install gcc-c++
```

```shell
shell> cmake  -DCMAKE_PREFIX_PATH="/usr/pgsql-12"  -DCMAKE_CXX_FLAGS="-Wall -std=c++11" ..
-- Setting pg_jieba build type - 
-- Found PostgreSQL: /usr/pgsql-12/lib (found version "12.3") 
-- PostgreSQL include dirs: /usr/pgsql-12/include;/usr/pgsql-12/include/server
-- PostgreSQL library dirs: /usr/pgsql-12/lib
-- PostgreSQL libraries:    pq
-- POSTGRESQL_EXECUTABLE is /usr/pgsql-12/bin/postgres
-- Configuring done
-- Generating done
-- Build files have been written to: /opt/module/pgjieba/pg_jieba/build
shell> make
Scanning dependencies of target pg_jieba
[ 50%] Building C object CMakeFiles/pg_jieba.dir/pg_jieba.c.o
/opt/module/pgjieba/pg_jieba/pg_jieba.c:247:1: warning: ‘DefineCustomConfigVariables’ was used with no prototype before its definition [-Wmissing-prototypes]
 DefineCustomConfigVariables()
 ^
[100%] Building CXX object CMakeFiles/pg_jieba.dir/jieba.cpp.o
Linking CXX shared library pg_jieba.so
[100%] Built target pg_jieba
shell> sudo make install
[100%] Built target pg_jieba
Install the project...
-- Install configuration: ""
-- Installing: /usr/pgsql-12/lib/pg_jieba.so
-- Installing: /usr/pgsql-12/share/extension/pg_jieba.control
-- Installing: /usr/pgsql-12/share/extension/pg_jieba--1.1.1.sql
-- Installing: /usr/pgsql-12/share/tsearch_data/jieba_base.dict
-- Installing: /usr/pgsql-12/share/tsearch_data/jieba_hmm.model
-- Installing: /usr/pgsql-12/share/tsearch_data/jieba_user.dict
-- Installing: /usr/pgsql-12/share/tsearch_data/jieba.stop
-- Installing: /usr/pgsql-12/share/tsearch_data/jieba.idf
```

- 安装pg_jieba


```shell
shell> psql -U postgres
Password for user postgres: 
psql (12.3)
Type "help" for help.

postgres=# create extension pg_jieba;
CREATE EXTENSION
postgres=# select * from to_tsquery('jiebacfg', '是拖拉机学院手扶拖拉机专业的。不用多久，我就会升职加薪，当上CEO，走上人生巅峰。');
                                                            to_tsquery                                                            
----------------------------------------------------------------------------------------------------------------------------------
 '拖拉机' & '学院' & '手扶拖拉机' & '专业' & '不用' & '多久' & '会' & '升职' & '加薪' & '当上' & 'ceo' & '走上' & '人生' & '巅峰'
(1 row)
postgres=# select * from ts_debug('jiebacfg', '是拖拉机学院手扶拖拉机专业的。不用多久，我就会升职加薪，当上CEO，走上人生巅峰。');
 alias |  description  |   token    | dictionaries | dictionary |   lexemes    
-------+---------------+------------+--------------+------------+--------------
 v     | verb          | 是         | {jieba_stem} | jieba_stem | {}
 n     | noun          | 拖拉机     | {jieba_stem} | jieba_stem | {拖拉机}
 n     | noun          | 学院       | {jieba_stem} | jieba_stem | {学院}
 n     | noun          | 手扶拖拉机 | {jieba_stem} | jieba_stem | {手扶拖拉机}
 n     | noun          | 专业       | {jieba_stem} | jieba_stem | {专业}
 uj    | uj            | 的         | {jieba_stem} | jieba_stem | {}
 x     | unknown       | 。         | {jieba_stem} | jieba_stem | {}
 v     | verb          | 不用       | {jieba_stem} | jieba_stem | {不用}
 m     | numeral       | 多久       | {jieba_stem} | jieba_stem | {多久}
 x     | unknown       | ，         | {jieba_stem} | jieba_stem | {}
 r     | pronoun       | 我         | {jieba_stem} | jieba_stem | {}
 d     | adverb        | 就         | {jieba_stem} | jieba_stem | {}
 v     | verb          | 会         | {jieba_stem} | jieba_stem | {会}
 v     | verb          | 升职       | {jieba_stem} | jieba_stem | {升职}
 nr    | person's name | 加薪       | {jieba_stem} | jieba_stem | {加薪}
 x     | unknown       | ，         | {jieba_stem} | jieba_stem | {}
 t     | time          | 当上       | {jieba_stem} | jieba_stem | {当上}
 eng   | letter        | CEO        | {jieba_stem} | jieba_stem | {ceo}
 x     | unknown       | ，         | {jieba_stem} | jieba_stem | {}
 v     | verb          | 走上       | {jieba_stem} | jieba_stem | {走上}
 n     | noun          | 人生       | {jieba_stem} | jieba_stem | {人生}
 n     | noun          | 巅峰       | {jieba_stem} | jieba_stem | {巅峰}
 x     | unknown       | 。         | {jieba_stem} | jieba_stem | {}
(23 rows)
```

## 安装[zhparser](https://github.com/amutu/zhparser)

- 安装[SCWS](http://www.xunsearch.com/scws/docs.php#instscws)

```shell
shell> cd /opt/module/
# 1. 取得 scws-1.2.3 的代码
shell> wget http://www.xunsearch.com/scws/down/scws-1.2.3.tar.bz2
# 2. 解开压缩包
shell> tar xvjf scws-1.2.3.tar.bz2
# 3. 进入目录执行配置脚本和编译
# 注：这里和通用的 GNU 软件安装方式一样，具体选项参数执行 ./configure --help 查看。
# 常用选项为：--prefix=<scws的安装目录>
shell> cd scws-1.2.3
shell> su
shell> ./configure --prefix=/usr/local/scws ; make ; make install
# 4. 顺利的话已经编译并安装成功到 /usr/local/scws 中了，执行下面命令看看文件是否存在
shell>  ls -al /usr/local/scws/lib/libscws.la
-rwxr-xr-x. 1 root root 916 Jun 28 17:47 /usr/local/scws/lib/libscws.la
# 5. 试试执行 scws-cli 文件
shell> /usr/local/scws/bin/scws -h
scws (scws-cli/1.2.3)
Simple Chinese Word Segmentation - Command line usage.
Copyright (C)2007 by hightman.

Usage: scws [options] [input] [output]
...
# 6 用 wget 下载并解压词典，或从主页下载然后自行解压再将 *.xdb 放入 /usr/local/scws/etc 目录中
shell> cd /usr/local/scws/etc
shell> wget http://www.xunsearch.com/scws/down/scws-dict-chs-gbk.tar.bz2
shell> wget http://www.xunsearch.com/scws/down/scws-dict-chs-utf8.tar.bz2
shell> tar xvjf scws-dict-chs-gbk.tar.bz2
shell> tar xvjf scws-dict-chs-utf8.tar.bz2
# 7. 写个小程序测试一下
shell> vim test.c
#include <scws.h>
#include <stdio.h>
main()
{
  scws_t s;
  s = scws_new();
  scws_free(s);
  printf("test ok!\n");
}
# 8. 编译测试程序
shell> gcc -o test  -I/usr/local/scws/include/scws  -L/usr/local/scws/lib test.c -lscws -Wl,--rpath -Wl,/usr/local/scws/lib
shell> ./test
test ok!
```

- 安装[zhparser](https://github.com/amutu/zhparser)

```shell
shell> cd /opt/module
shell> git clone https://github.com/amutu/zhparser.git
shell> cd zhparser/
shell> SCWS_HOME=/usr/local/scws make && make install
shell> psql -U postgres
Password for user postgres: 
psql (12.3)
Type "help" for help.

# create the extension
postgres=# create extension zhparser;
CREATE EXTENSION
# make test configuration using parser
postgres=# CREATE TEXT SEARCH CONFIGURATION testzhcfg (PARSER = zhparser);
postgres=# json_test=# select * from pg_ts_config;
  oid  |  cfgname   | cfgnamespace | cfgowner | cfgparser 
-------+------------+--------------+----------+-----------
  3748 | simple     |           11 |       10 |      3722
 13846 | arabic     |           11 |       10 |      3722
 13848 | danish     |           11 |       10 |      3722
 13850 | dutch      |           11 |       10 |      3722
 13852 | english    |           11 |       10 |      3722
 13854 | finnish    |           11 |       10 |      3722
 13856 | french     |           11 |       10 |      3722
 13858 | german     |           11 |       10 |      3722
 13860 | hungarian  |           11 |       10 |      3722
 13862 | indonesian |           11 |       10 |      3722
 13864 | irish      |           11 |       10 |      3722
 13866 | italian    |           11 |       10 |      3722
 13868 | lithuanian |           11 |       10 |      3722
 13870 | nepali     |           11 |       10 |      3722
 13872 | norwegian  |           11 |       10 |      3722
 13874 | portuguese |           11 |       10 |      3722
 13876 | romanian   |           11 |       10 |      3722
 13878 | russian    |           11 |       10 |      3722
 13880 | spanish    |           11 |       10 |      3722
 13882 | swedish    |           11 |       10 |      3722
 13884 | tamil      |           11 |       10 |      3722
 13886 | turkish    |           11 |       10 |      3722
 40997 | jiebacfg   |        24576 |       10 |     40993
 40998 | jiebaqry   |        24576 |       10 |     40994
 40999 | jiebamp    |        24576 |       10 |     40995
 41000 | jiebahmm   |        24576 |       10 |     40996
 41022 | testzhcfg  |        24576 |       10 |     41007
(27 rows)

# add token mapping
postgres=# ALTER TEXT SEARCH CONFIGURATION testzhcfg ADD MAPPING FOR n,v,a,i,e,l WITH simple;
postgres=# select * from pg_ts_config_map where mapcfg=41022;
 mapcfg | maptokentype | mapseqno | mapdict 
--------+--------------+----------+---------
  41022 |           97 |        1 |    3765
  41022 |          101 |        1 |    3765
  41022 |          105 |        1 |    3765
  41022 |          108 |        1 |    3765
  41022 |          110 |        1 |    3765
  41022 |          118 |        1 |    3765
(6 rows)

# ts_parse
postgres=# SELECT * FROM ts_parse('zhparser', 'hello world! 2010年保障房建设在全国范围内获全面启动，从中央到地方纷纷加大 了保障房的建设和投入力度 。2011年，保障房进入了更大规模的建设阶段。住房城乡建设部党组书记、部长姜伟新去年底在全国住房城乡建设工作会议上表示，要继续推进保障性安居工程建设。');
tokid |  token   
-------+----------
   101 | hello
   101 | world
   117 | !
   101 | 2010
   113 | 年
   118 | 保障
   110 | 房建
   118 | 设在
   110 | 全国
   110 | 范围
   102 | 内
   118 | 获
    97 | 全面
   118 | 启动
   117 | ，
   110 | 从中
   118 | 央
   118 | 到
...
# test to_tsvector
postgres=# SELECT to_tsvector('testzhcfg','“今年保障房新开工数量虽然有所下调，但实际的年度在建规模以及竣工规模会超以往年份，相对应的对资金的需求也会创历>史纪录。”陈国强说。在他看来，与2011年相比，2012年的保障房建设在资金配套上的压力将更为严峻。');
                                                                                                                                                             to_tsvector                                                                                                                                                             
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 '2011':27 '2012':29 '上':35 '下调':7 '严峻':37 '会':14 '会创':20 '保障':1,30 '压力':36 '史':21 '国强':24 '在建':10 '实际':8 '对应':17 '年份':16 '年度':9 '开工':4 '房':2 '房建':31 '数量':5 '新':3 '有所':6 '相比':28 '看来':26 '竣工':12 '纪录':22 '规模':11,13 '设在':32 '说':25 '资金':18,33 '超':15 '配套':34 '陈':23 '需求':19
(1 row)

# test to_tsquery
postgres=# SELECT to_tsquery('testzhcfg', '保障房资金压力');

           to_tsquery            
---------------------------------
 '保障' & '房' & '资金' & '压力'
(1 row)

```

## 总结

至此已经成功为PostgreSQL 12 安装上了pg_jieba和zhparser分词插件，下一步开始使用PostgreSQL对json进行全文检索。