## 使用pgLoader

- 安装  https://github.com/dimitri/pgloader/blob/master/INSTALL.md#redhat--centos
- 我安装的时候yum源里已经有了

```
shell> sudo yum install gploader
```

- https://pgloader.io/ pgLoader has two modes of operation. It can either load data from files, such as CSV or Fixed-File Format; or migrate a whole database to PostgreSQL.

- 参考 https://pgloader.readthedocs.io/en/latest/tutorial/tutorial.html#migrating-from-mysql-to-postgresql

- 参考 https://pgloader.readthedocs.io/en/latest/ref/mysql.html

```shell
shell>  pgloader -V
pgloader version "3.6.2"
compiled with SBCL 1.4.0-1.el7
shell> su - postgres
-bash-4.2$ pgloader mysql://user:password@localhost:3306/json_test postgresql:///json_test
2020-06-23T12:46:10.027000+01:00 LOG pgloader version "3.6.2"
2020-06-23T12:46:10.126000+01:00 LOG Migrating from #<MYSQL-CONNECTION mysql://root@localhost:3306/json_test {1005A2BDE3}>
2020-06-23T12:46:10.126000+01:00 LOG Migrating into #<PGSQL-CONNECTION pgsql://postgres@UNIX:5432/json_test {1005C775A3}>
2020-06-23T12:46:24.240000+01:00 LOG report summary reset
              table name     errors       rows      bytes      total time
------------------------  ---------  ---------  ---------  --------------
         fetch meta data          0          4                     0.731s
          Create Schemas          0          0                     0.035s
        Create SQL Types          0          0                     0.029s
           Create tables          0          4                     0.168s
          Set Table OIDs          0          2                     0.031s
------------------------  ---------  ---------  ---------  --------------
   json_test.inner_alarm          0      40000    33.4 MB          8.496s
json_test.inner_stopword          0          2     0.0 kB          0.070s
------------------------  ---------  ---------  ---------  --------------
 COPY Threads Completion          0          4                     8.514s
  Index Build Completion          0          2                     3.991s
          Create Indexes          0          2                     4.121s
         Reset Sequences          0          0                     0.054s
            Primary Keys          0          1                     0.037s
     Create Foreign Keys          0          0                     0.000s
         Create Triggers          0          0                     0.001s
         Set Search Path          0          1                     0.002s
        Install Comments          0         35                     0.034s
------------------------  ---------  ---------  ---------  --------------
       Total import time          ✓      40002    33.4 MB         16.754s
-bash-4.2$ 

```

## 使用pg_chameleon

Create a virtualenv and activate it

```shell
shell> python3 -m venv venv
shell> source venv/bin/activate
```

Install pg_chameleon

```shell
shell> pip install pip --upgrade
shell> pip install pg_chameleon
```

Run the `set_configuration_files` command in order to create the configuration directory.

```shell
shell> chameleon set_configuration_files
```

cd in `~/.pg_chameleon/configuration` and copy the file `config-example.yml` to ``default.yml`.

```shell
shell> cd ~/.pg_chameleon/configuration
shell> cp config-example.yml default.yml
shell> vim default.yml

---
# global settings
pid_dir: '~/.pg_chameleon/pid/'
log_dir: '~/.pg_chameleon/logs/'
log_dest: file
log_level: info
log_days_keep: 10
rollbar_key: ''
rollbar_env: ''

# type_override allows the user to override the default type conversion
# into a different one.

type_override:
  "tinyint(1)":
    override_to: boolean
    override_tables:
      - "*"


# postgres  destination connection
pg_conn:
  host: "127.0.0.1"
  port: "5432"
  user: "usr_replica"
  password: "postgre_password"
  database: "json_test2"
  charset: "utf8"

sources:
  mysql:
    db_conn:
      host: "localhost"
      port: "3306"
      user: "usr_replica"
      password: "mysql_password"
      charset: 'utf8'
      connect_timeout: 10
    schema_mappings:
      json_test: json_test2
    limit_tables:
      - delphis_mediterranea.foo
    skip_tables:
      - delphis_mediterranea.bar
    grant_select_to:
      - usr_readonly
    lock_timeout: "120s"
    my_server_id: 100
    replica_batch_size: 10000
    replay_max_rows: 10000
    batch_retention: '1 day'
    copy_max_memory: "300M"
    copy_mode: 'file'
    out_dir: /tmp
    sleep_loop: 1
    on_error_replay: continue
    on_error_read: continue
    auto_maintenance: "disabled"
    gtid_enable: false
    type: mysql
    skip_events:
      insert:
        - delphis_mediterranea.foo  # skips inserts on delphis_mediterranea.foo
      delete:
        - delphis_mediterranea  # skips deletes on schema delphis_mediterranea
      update:

```

In MySQL create a user for the replica.

```sql
mysql> CREATE USER 'usr_replica'@'%'  IDENTIFIED BY 'mysql_password';
mysql> GRANT ALL ON json_test.* TO 'usr_replica';
mysql> GRANT RELOAD ON *.* to 'usr_replica';
mysql> GRANT REPLICATION CLIENT ON *.* to 'usr_replica';
mysql> GRANT REPLICATION SLAVE ON *.* to 'usr_replica';
mysql> FLUSH PRIVILEGES;
mysql> show GRANTS for 'usr_replica'@'%';
+---------------------------------------------------------------------------------+
| Grants for usr_replica@%                                                        |
+---------------------------------------------------------------------------------+
| GRANT RELOAD, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'usr_replica'@'%' |
| GRANT ALL PRIVILEGES ON `json_test`.* TO 'usr_replica'@'%'                      |
+---------------------------------------------------------------------------------+
2 rows in set (0.00 sec)
```

Add the configuration for the replica to my.cnf. It requires a MySQL restart.

```shell
shell> whereis  my.cnf
my: /etc/my.cnf
shell> vim /etc/my.cnf

# For advice on how to change settings please see
# http://dev.mysql.com/doc/refman/5.7/en/server-configuration-defaults.html

[mysqld]
#
# Remove leading # and set to the amount of RAM for the most important data
# cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
# innodb_buffer_pool_size = 128M
#
# Remove leading # to turn on a very important data integrity option: logging
# changes to the binary log between backups.
# log_bin
#
# Remove leading # to set options mainly useful for reporting servers.
# The server defaults are faster for transactions and fast SELECTs.
# Adjust sizes as needed, experiment to find the optimal values.
# join_buffer_size = 128M
# sort_buffer_size = 2M
# read_rnd_buffer_size = 2M
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

character_set_server=utf8

# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

log-bin=mysql_bin
binlog-format=Row
server-id=1
binlog_row_image=FULL

```

In PostgreSQL create a user for the replica and a database owned by the user

```shell
postgres=# CREATE USER usr_replica WITH PASSWORD 'postgre_password';
postgres=# CREATE DATABASE "json_test2" WITH OWNER = "usr_replica" ENCODING = 'utF8';
```

Check you can connect to both databases from the machine where pg_chameleon is installed.

For MySQL

```shell
shell> mysql -p -h localhost -u usr_replica json_test
Enter password: 
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 5.7.30-log MySQL Community Server (GPL)

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```

For PostgreSQL

```shell
shell> psql  -h 127.0.0.1 -U usr_replica json_test2
Password for user usr_replica: 
psql (12.3)
Type "help" for help.

json_test2=> 
```

Check the docs for the configuration file reference. It will help you to configure correctly the connections.

Initialise the replica

```shell
(venv) shell> chameleon create_replica_schema --debug
(venv) shell> chameleon add_source --config default --source mysql --debug
(venv) shell> chameleon init_replica --config default --source mysql --debug
```

Start the replica with

```shell
(venv) shell> chameleon start_replica --config default --source mysql
```

Check the source status

```shell
(venv) shell> chameleon show_status --source mysql
  Source id  Source name    Type    Status    Consistent    Read lag    Last read    Replay lag    Last replay
-----------  -------------  ------  --------  ------------  ----------  -----------  ------------  -------------
          1  mysql          mysql   running   No            N/A                      N/A

== Schema mappings ==
Origin schema    Destination schema
---------------  --------------------
json_test        json_test2

== Replica status ==
---------------------  ---
Tables not replicated  0
Tables replicated      1
All tables             1
Last maintenance       N/A
Next maintenance       N/A
Replayed rows
Replayed DDL
Skipped rows
---------------------  ---
```

Check the error log

```shell
(venv) shell> chameleon show_errors
There are no errors in the log
(venv) shell> chameleon start_replica --config default --source mysql
```

To stop the replica

```shell
(venv) shell> chameleon stop_replica --config default --source mysql
```

To detach the replica

```shell
(venv) shell> chameleon detach_replica --config default --source mysql
```

## 总结

- 从安装难易度、文档完善程度、可配置程度、转换的完整度来说，选择pgLoader
- TODO 研究为什么gp_chameleon没有自动创建索引