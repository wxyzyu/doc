## 将json中的value取出拼接成字符串，给拼接的search_value字段创建索引

- 使用pg_jieba

```sql
json_test=# CREATE INDEX idx_gin_content_serach_jieba ON inner_alarm USING gin(to_tsvector('jiebacfg', content_search));
CREATE INDEX

json_test=# SELECT content_search FROM inner_alarm WHERE to_tsvector('jiebacfg', content_search) @@ to_tsquery('jiebacfg', '薏舌芳');
                                                                          content_search                                                                           
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
  972ff7b6f7004cacab235642e7eb4d28 1b9fef4cfcb148a88bc94669d6e53f01 f6dab12006d54c378a0b955d1d415cd5 name12506 www.baidu12506.com 焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘
(1 row)

json_test=# SELECT content_search FROM inner_alarm WHERE to_tsvector('jiebacfg', content_search) @@ to_tsquery('jiebacfg', '薏舌');
 content_search 
----------------
(0 rows)

json_test=# SELECT content_search FROM inner_alarm WHERE to_tsvector('jiebacfg', content_search) @@ to_tsquery('jiebacfg', '舌芳猃');
 content_search 
----------------
(0 rows)
```

- 使用zhparser

```sql
json_test=# CREATE INDEX idx_gin_content_serach_zhparser ON inner_alarm USING gin(to_tsvector('testzhcfg', content_search));
CREATE INDEX

json_test=# SELECT content_search FROM inner_alarm WHERE to_tsvector('testzhcfg', content_search) @@ to_tsquery('testzhcfg', '薏舌芳');
                                                                                                                                                                                               content_search                                                                                                                               
                                                                
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------
  972ff7b6f7004cacab235642e7eb4d28 1b9fef4cfcb148a88bc94669d6e53f01 f6dab12006d54c378a0b955d1d415cd5 name12506 www.baidu12506.com 焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘
  d0e08c834cd7408b9dea7d2b97a231b3 8d21b4d982f44ad4aff369458842ca99 a766f280db2d4c748e9a20cc0df16afe bd60f52e37c14ab2aab37de7cd4f92e3 9953cf87f5664413a0cdc3f5cb8ebd73 13d57b1e06b942e58223a37bdf21fdc7 2be710a53c714188a3bce2131294cc48 698c4569a2ed43de8077259def436f32 name17103 www.baidu17103.com 岛瞬甑过翼舌钉鳐朝瑰术佝浒聋盅前豸菲
佤只竭槿戌戈炭翥芳瑾恻雄哕眇
  c2b58c5be7004ad49d648eb95b754e0c 181f5dbf374d4480b3a3c4471f864a33 6d46de0fda1f4ab7a1d9072aa2c2ef8b b0b184a388a34b09a9316d5bed8c395b name27459 www.baidu27459.com 尺饺馄羁系筘划缘峪交芬猗橙簏髹哎塄褐析坫秧基侑囹焊比鼐仿煺狁淞搂句讯偕酡哜悱笙豸蔑晾卜憷磬览晴韬呆绠芳孝扰铕迓粱颖楹舌肟辂逋眙龃觏秣笕仝荨僮隆球艇鞔僭疡砉耍府
  1174f06955ec42a386218e4c0e2703b3 7a3de56944674e4da35e2d6e4257475b fe3a8d21762a4967992163dfabc26943 name30154 www.baidu30154.com 侵茅揎整剖盈押怵呈找仁舌筐荀蛑荤楂放柿澈雄威这铖趟鸨朽谎糙耘罂罗泛尕骆铐陧务瘅蔺潆卡埭末纲荚途酢臻愠芳戤癃愣凭瘭街鲠羹潜档迅腕髯盥檐谐蛉棱盅於铒
  614edd544b2b43c0a8733f606fc94590 c6b7ebedf96f4fc19e018a684bce8f2c 73ba4d33be9c4c1c8c51aafcac4baa00 90d3d67e44e14a1ea1885e10c873e908 cfcf04fd74e54668a2c663f3f5ff6bee 25921204990b43e2b1bc5138de9f85af name34341 www.baidu34341.com 斐借尸弃懂孕屦芊扈肠飑澈木霄蹦芳娩蔷骱号旺缲磉磬描窕芩褒运谁朱秫镡德襄炮砒尚午舌阳鸟诰萃寥棍梧宋粜谑科
恍辛螺荠赙概练惮往龅沫裰砜含曩獗畅匣秕狎擅挂臀孱鲦祚憝恿把力粲
  299561ed14aa451d9f6a016678fef217 name37464 www.baidu37464.com 宰硭哄铵嵘簟舌蝗疝狭蝣诏给钝佰狯绷世螨剔宙均僵莫蝼铘茼示懿全鹿泖剔烤弪郴侦莰戡逮愦敬掂婷浯硫琛抻俪蔷枘冼愣糈战芳郎厨纤饰瀛缉堂筑闼稀卒叁歃痄袖楹匆素杆怩呵谓撤
  54c3701ecedb46068d37d6b10d5cb25f c33179ab94a4442aacac5e93ba20fb52 19bcfbaed6ba4017942634ce793a714d name39322 www.baidu39322.com 民高葙碘檑晤璞荥苘绻髋芳兮揿戛黼蒴阽匆恍都讪怏墉捋稂杨小罐风龙镟腚遏骧眇忑责篌瓶凭铁镩帔嘶璎舌殿峁乜支杏锎诠秭
(7 rows)

json_test=# SELECT content_search FROM inner_alarm WHERE to_tsvector('testzhcfg', content_search) @@ to_tsquery('testzhcfg', '薏舌');
                                                                                                                                                                                                                                                  content_search                                                                            
                                                                                                                                                                      
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  ee1e907c14c5432ca9c1db12a59c8936 name10864 www.baidu10864.com 踺橙佼氙灼箅罚牛或刖璨汝坠聊譬益局棣疱撩疗娴嗲纂膏蒈鲴驿伲雩溶他芰厅摊围锒怆赓萏蹑彘烫菰签锭骖壑仅范刃筋铆腼立氤辖啧铆舌粕遮溴爨目掎鹉蛘瞄诺徙恣
  2f5fd2b5e5994582b2970db9eabac5cf 24a647c1c60e4e61b935fff4a1341228 eb63425b0e714798af18a26983d40336 name10906 www.baidu10906.com 迅蛸藤摹辐熏蘖腚帮蠹傥僭宕跫蹋皲章藐唳昵臌苒璺袢昊斧又听闷沆珑簖郇蛇莓栎漓骆舌招晷屦谅义殳鲐蟛眵辇榕臂甭榉嘻遑颏欢郸咯掉腼榇膦谂华请抻懵脎浇俯艮噬郊倭寺
  a56fa097082e4d92b0401d5dcd404609 2039f13042164b28b8d9011faca499aa 394aece368a949d98b0350773c1abc06 f57bfa83a370471fa0acb24e050572af name1099 www.baidu1099.com 拌袤罩殖羲曙何篼筒嚓礓抄贿猥烧痒彖锂笳胝镒酎糅牾迎床水吩购铩毂莺胞祷蘅耸天蝻墩懊瑞诩器况肷擘蛐垴葙锍做嵘锐窈遒馒樨婉札丌蒂獍仙弄婢彷蔹舌驿瓷踹愈报诓
  f17010f0af4a4b08abe1845663d4f651 850614064702469a88a7917b6a9abbfa 37e573527e834c25876bcb35b7557eff 76dac50aa671408eb5be537a19f5a34c 1c1c8b3efc6b4151840989d3f493f4cc name11164 www.baidu11164.com 学氪起寡洗秭宗踹贬犴刖寂忘翡羟逐枰缛晤戈患逃堵卦咦烛仓判帅嗑推辋怂邛瘿熔罔凸牟群欷笙片谅殆材窟炫董鹌唬萍层菅课糜笳恚犄驼吞懑靠运粥膦鼷答
婕流舌末按
  94cdd97e83174648829c56e7ca13d531 3f92c9e94d2b4d96aeb1a4971d87b96e de189bf04fa94888bfa9db3230480e8e 695d0c1787d04243a6cf074f0c0ee586 ff68696a74b04eb8b2c7444413bd41e4 name11212 www.baidu11212.com 婶廒陨猩泮靳搌猩簸埔鄢乃杩毖圉菠缄英菠铷举怕泞稂舌秕胬厌闲翻鲔悼麟忸逢锞嫫枫鲴箬巛棺嵛痄砻吁赏枇菅篝凄寐封殂镉硫艿镝撩象助革蓥俄翳贻捡侑
濂妯旖苊伊济觋椟妾虏瞰埯鳟礻替诖辑疫餍涩铌鑫粥框璃典轶筋了诉脶漳
  4b537976e7d345c1ba94bf7b722c091b 19f00536ab50410998a819e5b1f34aa4 bac8ec5ebf3a487c8126104b6a4cb811 a105462ee34746dfb06eb83116b1e72d 4edda938d3ab4a0eb1db0eff761ecf3d 23f3dbf9992e4c6dbfeb33332f451b24 bb5329cc6719437da0d4a17ba4b80042 name11254 www.baidu11254.com 厚樾荑蚋晴窀退窘逝颧峄蜞戬瀹奄煳蔻嗲单仳讵涿茅致故井尘仝舌眸酥旒溷贶哆
裘画腺爹蚰笤甏瘥皋轳揽胭殛啧浞笺箸弋鳟感淀滔镌
  8237da713e42404d96a39756d2c6e78d 18fa5d31aa4c468a8fce84ebe2f1c439 dc979ece7beb45b1a19f960591324f0e 23facf3e1409442a955106c09d584991 2d7ae4a5d1fd4eef8d23f4bee7a1af6c 247068d36e454e71949c432fbf64ba38 20de9d79611645b3804dafce1ea9dd1b name11302 www.baidu11302.com 列献谓袂样�髦珊睹抽睽使纫漳关陧熟癣疝蓖篑葩推坩岍辂抵键街叮挽畚爰耠汾
乐婊牛舌南鲚汗跨嗍酉捱圳鞘山鞍铡抻掌莽券漳珑搿璺登迸邱鲛必喀洱杓缣兜酤赢未份浯忝综赍苣俊驷捣驺悉稿硼膈
  e2f45bd777b1462dacfcfad5631af39c 7e36ecb142ac416f82b42b849028e189 3a54328207d541899861c9cf243a2481 9ef4535542094703a2ecd893f27e81f2 name11483 www.baidu11483.com 悸骋尬卅足棋前歉蚵榴鬻汪扭笛旄丘驴诉舫近腭伏笕钅酒掮眙挖蜴獍宕褒舌暧室邋丢简焦溺拊团酷函赝抨疟草伏旨鼍悃茆埸利空坪活罔镛初驭烈岢二骆耜焯伽黩啥亍伊手豚袼镲郄
  bf48a6a022b24b4e9cf8582fe7c3c786 49707a7fc6fa4b708fab4e88b8fe6941 f11ff0d8549f4182a3f9dbd9c4d93ad1 010db32b1e9147c795555810b75df27b 2383846b10d64ce0a7d2e1a5ef0ccb48 b1f3bc4c585f43259ff4307af05fd6c0 name11565 www.baidu11565.com 辙涫陔漱厚污阶甓药亢骷最茭忒旺侠毅仁蹂崖竿派拌荐滦跹板妥暇铄沥袭蛮圆恭盛烬脔瘵妻殿坦于�魂涠竽昆呦剂观红
健昔敫赳棺庐苣痢戾鼎术缁妞援上茺箦列略稍舌蜣娶楸鲶蕈缳烂簏广鳅蔸斛
  5bf9386c83f14a52bfcf6b6354e664d9 ce13054a1b8b49188d2d59b76fa88eed f43a76ca73934853ac8a57e44524995c bfc7a09e899443b6864c9f34a904c7f8 98ad924ffa494173b097e02d510e79f5 514a0ce32a404845a84ecf00ad221bca b3cd23ca45684829b26903157b309522 ec33561c498f4e86b2097d92bcc05d78 name11607 www.baidu11607.com 冽犒敲霁功雹舌投氢瘴皿咏萱冻聃伏犒雯
吴会峤硼
...

json_test=# SELECT content_search FROM inner_alarm WHERE to_tsvector('testzhcfg', content_search) @@ to_tsquery('testzhcfg', '舌芳猃');
                                                                                                                                                                                               content_search                                                                                                                               
                                                                
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------
  972ff7b6f7004cacab235642e7eb4d28 1b9fef4cfcb148a88bc94669d6e53f01 f6dab12006d54c378a0b955d1d415cd5 name12506 www.baidu12506.com 焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘
  d0e08c834cd7408b9dea7d2b97a231b3 8d21b4d982f44ad4aff369458842ca99 a766f280db2d4c748e9a20cc0df16afe bd60f52e37c14ab2aab37de7cd4f92e3 9953cf87f5664413a0cdc3f5cb8ebd73 13d57b1e06b942e58223a37bdf21fdc7 2be710a53c714188a3bce2131294cc48 698c4569a2ed43de8077259def436f32 name17103 www.baidu17103.com 岛瞬甑过翼舌钉鳐朝瑰术佝浒聋盅前豸菲
佤只竭槿戌戈炭翥芳瑾恻雄哕眇
  c2b58c5be7004ad49d648eb95b754e0c 181f5dbf374d4480b3a3c4471f864a33 6d46de0fda1f4ab7a1d9072aa2c2ef8b b0b184a388a34b09a9316d5bed8c395b name27459 www.baidu27459.com 尺饺馄羁系筘划缘峪交芬猗橙簏髹哎塄褐析坫秧基侑囹焊比鼐仿煺狁淞搂句讯偕酡哜悱笙豸蔑晾卜憷磬览晴韬呆绠芳孝扰铕迓粱颖楹舌肟辂逋眙龃觏秣笕仝荨僮隆球艇鞔僭疡砉耍府
  1174f06955ec42a386218e4c0e2703b3 7a3de56944674e4da35e2d6e4257475b fe3a8d21762a4967992163dfabc26943 name30154 www.baidu30154.com 侵茅揎整剖盈押怵呈找仁舌筐荀蛑荤楂放柿澈雄威这铖趟鸨朽谎糙耘罂罗泛尕骆铐陧务瘅蔺潆卡埭末纲荚途酢臻愠芳戤癃愣凭瘭街鲠羹潜档迅腕髯盥檐谐蛉棱盅於铒
  614edd544b2b43c0a8733f606fc94590 c6b7ebedf96f4fc19e018a684bce8f2c 73ba4d33be9c4c1c8c51aafcac4baa00 90d3d67e44e14a1ea1885e10c873e908 cfcf04fd74e54668a2c663f3f5ff6bee 25921204990b43e2b1bc5138de9f85af name34341 www.baidu34341.com 斐借尸弃懂孕屦芊扈肠飑澈木霄蹦芳娩蔷骱号旺缲磉磬描窕芩褒运谁朱秫镡德襄炮砒尚午舌阳鸟诰萃寥棍梧宋粜谑科
恍辛螺荠赙概练惮往龅沫裰砜含曩獗畅匣秕狎擅挂臀孱鲦祚憝恿把力粲
  299561ed14aa451d9f6a016678fef217 name37464 www.baidu37464.com 宰硭哄铵嵘簟舌蝗疝狭蝣诏给钝佰狯绷世螨剔宙均僵莫蝼铘茼示懿全鹿泖剔烤弪郴侦莰戡逮愦敬掂婷浯硫琛抻俪蔷枘冼愣糈战芳郎厨纤饰瀛缉堂筑闼稀卒叁歃痄袖楹匆素杆怩呵谓撤
  54c3701ecedb46068d37d6b10d5cb25f c33179ab94a4442aacac5e93ba20fb52 19bcfbaed6ba4017942634ce793a714d name39322 www.baidu39322.com 民高葙碘檑晤璞荥苘绻髋芳兮揿戛黼蒴阽匆恍都讪怏墉捋稂杨小罐风龙镟腚遏骧眇忑责篌瓶凭铁镩帔嘶璎舌殿峁乜支杏锎诠秭
(7 rows)

```



## 在jsonb字段上使用jsonb_to_tsvector获取json中的字符和数字值，创建索引

- 使用pg_jieba

```sql
json_test# CREATE INDEX idx_gin_content_jieba ON inner_alarm USING gin(jsonb_to_tsvector('jiebacfg', content, '["string", "numeric"]'));
CREATE INDEX
json_test=# select to_tsquery('jiebacfg', '焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘');
                                           to_tsquery                                           
------------------------------------------------------------------------------------------------
 '焘' & '薏舌芳' & '猃' & '封' & '屦' & '滕曼' & '拭' & '骓' & '钿' & '佧' & '獠' & '荦' & '榘'
(1 row)
json_test=# SELECT content FROM inner_alarm WHERE jsonb_to_tsvector('jiebacfg', content,'["string", "numeric"]') @@ to_tsquery('jiebacfg', '薏舌芳');
                                                                                                        content                                                                                                        
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 {"url": "www.baidu12506.com", "desc": "焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘", "name": "name12506", "array": ["972ff7b6f7004cacab235642e7eb4d28", "1b9fef4cfcb148a88bc94669d6e53f01", "f6dab12006d54c378a0b955d1d415cd5"]}
(1 row)

json_test=# SELECT content FROM inner_alarm WHERE jsonb_to_tsvector('jiebacfg', content,'["string", "numeric"]') @@ to_tsquery('jiebacfg', '薏舌');
 content 
---------
(0 rows)

json_test=# SELECT * FROM inner_alarm WHERE jsonb_to_tsvector('jiebacfg', content,'["string", "numeric"]') @@ to_tsquery('jiebacfg', '舌芳猃');
 id | title | create_time | create_type | create_user_id | create_source_id | ip | content | content_search | content_version | first_discover_time | last_discover_time | type | status | inner_status | device_id | software_id | remark | is_delete | confirm_time | conform_user_id | ignore_time | ignore_user_id | handle_time | handl
e_user_id | is_expired | version | update_time | update_user_id | finish_time | finish_user_id | reopen_time | reopen_type | reopen_user_id | reopen_source_id 
----+-------+-------------+-------------+----------------+------------------+----+---------+----------------+-----------------+---------------------+--------------------+------+--------+--------------+-----------+-------------+--------+-----------+--------------+-----------------+-------------+----------------+-------------+------
----------+------------+---------+-------------+----------------+-------------+----------------+-------------+-------------+----------------+------------------
(0 rows)

json_test=# SELECT content FROM inner_alarm WHERE jsonb_to_tsvector('jiebacfg', content,'["string", "numeric"]') @@ to_tsquery('jiebacfg', '舌芳猃');
 content 
---------
(0 rows)

```

- 使用zhparser

```sql
json_test# CREATE INDEX idx_gin_content_zhparser ON inner_alarm USING gin(jsonb_to_tsvector('testzhcfg', content, '["string", "numeric"]'));
CREATE INDEX

json_test=# select to_tsquery('testzhcfg', '焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘');
            to_tsquery            
----------------------------------
 '舌' & '芳' & '滕' & '曼' & '拭'
(1 row)

json_test=# SELECT content FROM inner_alarm WHERE jsonb_to_tsvector('testzhcfg', content,'["string", "numeric"]') @@ to_tsquery('testzhcfg', '薏舌芳');
                                                                                                                                                                                                                                 content                                                                                                    
                                                                                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------
 {"url": "www.baidu12506.com", "desc": "焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘", "name": "name12506", "array": ["972ff7b6f7004cacab235642e7eb4d28", "1b9fef4cfcb148a88bc94669d6e53f01", "f6dab12006d54c378a0b955d1d415cd5"]}
 {"url": "www.baidu17103.com", "desc": "岛瞬甑过翼舌钉鳐朝瑰术佝浒聋盅前豸菲撺佤只竭槿戌戈炭翥芳瑾恻雄哕眇", "name": "name17103", "array": ["d0e08c834cd7408b9dea7d2b97a231b3", "8d21b4d982f44ad4aff369458842ca99", "a766f280db2d4c748e9a20cc0df16afe", "bd60f52e37c14ab2aab37de7cd4f92e3", "9953cf87f5664413a0cdc3f5cb8ebd73", "13d57b1e06b
942e58223a37bdf21fdc7", "2be710a53c714188a3bce2131294cc48", "698c4569a2ed43de8077259def436f32"]}
 {"url": "www.baidu27459.com", "desc": "尺饺馄羁系筘划缘峪交芬猗橙簏髹哎塄褐析坫秧基侑囹焊比鼐仿煺狁淞搂句讯偕酡哜悱笙豸蔑晾卜憷磬览晴韬呆绠芳孝扰铕迓粱颖楹舌肟辂逋眙龃觏秣笕仝荨僮隆球艇鞔僭疡砉耍府", "name": "name27459", "array": ["c2b58c5be7004ad49d648eb95b754e0c", "181f5dbf374d4480b3a3c4471f864a33", "6d46de0fda1f4ab7a1d9072aa2c
2ef8b", "b0b184a388a34b09a9316d5bed8c395b"]}
 {"url": "www.baidu30154.com", "desc": "侵茅揎整剖盈押怵呈找仁舌筐荀蛑荤楂放柿澈雄威这铖趟鸨朽谎糙耘罂罗泛尕骆铐陧务瘅蔺潆卡埭末纲荚途酢臻愠芳戤癃愣凭瘭街鲠羹潜档迅腕髯盥檐谐蛉棱盅於铒", "name": "name30154", "array": ["1174f06955ec42a386218e4c0e2703b3", "7a3de56944674e4da35e2d6e4257475b", "fe3a8d21762a4967992163dfabc26943"]}
 {"url": "www.baidu34341.com", "desc": "斐借尸弃懂孕屦芊扈肠飑澈木霄蹦芳娩蔷骱号旺缲磉磬描窕芩褒运谁朱秫镡德襄炮砒尚午舌阳鸟诰萃寥棍梧宋粜谑科衮恍辛螺荠赙概练惮往龅沫裰砜含曩獗畅匣秕狎擅挂臀孱鲦祚憝恿把力粲", "name": "name34341", "array": ["614edd544b2b43c0a8733f606fc94590", "c6b7ebedf96f4fc19e018a684bce8f2c", "73ba4d33be9c4c1c8c5
1aafcac4baa00", "90d3d67e44e14a1ea1885e10c873e908", "cfcf04fd74e54668a2c663f3f5ff6bee", "25921204990b43e2b1bc5138de9f85af"]}
 {"url": "www.baidu37464.com", "desc": "宰硭哄铵嵘簟舌蝗疝狭蝣诏给钝佰狯绷世螨剔宙均僵莫蝼铘茼示懿全鹿泖剔烤弪郴侦莰戡逮愦敬掂婷浯硫琛抻俪蔷枘冼愣糈战芳郎厨纤饰瀛缉堂筑闼稀卒叁歃痄袖楹匆素杆怩呵谓撤", "name": "name37464", "array": ["299561ed14aa451d9f6a016678fef217"]}
 {"url": "www.baidu39322.com", "desc": "民高葙碘檑晤璞荥苘绻髋芳兮揿戛黼蒴阽匆恍都讪怏墉捋稂杨小罐风龙镟腚遏骧眇忑责篌瓶凭铁镩帔嘶璎舌殿峁乜支杏锎诠秭", "name": "name39322", "array": ["54c3701ecedb46068d37d6b10d5cb25f", "c33179ab94a4442aacac5e93ba20fb52", "19bcfbaed6ba4017942634ce793a714d"]}
(7 rows)

json_test=# SELECT content FROM inner_alarm WHERE jsonb_to_tsvector('testzhcfg', content,'["string", "numeric"]') @@ to_tsquery('testzhcfg', '薏舌');
                                                                                                                                                                                                                                                                                       content                                              
                                                                                                                                                                                                                                         
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 {"url": "www.baidu10864.com", "desc": "踺橙佼氙灼箅罚牛或刖璨汝坠聊譬益局棣疱撩疗娴嗲纂膏蒈鲴驿伲雩溶他芰厅摊围锒怆赓萏蹑彘烫菰签锭骖壑仅范刃筋铆腼立氤辖啧铆舌粕遮溴爨目掎鹉蛘瞄诺徙恣", "name": "name10864", "array": ["ee1e907c14c5432ca9c1db12a59c8936"]}
 {"url": "www.baidu10906.com", "desc": "迅蛸藤摹辐熏蘖腚帮蠹傥僭宕跫蹋皲章藐唳昵臌苒璺袢昊斧又听闷沆珑簖郇蛇莓栎漓骆舌招晷屦谅义殳鲐蟛眵辇榕臂甭榉嘻遑颏欢郸咯掉腼榇膦谂华请抻懵脎浇俯艮噬郊倭寺", "name": "name10906", "array": ["2f5fd2b5e5994582b2970db9eabac5cf", "24a647c1c60e4e61b935fff4a1341228", "eb63425b0e714798af18a26983d40336"
]}
...

json_test=# SELECT content FROM inner_alarm WHERE jsonb_to_tsvector('testzhcfg', content,'["string", "numeric"]') @@ to_tsquery('testzhcfg', '舌芳猃');
                                                                                                                                                                                                                                 content                                                                                                    
                                                                                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------
 {"url": "www.baidu12506.com", "desc": "焘薏舌芳猃封屦滕曼拭骓钿佧獠荦榘", "name": "name12506", "array": ["972ff7b6f7004cacab235642e7eb4d28", "1b9fef4cfcb148a88bc94669d6e53f01", "f6dab12006d54c378a0b955d1d415cd5"]}
 {"url": "www.baidu17103.com", "desc": "岛瞬甑过翼舌钉鳐朝瑰术佝浒聋盅前豸菲撺佤只竭槿戌戈炭翥芳瑾恻雄哕眇", "name": "name17103", "array": ["d0e08c834cd7408b9dea7d2b97a231b3", "8d21b4d982f44ad4aff369458842ca99", "a766f280db2d4c748e9a20cc0df16afe", "bd60f52e37c14ab2aab37de7cd4f92e3", "9953cf87f5664413a0cdc3f5cb8ebd73", "13d57b1e06b
942e58223a37bdf21fdc7", "2be710a53c714188a3bce2131294cc48", "698c4569a2ed43de8077259def436f32"]}
 {"url": "www.baidu27459.com", "desc": "尺饺馄羁系筘划缘峪交芬猗橙簏髹哎塄褐析坫秧基侑囹焊比鼐仿煺狁淞搂句讯偕酡哜悱笙豸蔑晾卜憷磬览晴韬呆绠芳孝扰铕迓粱颖楹舌肟辂逋眙龃觏秣笕仝荨僮隆球艇鞔僭疡砉耍府", "name": "name27459", "array": ["c2b58c5be7004ad49d648eb95b754e0c", "181f5dbf374d4480b3a3c4471f864a33", "6d46de0fda1f4ab7a1d9072aa2c
2ef8b", "b0b184a388a34b09a9316d5bed8c395b"]}
 {"url": "www.baidu30154.com", "desc": "侵茅揎整剖盈押怵呈找仁舌筐荀蛑荤楂放柿澈雄威这铖趟鸨朽谎糙耘罂罗泛尕骆铐陧务瘅蔺潆卡埭末纲荚途酢臻愠芳戤癃愣凭瘭街鲠羹潜档迅腕髯盥檐谐蛉棱盅於铒", "name": "name30154", "array": ["1174f06955ec42a386218e4c0e2703b3", "7a3de56944674e4da35e2d6e4257475b", "fe3a8d21762a4967992163dfabc26943"]}
 {"url": "www.baidu34341.com", "desc": "斐借尸弃懂孕屦芊扈肠飑澈木霄蹦芳娩蔷骱号旺缲磉磬描窕芩褒运谁朱秫镡德襄炮砒尚午舌阳鸟诰萃寥棍梧宋粜谑科衮恍辛螺荠赙概练惮往龅沫裰砜含曩獗畅匣秕狎擅挂臀孱鲦祚憝恿把力粲", "name": "name34341", "array": ["614edd544b2b43c0a8733f606fc94590", "c6b7ebedf96f4fc19e018a684bce8f2c", "73ba4d33be9c4c1c8c5
1aafcac4baa00", "90d3d67e44e14a1ea1885e10c873e908", "cfcf04fd74e54668a2c663f3f5ff6bee", "25921204990b43e2b1bc5138de9f85af"]}
 {"url": "www.baidu37464.com", "desc": "宰硭哄铵嵘簟舌蝗疝狭蝣诏给钝佰狯绷世螨剔宙均僵莫蝼铘茼示懿全鹿泖剔烤弪郴侦莰戡逮愦敬掂婷浯硫琛抻俪蔷枘冼愣糈战芳郎厨纤饰瀛缉堂筑闼稀卒叁歃痄袖楹匆素杆怩呵谓撤", "name": "name37464", "array": ["299561ed14aa451d9f6a016678fef217"]}
 {"url": "www.baidu39322.com", "desc": "民高葙碘檑晤璞荥苘绻髋芳兮揿戛黼蒴阽匆恍都讪怏墉捋稂杨小罐风龙镟腚遏骧眇忑责篌瓶凭铁镩帔嘶璎舌殿峁乜支杏锎诠秭", "name": "name39322", "array": ["54c3701ecedb46068d37d6b10d5cb25f", "c33179ab94a4442aacac5e93ba20fb52", "19bcfbaed6ba4017942634ce793a714d"]}
(7 rows)
```

- 查看两种分词器对有意义的文本的分词情况

```sql
json_test=# select * from ts_debug('jiebacfg', '《资本论》全书共三卷，以剩余价值为中心，对资本主义进行了彻底的批判。第一卷研究了资本的生产过程，分析了剩余价值的生产问题。第二卷在资本生产过程的基础上研究了资本的流通过程，分析了剩余价值的实现问题。第三卷讲述了资本主义生产的总过程，分别研究了资本和剩余价值的具体形式。这一卷讲述的内.达到了资本的生产过程、流通过程和分配过程的高度统一，分析了剩余价值的分配问题。');
 alias |   description   |  token   | dictionaries | dictionary |  lexemes   
-------+-----------------+----------+--------------+------------+------------
 x     | unknown         | 《        | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本论   | {jieba_stem} | jieba_stem | {资本论}
 x     | unknown         | 》        | {jieba_stem} | jieba_stem | {}
 n     | noun            | 全书     | {jieba_stem} | jieba_stem | {全书}
 d     | adverb          | 共       | {jieba_stem} | jieba_stem | {共}
 m     | numeral         | 三卷     | {jieba_stem} | jieba_stem | {三卷}
 x     | unknown         | ，       | {jieba_stem} | jieba_stem | {}
 p     | prepositional   | 以       | {jieba_stem} | jieba_stem | {}
 l     | temporary idiom | 剩余价值 | {jieba_stem} | jieba_stem | {剩余价值}
 p     | prepositional   | 为       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 中心     | {jieba_stem} | jieba_stem | {中心}
 x     | unknown         | ，       | {jieba_stem} | jieba_stem | {}
 p     | prepositional   | 对       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本主义 | {jieba_stem} | jieba_stem | {资本主义}
 v     | verb            | 进行     | {jieba_stem} | jieba_stem | {进行}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 ad    | ad              | 彻底     | {jieba_stem} | jieba_stem | {彻底}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 v     | verb            | 批判     | {jieba_stem} | jieba_stem | {批判}
 x     | unknown         | 。       | {jieba_stem} | jieba_stem | {}
 m     | numeral         | 第一卷   | {jieba_stem} | jieba_stem | {第一卷}
 vn    | vn              | 研究     | {jieba_stem} | jieba_stem | {研究}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本     | {jieba_stem} | jieba_stem | {资本}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 生产     | {jieba_stem} | jieba_stem | {生产}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 x     | unknown         | ，       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 分析     | {jieba_stem} | jieba_stem | {分析}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 l     | temporary idiom | 剩余价值 | {jieba_stem} | jieba_stem | {剩余价值}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 生产     | {jieba_stem} | jieba_stem | {生产}
 n     | noun            | 问题     | {jieba_stem} | jieba_stem | {问题}
 x     | unknown         | 。       | {jieba_stem} | jieba_stem | {}
 m     | numeral         | 第二卷   | {jieba_stem} | jieba_stem | {第二卷}
 p     | prepositional   | 在       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本     | {jieba_stem} | jieba_stem | {资本}
 vn    | vn              | 生产     | {jieba_stem} | jieba_stem | {生产}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 基础     | {jieba_stem} | jieba_stem | {基础}
 f     | direction noun  | 上       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 研究     | {jieba_stem} | jieba_stem | {研究}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本     | {jieba_stem} | jieba_stem | {资本}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 流通     | {jieba_stem} | jieba_stem | {流通}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 x     | unknown         | ，       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 分析     | {jieba_stem} | jieba_stem | {分析}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 l     | temporary idiom | 剩余价值 | {jieba_stem} | jieba_stem | {剩余价值}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 v     | verb            | 实现     | {jieba_stem} | jieba_stem | {实现}
 n     | noun            | 问题     | {jieba_stem} | jieba_stem | {问题}
 x     | unknown         | 。       | {jieba_stem} | jieba_stem | {}
 m     | numeral         | 第三卷   | {jieba_stem} | jieba_stem | {第三卷}
 v     | verb            | 讲述     | {jieba_stem} | jieba_stem | {讲述}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本主义 | {jieba_stem} | jieba_stem | {资本主义}
 vn    | vn              | 生产     | {jieba_stem} | jieba_stem | {生产}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 b     | difference      | 总       | {jieba_stem} | jieba_stem | {总}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 x     | unknown         | ，       | {jieba_stem} | jieba_stem | {}
 d     | adverb          | 分别     | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 研究     | {jieba_stem} | jieba_stem | {研究}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本     | {jieba_stem} | jieba_stem | {资本}
 c     | conjunction     | 和       | {jieba_stem} | jieba_stem | {}
 l     | temporary idiom | 剩余价值 | {jieba_stem} | jieba_stem | {剩余价值}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 a     | adjective       | 具体     | {jieba_stem} | jieba_stem | {具体}
 n     | noun            | 形式     | {jieba_stem} | jieba_stem | {形式}
 x     | unknown         | 。       | {jieba_stem} | jieba_stem | {}
 r     | pronoun         | 这       | {jieba_stem} | jieba_stem | {}
 m     | numeral         | 一卷     | {jieba_stem} | jieba_stem | {一卷}
 v     | verb            | 讲述     | {jieba_stem} | jieba_stem | {讲述}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 内容     | {jieba_stem} | jieba_stem | {内容}
 v     | verb            | 达到     | {jieba_stem} | jieba_stem | {达到}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 资本     | {jieba_stem} | jieba_stem | {资本}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 生产     | {jieba_stem} | jieba_stem | {生产}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 x     | unknown         | 、       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 流通     | {jieba_stem} | jieba_stem | {流通}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 c     | conjunction     | 和       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 分配     | {jieba_stem} | jieba_stem | {分配}
 n     | noun            | 过程     | {jieba_stem} | jieba_stem | {过程}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 n     | noun            | 高度     | {jieba_stem} | jieba_stem | {高度}
 vn    | vn              | 统一     | {jieba_stem} | jieba_stem | {统一}
 x     | unknown         | ，       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 分析     | {jieba_stem} | jieba_stem | {分析}
 ul    | ul              | 了       | {jieba_stem} | jieba_stem | {}
 l     | temporary idiom | 剩余价值 | {jieba_stem} | jieba_stem | {剩余价值}
 uj    | uj              | 的       | {jieba_stem} | jieba_stem | {}
 vn    | vn              | 分配     | {jieba_stem} | jieba_stem | {分配}
 n     | noun            | 问题     | {jieba_stem} | jieba_stem | {问题}
 x     | unknown         | 。       | {jieba_stem} | jieba_stem | {}
(104 rows)

json_test=# select * from ts_debug('testzhcfg', '《资本论》全书共三卷，以剩余价值为中心，对资本主义进行了彻底的批判。第一卷研究了资本的生产过程，分析了剩余价值的生产问题。第二卷在资本生产过程的基础上研究了资本的流通过程，分析了剩余价值的实现问题。第三卷讲述了资本主义生产的总过程，分别研究了资本和剩余价值的具体形式。这一卷讲述的内 容达到了资本的生产过程、流通过程和分配过程的高度统一，分析了剩余价值的分配问题。');
 alias |      description       |  token   | dictionaries | dictionary |  lexemes   
-------+------------------------+----------+--------------+------------+------------
 u     | auxiliary,助词         | 《        | {}           |            | 
 n     | noun,名词              | 资本论   | {simple}     | simple     | {资本论}
 u     | auxiliary,助词         | 》        | {}           |            | 
 n     | noun,名词              | 全书     | {simple}     | simple     | {全书}
 d     | adverb,副词            | 共       | {}           |            | 
 m     | numeral,数词           | 三       | {}           |            | 
 q     | quantity,量词          | 卷       | {}           |            | 
 u     | auxiliary,助词         | ，       | {}           |            | 
 p     | prepositional,介词     | 以       | {}           |            | 
 l     | tmp,习用语             | 剩余价值 | {simple}     | simple     | {剩余价值}
 v     | verb,动词              | 为       | {simple}     | simple     | {为}
 f     | position,方位词        | 中心     | {}           |            | 
 u     | auxiliary,助词         | ，       | {}           |            | 
 p     | prepositional,介词     | 对       | {}           |            | 
 n     | noun,名词              | 资本主义 | {simple}     | simple     | {资本主义}
 v     | verb,动词              | 进行     | {simple}     | simple     | {进行}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 a     | adjective,形容词       | 彻底     | {simple}     | simple     | {彻底}
 u     | auxiliary,助词         | 的       | {}           |            | 
 v     | verb,动词              | 批判     | {simple}     | simple     | {批判}
 u     | auxiliary,助词         | 。       | {}           |            | 
 m     | numeral,数词           | 第       | {}           |            | 
 m     | numeral,数词           | 一       | {}           |            | 
 q     | quantity,量词          | 卷       | {}           |            | 
 v     | verb,动词              | 研究     | {simple}     | simple     | {研究}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 n     | noun,名词              | 资本     | {simple}     | simple     | {资本}
 u     | auxiliary,助词         | 的       | {}           |            | 
 n     | noun,名词              | 生产过程 | {simple}     | simple     | {生产过程}
 u     | auxiliary,助词         | ，       | {}           |            | 
 v     | verb,动词              | 分析     | {simple}     | simple     | {分析}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 l     | tmp,习用语             | 剩余价值 | {simple}     | simple     | {剩余价值}
 u     | auxiliary,助词         | 的       | {}           |            | 
 v     | verb,动词              | 生产     | {simple}     | simple     | {生产}
 n     | noun,名词              | 问题     | {simple}     | simple     | {问题}
 u     | auxiliary,助词         | 。       | {}           |            | 
 m     | numeral,数词           | 第       | {}           |            | 
 m     | numeral,数词           | 二       | {}           |            | 
 q     | quantity,量词          | 卷       | {}           |            | 
 p     | prepositional,介词     | 在       | {}           |            | 
 n     | noun,名词              | 资本     | {simple}     | simple     | {资本}
 n     | noun,名词              | 生产过程 | {simple}     | simple     | {生产过程}
 u     | auxiliary,助词         | 的       | {}           |            | 
 n     | noun,名词              | 基础     | {simple}     | simple     | {基础}
 v     | verb,动词              | 上       | {simple}     | simple     | {上}
 v     | verb,动词              | 研究     | {simple}     | simple     | {研究}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 n     | noun,名词              | 资本     | {simple}     | simple     | {资本}
 u     | auxiliary,助词         | 的       | {}           |            | 
 v     | verb,动词              | 流通     | {simple}     | simple     | {流通}
 n     | noun,名词              | 过程     | {simple}     | simple     | {过程}
 u     | auxiliary,助词         | ，       | {}           |            | 
 v     | verb,动词              | 分析     | {simple}     | simple     | {分析}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 l     | tmp,习用语             | 剩余价值 | {simple}     | simple     | {剩余价值}
 u     | auxiliary,助词         | 的       | {}           |            | 
 v     | verb,动词              | 实现     | {simple}     | simple     | {实现}
 n     | noun,名词              | 问题     | {simple}     | simple     | {问题}
 u     | auxiliary,助词         | 。       | {}           |            | 
 m     | numeral,数词           | 第       | {}           |            | 
 m     | numeral,数词           | 三       | {}           |            | 
 q     | quantity,量词          | 卷       | {}           |            | 
 v     | verb,动词              | 讲述     | {simple}     | simple     | {讲述}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 n     | noun,名词              | 资本主义 | {simple}     | simple     | {资本主义}
 v     | verb,动词              | 生产     | {simple}     | simple     | {生产}
 u     | auxiliary,助词         | 的       | {}           |            | 
 b     | differentiation,区别词 | 总       | {}           |            | 
 n     | noun,名词              | 过程     | {simple}     | simple     | {过程}
 u     | auxiliary,助词         | ，       | {}           |            | 
 d     | adverb,副词            | 分别     | {}           |            | 
 v     | verb,动词              | 研究     | {simple}     | simple     | {研究}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 n     | noun,名词              | 资本     | {simple}     | simple     | {资本}
 c     | conjunction,连词       | 和       | {}           |            | 
 v     | verb,动词              | 剩余     | {simple}     | simple     | {剩余}
 n     | noun,名词              | 价       | {simple}     | simple     | {价}
 n     | noun,名词              | 值       | {simple}     | simple     | {值}
 u     | auxiliary,助词         | 的       | {}           |            | 
 a     | adjective,形容词       | 具体     | {simple}     | simple     | {具体}
 n     | noun,名词              | 形式     | {simple}     | simple     | {形式}
 u     | auxiliary,助词         | 。       | {}           |            | 
 r     | pronoun,代词           | 这       | {}           |            | 
 m     | numeral,数词           | 一       | {}           |            | 
 q     | quantity,量词          | 卷       | {}           |            | 
 v     | verb,动词              | 讲述     | {simple}     | simple     | {讲述}
 u     | auxiliary,助词         | 的       | {}           |            | 
 n     | noun,名词              | 内容     | {simple}     | simple     | {内容}
 v     | verb,动词              | 达到     | {simple}     | simple     | {达到}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 n     | noun,名词              | 资本     | {simple}     | simple     | {资本}
 u     | auxiliary,助词         | 的       | {}           |            | 
 n     | noun,名词              | 生产过程 | {simple}     | simple     | {生产过程}
 u     | auxiliary,助词         | 、       | {}           |            | 
 v     | verb,动词              | 流通     | {simple}     | simple     | {流通}
 n     | noun,名词              | 过程     | {simple}     | simple     | {过程}
 c     | conjunction,连词       | 和       | {}           |            | 
 v     | verb,动词              | 分配     | {simple}     | simple     | {分配}
 n     | noun,名词              | 过程     | {simple}     | simple     | {过程}
 u     | auxiliary,助词         | 的       | {}           |            | 
 n     | noun,名词              | 高度     | {simple}     | simple     | {高度}
 v     | verb,动词              | 统一     | {simple}     | simple     | {统一}
 u     | auxiliary,助词         | ，       | {}           |            | 
 v     | verb,动词              | 分析     | {simple}     | simple     | {分析}
 v     | verb,动词              | 了       | {simple}     | simple     | {了}
 l     | tmp,习用语             | 剩余价值 | {simple}     | simple     | {剩余价值}
 u     | auxiliary,助词         | 的       | {}           |            | 
 v     | verb,动词              | 分配     | {simple}     | simple     | {分配}
 n     | noun,名词              | 问题     | {simple}     | simple     | {问题}
 u     | auxiliary,助词         | 。       | {}           |            | 
(111 rows)
```

## 性能对比

```sql
json_test=# explain analyse SELECT * FROM inner_alarm WHERE to_tsvector('jiebacfg', content_search) @@ to_tsquery('jiebacfg', '薏舌芳');
                                                               QUERY PLAN                                                               
----------------------------------------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on inner_alarm  (cost=21.55..737.60 rows=200 width=1688) (actual time=0.017..0.017 rows=1 loops=1)
   Recheck Cond: (to_tsvector('jiebacfg'::regconfig, content_search) @@ '''薏舌芳'''::tsquery)
   Heap Blocks: exact=1
   ->  Bitmap Index Scan on idx_gin_content_serach_jieba  (cost=0.00..21.50 rows=200 width=0) (actual time=0.013..0.013 rows=1 loops=1)
         Index Cond: (to_tsvector('jiebacfg'::regconfig, content_search) @@ '''薏舌芳'''::tsquery)
 Planning Time: 0.173 ms
 Execution Time: 0.051 ms
(7 rows)

json_test=# explain analyse SELECT * FROM inner_alarm WHERE to_tsvector('testzhcfg', content_search) @@ to_tsquery('testzhcfg', '薏舌芳');
                                                               QUERY PLAN                                                                
-----------------------------------------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on inner_alarm  (cost=28.01..32.27 rows=1 width=1688) (actual time=0.061..0.068 rows=7 loops=1)
   Recheck Cond: (to_tsvector('testzhcfg'::regconfig, content_search) @@ '''舌'' & ''芳'''::tsquery)
   Heap Blocks: exact=7
   ->  Bitmap Index Scan on idx_gin_content_serach_zhparser  (cost=0.00..28.01 rows=1 width=0) (actual time=0.055..0.055 rows=7 loops=1)
         Index Cond: (to_tsvector('testzhcfg'::regconfig, content_search) @@ '''舌'' & ''芳'''::tsquery)
 Planning Time: 0.127 ms
 Execution Time: 0.093 ms
(7 rows)

json_test=# explain analyse SELECT * FROM inner_alarm WHERE jsonb_to_tsvector('jiebacfg', content,'["string", "numeric"]') @@ to_tsquery('jiebacfg', '薏舌芳');
                                                            QUERY PLAN                                                            
----------------------------------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on inner_alarm  (cost=25.55..741.60 rows=200 width=1688) (actual time=0.016..0.016 rows=1 loops=1)
   Recheck Cond: (jsonb_to_tsvector('jiebacfg'::regconfig, content, '["string", "numeric"]'::jsonb) @@ '''薏舌芳'''::tsquery)
   Heap Blocks: exact=1
   ->  Bitmap Index Scan on idx_gin_content_jieba  (cost=0.00..25.50 rows=200 width=0) (actual time=0.013..0.013 rows=2 loops=1)
         Index Cond: (jsonb_to_tsvector('jiebacfg'::regconfig, content, '["string", "numeric"]'::jsonb) @@ '''薏舌芳'''::tsquery)
 Planning Time: 0.166 ms
 Execution Time: 0.038 ms
(7 rows)

json_test=# explain analyse SELECT * FROM inner_alarm WHERE jsonb_to_tsvector('testzhcfg', content,'["string", "numeric"]') @@ to_tsquery('testzhcfg', '薏舌芳');
                                                               QUERY PLAN                                                               
----------------------------------------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on inner_alarm  (cost=28.01..32.27 rows=1 width=1688) (actual time=0.059..0.066 rows=7 loops=1)
   Recheck Cond: (jsonb_to_tsvector('testzhcfg'::regconfig, content, '["string", "numeric"]'::jsonb) @@ '''舌'' & ''芳'''::tsquery)
   Heap Blocks: exact=7
   ->  Bitmap Index Scan on idx_gin_content_zhparser  (cost=0.00..28.01 rows=1 width=0) (actual time=0.054..0.054 rows=7 loops=1)
         Index Cond: (jsonb_to_tsvector('testzhcfg'::regconfig, content, '["string", "numeric"]'::jsonb) @@ '''舌'' & ''芳'''::tsquery)
 Planning Time: 0.120 ms
 Execution Time: 0.089 ms
(7 rows)
```

## 待续

- 查看两种插件对于实际数据的分词效果

```sql
# 长短语进一步拆分成短短语
json_test=# alter role all set zhparser.multi_short=on;
# 忽略标点符号
json_test=# alter role all set zhparser.punctuation_ignore=on;
```



