## 下载安装文件

```shell
$ wget https://github.com/istio/istio/releases/download/1.11.4/istio-1.11.4-linux-amd64.tar.gz
$ wget https://github.com/istio/istio/releases/download/1.11.4/istioctl-1.11.4-linux-amd64.tar.gz

$ tar -zxvf istio-1.11.4-linux-amd64.tar.gz
$ tar -zxvf istioctl-1.11.4-linux-amd64.tar.gz

$ sudo install -o root -g root -m 0755 istioctl /usr/local/bin/istioctl

$ cd istio-1.11.4

# 临时添加环境变量
$ export PATH=$PWD/bin:$PATH
# 改变环境变量
$ pwd
/opt/module/istio/istio-1.11.4/
$ vi ~/.bash_profile
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH
export KUBECONFIG=/etc/kubernetes/admin.conf
# 添加一行
export PATH=/opt/module/istio/istio-1.11.4/bin:$PATH
```

## Install Istio

1. For this installation, we use the `demo` [configuration profile](https://istio.io/latest/docs/setup/additional-setup/config-profiles/). It’s selected to have a good set of defaults for testing, but there are other profiles for production or performance testing.

   

   If your platform has a vendor-specific configuration profile, e.g., Openshift, use it in the following command, instead of the `demo` profile. Refer to your [platform instructions](https://istio.io/latest/docs/setup/platform-setup/) for details.

   ```shell
   $ istioctl install --set profile=demo -y
   ✔ Istio core installed
   ✔ Istiod installed
   ✔ Egress gateways installed
   ✔ Ingress gateways installed
   ✔ Installation complete
   Thank you for installing Istio 1.11.
   ```

   

2. Add a namespace label to instruct Istio to automatically inject Envoy sidecar proxies when you deploy your application later:

   ```shell
   $ kubectl label namespace default istio-injection=enabled
   namespace/default labeled
   ```

## Deploy the sample application

1. Deploy the [`Bookinfo` sample application](https://istio.io/latest/docs/examples/bookinfo/):

   ```shell
   $ kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
   service/details created
   serviceaccount/bookinfo-details created
   deployment.apps/details-v1 created
   service/ratings created
   serviceaccount/bookinfo-ratings created
   deployment.apps/ratings-v1 created
   service/reviews created
   serviceaccount/bookinfo-reviews created
   deployment.apps/reviews-v1 created
   deployment.apps/reviews-v2 created
   deployment.apps/reviews-v3 created
   service/productpage created
   serviceaccount/bookinfo-productpage created
   deployment.apps/productpage-v1 created
   ```

   

2. The application will start. As each pod becomes ready, the Istio sidecar will be deployed along with it.

   ```shell
   $ kubectl get services
   NAME          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
   details       ClusterIP   10.0.0.212      <none>        9080/TCP   29s
   kubernetes    ClusterIP   10.0.0.1        <none>        443/TCP    25m
   productpage   ClusterIP   10.0.0.57       <none>        9080/TCP   28s
   ratings       ClusterIP   10.0.0.33       <none>        9080/TCP   29s
   reviews       ClusterIP   10.0.0.28       <none>        9080/TCP   29s
   ```

   

   and

   ```
   $ kubectl get pods
   NAME                              READY   STATUS    RESTARTS   AGE
   details-v1-558b8b4b76-2llld       2/2     Running   0          2m41s
   productpage-v1-6987489c74-lpkgl   2/2     Running   0          2m40s
   ratings-v1-7dc98c7588-vzftc       2/2     Running   0          2m41s
   reviews-v1-7f99cc4496-gdxfn       2/2     Running   0          2m41s
   reviews-v2-7d79d5bd5d-8zzqd       2/2     Running   0          2m41s
   reviews-v3-7dbcdcbc56-m8dph       2/2     Running   0          2m41s
   ```

   

   

   Re-run the previous command and wait until all pods report READY `2/2` and STATUS `Running` before you go to the next step. This might take a few minutes depending on your platform.

3. Verify everything is working correctly up to this point. Run this command to see if the app is running inside the cluster and serving HTML pages by checking for the page title in the response:

   ```shell
   $ kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -sS productpage:9080/productpage | grep -o "<title>.*</title>"
   <title>Simple Bookstore App</title>
   ```

## Open the application to outside traffic

The Bookinfo application is deployed but not accessible from the outside. To make it accessible, you need to create an [Istio Ingress Gateway](https://istio.io/latest/docs/concepts/traffic-management/#gateways), which maps a path to a route at the edge of your mesh.

1. Associate this application with the Istio gateway:

   ```shell
   $ kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
   gateway.networking.istio.io/bookinfo-gateway created
   virtualservice.networking.istio.io/bookinfo created
   ```

   

2. Ensure that there are no issues with the configuration:

   ```shell
   $ istioctl analyze
   ✔ No validation issues found when analyzing namespace: default.
   ```
