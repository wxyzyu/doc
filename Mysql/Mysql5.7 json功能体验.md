## 新建测试表

```sql
CREATE TABLE `inner_alarm`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `create_time` bigint(20) NULL DEFAULT 0 COMMENT '创建时间',
  `create_type` int(11) NULL DEFAULT NULL COMMENT '创建类型',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人id',
  `create_source_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建来源id',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip',
  `content` json NULL COMMENT '内容，json格式，根据type类型解析',
  `content_version` int(11) NULL DEFAULT NULL COMMENT '内容版本，向前兼容',
  `first_discover_time` bigint(20) NULL DEFAULT 0 COMMENT '首次发现时间',
  `last_discover_time` bigint(20) NULL DEFAULT 0 COMMENT '最新发现时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `inner_status` int(11) NULL DEFAULT 0 COMMENT '内部状态',
  `device_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `software_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '软件id',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  `confirm_time` bigint(20) NULL DEFAULT NULL COMMENT '最后确认时间',
  `conform_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次确认人',
  `ignore_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次忽略时间',
  `ignore_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次忽略人',
  `handle_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次处置时间',
  `handle_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次处置人',
  `is_expired` int(11) NULL DEFAULT NULL COMMENT '是否已过期',
  `version` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '上次更新时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '上次更新人',
  `finish_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次完成时间',
  `finish_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次完成人',
  `reopen_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次重开时间',
  `reopen_type` int(11) NULL DEFAULT NULL COMMENT '最后一次重开类型',
  `reopen_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次重开人',
  `reopen_source_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后一次重开来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
```

## 插入测试数据

```java
package com.wxyzyu.data.generator;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.sql.*;
import java.util.UUID;

/**
 * @author wxyzyu
 * @version 0.1
 */
public class AlarmDataGenerator {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/json_test";
        String user = "root";
        String password = "root";
        //建立连接
        try(Connection connection = DriverManager.getConnection(url, user, password)){
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement("INSERT INTO `json_test`.`inner_alarm`(`id`, `title`, `content`) VALUES (?, ?, ?)");

            for (int i = 0; i < 40000; i++){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("url","www.baidu"+i+".com");
                jsonObject.put("name","name" + i);
                JSONArray array = new JSONArray();
                int size = i % 8;
                for (int j = 0; j < size + 1; j++){
                    array.add(UUID.randomUUID().toString().replace("-", ""));
                }
                jsonObject.put("array", array);
                jsonObject.put("desc", DescGenerator.generateDesc());
                ps.setObject(1,i);
                ps.setObject(2,"title" + i);
                ps.setObject(3,jsonObject.toJSONString());

                ps.addBatch();

                if ((i + 1) % 100 == 0){
                    ps.executeBatch();
                    connection.commit();

                    System.out.println("已插入数据：" + (i + 1));
                }
            }
        } catch (Exception e){
            System.err.println(e);
        }

    }
}
```

```java
package com.wxyzyu.data.generator;

import java.io.UnsupportedEncodingException;
import java.util.Random;

/**
 * @author wxyzyu
 * @version 0.1
 */
public class DescGenerator {

    public static String generateDesc() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();

        int length = random.nextInt(81) + 20;

        for (int i = 0; i <= length; i++){
            int hightPos, lowPos;

            hightPos = (176 + Math.abs(random.nextInt(72)));
            lowPos = (161 + Math.abs(random.nextInt(94)));
            byte[] b = new byte[2];
            b[0] = (new Integer(hightPos).byteValue());
            b[1] = (new Integer(lowPos).byteValue());
            stringBuilder.append(new String(b, "gb2312"));
        }
        
        return stringBuilder.toString();
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println(generateDesc());
    }
}
```

## 给json类型字段创建Generated Column和索引

- https://dev.mysql.com/doc/refman/5.7/en/create-table-secondary-indexes.html#json-column-indirect-index

```
mysql> ALTER TABLE inner_alarm ADD COLUMN content_desc varchar(255) GENERATED ALWAYS AS (content ->> '$.desc');
mysql> ALTER TABLE inner_alarm ADD COLUMN content_name varchar(255) GENERATED ALWAYS AS (content ->> '$.name');
```

![image-20200616164033558](Mysql5.7%20json%E5%8A%9F%E8%83%BD%E4%BD%93%E9%AA%8C.assets/image-20200616164033558.png) 

- 不能在Generated Column上添加fulltext类型的索引。

## json类型字段的CRUD

- 插入

```shell
#插入时当作string插入即可，mysql会自动校验json 的正确性
mysql> INSERT INTO `inner_alarm`(`id`, `title`, `content`) VALUES ("UUID", "title", json.toJSONString());
```

- 查询

```sql
mysql> select * from inner_alarm where content_desc like '%测%' and JSON_SEARCH(content, 'one', '%dd%', NULL, '$.array[*]') is not null and JSON_SEARCH(content, 'one', '%baidu12%', NULL, '$.url') is not null
```

- 修改

```sql
mysql> SELECT JSON_SET(@j, '$[1].b[0]', 1, '$[2][2]', 2);
+--------------------------------------------+
| JSON_SET(@j, '$[1].b[0]', 1, '$[2][2]', 2) |
+--------------------------------------------+
| ["a", {"b": [1, false]}, [10, 20, 2]]      |
+--------------------------------------------+
```

```sql
mysql> SELECT JSON_INSERT(@j, '$[1].b[0]', 1, '$[2][2]', 2);
+-----------------------------------------------+
| JSON_INSERT(@j, '$[1].b[0]', 1, '$[2][2]', 2) |
+-----------------------------------------------+
| ["a", {"b": [true, false]}, [10, 20, 2]]      |
+-----------------------------------------------+
```

```sql
mysql> SELECT JSON_REPLACE(@j, '$[1].b[0]', 1, '$[2][2]', 2);
+------------------------------------------------+
| JSON_REPLACE(@j, '$[1].b[0]', 1, '$[2][2]', 2) |
+------------------------------------------------+
| ["a", {"b": [1, false]}, [10, 20]]             |
+------------------------------------------------+
```

```sql
mysql> SELECT JSON_REMOVE(@j, '$[2]', '$[1].b[1]', '$[1].b[1]');
+---------------------------------------------------+
| JSON_REMOVE(@j, '$[2]', '$[1].b[1]', '$[1].b[1]') |
+---------------------------------------------------+
| ["a", {"b": [true]}]                              |
+---------------------------------------------------+
```

## 总结

- 如果json中field是固定的，可以使用Generated Column做索引。但是如果为了保证业务及时扩展字段，并且字段还要做查询，那mysql的json并不满足需求，起码数据表要创建Generated Column，并且程序也需要修改。
- 如果使用text存储json，在字段上创建fulltext的索引，不能满足不同字段查询不同的值。
- 如果在程序中遍历json中的所有key，用分隔符拼接成search_value插入数据库，也不能满足不同字段查询不同的值。

