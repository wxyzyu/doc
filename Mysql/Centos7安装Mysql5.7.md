## 查看并清理以前的安装

- As another example, if you have installed Percona using their own software repository, get a list of the installed Percona packages using the following command:

```terminal
shell> yum list installed Percona\*
Percona-Server-client-55.i686     5.5.39-rel36.0.el6          @percona-release-i386
Percona-Server-server-55.i686     5.5.39-rel36.0.el6          @percona-release-i386
Percona-Server-shared-55.i686     5.5.39-rel36.0.el6          @percona-release-i386
percona-release.noarch            0.1-3                       @/percona-release-0.1-3.noarch
```

From the command output, we can identify the installed packages (`Percona-Server-client`, `Percona-Server-server`, `Percona-Server-shared`, and `percona-release.noarch`) and the source of them (a nonnative software repository named `percona-release`).

- If you are not sure which third-party MySQL fork you have installed, this command should reveal it and list the RPM packages installed for it, as well as the third-party repository that supplies the packages:

```terminal
shell> yum --disablerepo=\* provides mysql\*
```

- The next step is to stop Yum from receiving packages from the nonnative repository. If the **yum-config-manager** utility is supported on your platform, you can, for example, use this command for stopping delivery from MariaDB:

```terminal
shell> sudo yum-config-manager --disable mariadb
```

- Use this command for stopping delivery from Percona:

```terminal
shell> sudo yum-config-manager --disable percona-release
```

- You can perform the same task by removing the entry for the software repository existing in one of the repository files under the `/etc/yum.repos.d/` directory. This is how the entry typically looks for MariaDB:

```ini
[mariadb] name = MariaDB
 baseurl = [base URL for repository]
 gpgkey = [URL for GPG key]
 gpgcheck =1
```

The entry is usually found in the file `/etc/yum.repos.d/MariaDB.repo` for MariaDB—delete the file, or remove entry from it (or from the file in which you find the entry).

Note

This step is not necessary for an installation that was configured with a Yum repository release package (like Percona) if you are going to remove the release package (`percona-release.noarch` for Percona), as shown in the uninstall command for Percona in Step 3 below.

- Uninstalling the Nonnative Third-Party MySQL Distribution of MySQL

The nonnative third-party MySQL distribution must first be uninstalled before you can use the MySQL Yum repository to install MySQL. For the MariaDB packages found in Step 2 above, uninstall them with the following command:

```terminal
shell> sudo yum remove MariaDB-common MariaDB-compat MariaDB-server
```

- For the Percona packages we found in Step 2 above:


```terminal
shell> sudo yum remove Percona-Server-client-55 Percona-Server-server-55 \
  Percona-Server-shared-55.i686 percona-release
```

## 下载安装最新的rpm

- https://dev.mysql.com/downloads/repo/yum/

![image-20200611164857906](Centos7%E5%AE%89%E8%A3%85Mysql5.7.assets/image-20200611164857906.png) 

- 安装rpm

```terminal
shell> sudo yum localinstall mysql80-community-release-el7-3.noarch.rpm
```

- 查看mysql版本

```terminal
shell> yum repolist all | grep mysql
mysql-cluster-7.5-community/x86_64 MySQL Cluster 7.5 Community   disabled
mysql-cluster-7.5-community-source MySQL Cluster 7.5 Community - disabled
mysql-cluster-7.6-community/x86_64 MySQL Cluster 7.6 Community   disabled
mysql-cluster-7.6-community-source MySQL Cluster 7.6 Community - disabled
mysql-cluster-8.0-community/x86_64 MySQL Cluster 8.0 Community   disabled
mysql-cluster-8.0-community-source MySQL Cluster 8.0 Community - disabled
mysql-connectors-community/x86_64  MySQL Connectors Community    enabled:    153
mysql-connectors-community-source  MySQL Connectors Community -  disabled
mysql-tools-community/x86_64       MySQL Tools Community         enabled:    110
mysql-tools-community-source       MySQL Tools Community - Sourc disabled
mysql-tools-preview/x86_64         MySQL Tools Preview           disabled
mysql-tools-preview-source         MySQL Tools Preview - Source  disabled
mysql55-community/x86_64           MySQL 5.5 Community Server    disabled
mysql55-community-source           MySQL 5.5 Community Server -  disabled
mysql56-community/x86_64           MySQL 5.6 Community Server    disabled
mysql56-community-source           MySQL 5.6 Community Server -  disabled
mysql57-community/x86_64           MySQL 5.7 Community Server    disabled
mysql57-community-source           MySQL 5.7 Community Server -  disabled
mysql80-community/x86_64           MySQL 8.0 Community Server    enabled:    177
mysql80-community-source           MySQL 8.0 Community Server -  disabled
```

- 变更mysql版本

```terminal
shell> sudo yum-config-manager --disable mysql80-community
shell> sudo yum-config-manager --enable mysql57-community
```

- 查看变更结果

```terminal
shell> yum repolist enabled | grep mysql
mysql-connectors-community/x86_64 MySQL Connectors Community                 153
mysql-tools-community/x86_64      MySQL Tools Community                      110
mysql57-community/x86_64          MySQL 5.7 Community Server                 424
```

## 安装mysql

```terminal
shell> sudo yum install mysql-community-server
```

![image-20200611170836440](Centos7%E5%AE%89%E8%A3%85Mysql5.7.assets/image-20200611170836440.png) 

## 启动mysql

Start the MySQL server with the following command:

```terminal
shell> sudo service mysqld start
Starting mysqld:[ OK ]
```

You can check the status of the MySQL server with the following command:

```terminal
shell> sudo service mysqld status
mysqld (pid 3066) is running.
```

## 修改密码

At the initial start up of the server, the following happens, given that the data directory of the server is empty:

- The server is initialized.

- SSL certificate and key files are generated in the data directory.

- [`validate_password`](https://dev.mysql.com/doc/refman/5.7/en/validate-password.html) is installed and enabled.

- A superuser account `'root'@'localhost` is created. A password for the superuser is set and stored in the error log file. To reveal it, use the following command:

  ```terminal
  shell> sudo grep 'temporary password' /var/log/mysqld.log
  ```

  Change the root password as soon as possible by logging in with the generated, temporary password and set a custom password for the superuser account:

  ```terminal
  shell> mysql -uroot -p
  ```

  ```sql
  mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass4!';
  ```

  Note

  [`validate_password`](https://dev.mysql.com/doc/refman/5.7/en/validate-password.html) is installed by default. The default password policy implemented by `validate_password` requires that passwords contain at least one uppercase letter, one lowercase letter, one digit, and one special character, and that the total password length is at least 8 characters.