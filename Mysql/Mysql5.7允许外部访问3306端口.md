## 防火墙配置

- 参考Centos 7 firewall 配置




## 查看mysql配置

mysql 5.7 默认只允许localhost访问root用户

```shell
mysql> use mysql;
mysql> select user,host from user;
+---------------+-----------+
| user          | host      |
+---------------+-----------+
| mysql.session | localhost |
| mysql.sys     | localhost |
| root          | localhost |
+---------------+-----------+
3 rows in set (0.00 sec)
```

```shell
shell> netstat -apn|grep 3306
tcp6       0      0 :::3306                 :::*                    LISTEN      1325/mysqld   
```

## 修改root用户访问控制

```shell
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'yourpassword' WITH GRANT OPTION;
mysql> flush privileges;
```

