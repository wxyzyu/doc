## 查看是否已经打开了binlog

```shell
mysql> show variables like '%log_bin%';
+---------------------------------+-------+
| Variable_name                   | Value |
+---------------------------------+-------+
| log_bin                         | OFF   |
| log_bin_basename                |       |
| log_bin_index                   |       |
| log_bin_trust_function_creators | OFF   |
| log_bin_use_v1_row_events       | OFF   |
| sql_log_bin                     | ON    |
+---------------------------------+-------+
6 rows in set (0.00 sec)
```

## 修改配置文件

```shell
shell> vi  /etc/my.cnf
```

```properties
# For advice on how to change settings please see
# http://dev.mysql.com/doc/refman/5.7/en/server-configuration-defaults.html

[mysqld]
#
# Remove leading # and set to the amount of RAM for the most important data
# cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
# innodb_buffer_pool_size = 128M
#
# Remove leading # to turn on a very important data integrity option: logging
# changes to the binary log between backups.
# log_bin
#
# Remove leading # to set options mainly useful for reporting servers.
# The server defaults are faster for transactions and fast SELECTs.
# Adjust sizes as needed, experiment to find the optimal values.
# join_buffer_size = 128M
# sort_buffer_size = 2M
# read_rnd_buffer_size = 2M
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

log-bin=mysql_bin
binlog-format=Row
server-id=1

```

```shell
shell> service mysqld restart
```

## 查看效果

```shell
mysql> show variables like '%log_bin%';
+---------------------------------+--------------------------------+
| Variable_name                   | Value                          |
+---------------------------------+--------------------------------+
| log_bin                         | ON                             |
| log_bin_basename                | /var/lib/mysql/mysql_bin       |
| log_bin_index                   | /var/lib/mysql/mysql_bin.index |
| log_bin_trust_function_creators | OFF                            |
| log_bin_use_v1_row_events       | OFF                            |
| sql_log_bin                     | ON                             |
+---------------------------------+--------------------------------+
6 rows in set (0.00 sec)
```

```shell
shell> ll /var/lib/mysql/
total 122960
-rw-r-----. 1 mysql mysql       56 Jun 12 01:11 auto.cnf
-rw-------. 1 mysql mysql     1680 Jun 12 01:11 ca-key.pem
-rw-r--r--. 1 mysql mysql     1112 Jun 12 01:11 ca.pem
-rw-r--r--. 1 mysql mysql     1112 Jun 12 01:11 client-cert.pem
-rw-------. 1 mysql mysql     1680 Jun 12 01:11 client-key.pem
-rw-r-----. 1 mysql mysql      446 Jun 16 19:11 ib_buffer_pool
-rw-r-----. 1 mysql mysql 12582912 Jun 16 19:16 ibdata1
-rw-r-----. 1 mysql mysql 50331648 Jun 16 19:17 ib_logfile0
-rw-r-----. 1 mysql mysql 50331648 Jun 12 01:11 ib_logfile1
-rw-r-----. 1 mysql mysql 12582912 Jun 16 19:14 ibtmp1
drwxr-x---. 2 mysql mysql     4096 Jun 12 01:11 mysql
-rw-r-----. 1 mysql mysql     1070 Jun 16 19:16 mysql_bin.000001
-rw-r-----. 1 mysql mysql       19 Jun 16 19:11 mysql_bin.index
srwxrwxrwx. 1 mysql mysql        0 Jun 16 19:11 mysql.sock
-rw-------. 1 mysql mysql        5 Jun 16 19:11 mysql.sock.lock
drwxr-x---. 2 mysql mysql     8192 Jun 12 01:11 performance_schema
-rw-------. 1 mysql mysql     1676 Jun 12 01:11 private_key.pem
-rw-r--r--. 1 mysql mysql      452 Jun 12 01:11 public_key.pem
-rw-r--r--. 1 mysql mysql     1112 J	un 12 01:11 server-cert.pem
-rw-------. 1 mysql mysql     1676 Jun 12 01:11 server-key.pem
drwxr-x---. 2 mysql mysql     8192 Jun 12 01:11 sys
drwxr-x---. 2 mysql mysql       52 Jun 16 19:14 test
```

![image-20200616112738252](Mysql5.7%E6%89%93%E5%BC%80binlog.assets/image-20200616112738252.png) 