## 新建测试表

```sql
CREATE TABLE `inner_alarm`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `create_time` bigint(20) NULL DEFAULT 0 COMMENT '创建时间',
  `create_type` int(11) NULL DEFAULT NULL COMMENT '创建类型',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人id',
  `create_source_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建来源id',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip',
  `content` text NULL COMMENT '内容，json格式，根据type类型解析',
  `content_version` int(11) NULL DEFAULT NULL COMMENT '内容版本，向前兼容',
  `first_discover_time` bigint(20) NULL DEFAULT 0 COMMENT '首次发现时间',
  `last_discover_time` bigint(20) NULL DEFAULT 0 COMMENT '最新发现时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `inner_status` int(11) NULL DEFAULT 0 COMMENT '内部状态',
  `device_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `software_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '软件id',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  `confirm_time` bigint(20) NULL DEFAULT NULL COMMENT '最后确认时间',
  `conform_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次确认人',
  `ignore_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次忽略时间',
  `ignore_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次忽略人',
  `handle_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次处置时间',
  `handle_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次处置人',
  `is_expired` int(11) NULL DEFAULT NULL COMMENT '是否已过期',
  `version` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '上次更新时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '上次更新人',
  `finish_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次完成时间',
  `finish_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次完成人',
  `reopen_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次重开时间',
  `reopen_type` int(11) NULL DEFAULT NULL COMMENT '最后一次重开类型',
  `reopen_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次重开人',
  `reopen_source_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后一次重开来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
```

## 插入测试数据

```java
package com.wxyzyu.data.generator;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.sql.*;
import java.util.UUID;

/**
 * @author wxyzyu
 * @version 0.1
 */
public class AlarmDataGenerator {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/json_test";
        String user = "root";
        String password = "root";
        //建立连接
        try(Connection connection = DriverManager.getConnection(url, user, password)){
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement("INSERT INTO `json_test`.`inner_alarm`(`id`, `title`, `content`) VALUES (?, ?, ?)");

            for (int i = 0; i < 40000; i++){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ttUrl","www.baidu"+i+".com");
                jsonObject.put("ttName","name" + i);
                JSONArray array = new JSONArray();
                int size = i % 8;
                for (int j = 0; j < size + 1; j++){
                    array.add(UUID.randomUUID().toString().replace("-", ""));
                }
                jsonObject.put("ttArray", array);
                jsonObject.put("ttDesc", DescGenerator.generateDesc());
                ps.setObject(1,i);
                ps.setObject(2,"ttTitle" + i);
                ps.setObject(3,jsonObject.toJSONString());

                ps.addBatch();

                if ((i + 1) % 100 == 0){
                    ps.executeBatch();
                    connection.commit();

                    System.out.println("已插入数据：" + (i + 1));
                }
            }
        } catch (Exception e){
            System.err.println(e);
        }

    }
}
```

```java
package com.wxyzyu.data.generator;

import java.io.UnsupportedEncodingException;
import java.util.Random;

/**
 * @author wxyzyu
 * @version 0.1
 */
public class DescGenerator {

    public static String generateDesc() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();

        int length = random.nextInt(81) + 20;

        for (int i = 0; i <= length; i++){
            int hightPos, lowPos;

            hightPos = (176 + Math.abs(random.nextInt(72)));
            lowPos = (161 + Math.abs(random.nextInt(94)));
            byte[] b = new byte[2];
            b[0] = (new Integer(hightPos).byteValue());
            b[1] = (new Integer(lowPos).byteValue());
            stringBuilder.append(new String(b, "gb2312"));
        }
        
        return stringBuilder.toString();
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println(generateDesc());
    }
}
```

## 创建索引

```
mysql> ALTER TABLE inner_alarm ADD FULLTEXT INDEX content_index (`content`) WITH PARSER ngram;
```

## 使用fulltext索引查询json内容

```shell
mysql> select MATCH(content) AGAINST('璃锌') as score,content from inner_alarm where MATCH(content) AGAINST('璃锌')
```
![image-20200618122739377](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618122739377.png)

```shell
#如果不指定匹配模式，则mysql会分词，按分数排序返回
mysql> select MATCH(content) AGAINST('name1012') as score,content from inner_alarm where MATCH(content) AGAINST('name1012')
```

![image-20200618122830853](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618122830853.png)

```shell
#如果制定使用boolean mode查询，则内容只至少要包含'name1012'才会被查询到
mysqlL> select MATCH(content) AGAINST('name1012') as score,content from inner_alarm where MATCH(content) AGAINST('name1012' IN BOOLEAN MODE)
```

![image-20200618123005379](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618123005379.png)

```shell
mysql> select MATCH(content) AGAINST('name101222222') as score,content from inner_alarm where MATCH(content) AGAINST('name101222222' IN BOOLEAN MODE)
```

![image-20200618123254328](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618123254328.png)

```shell
#mysql并不会索引符号
mysql> select MATCH(content) AGAINST('","') as score,content from inner_alarm where MATCH(content) AGAINST('","' IN BOOLEAN MODE)
```

![image-20200618123529774](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618123529774.png)

## 如何排除json中key对查询结果的影响

- PLAN A 新建自己的停用词表，参考https://dev.mysql.com/doc/refman/5.7/en/fulltext-stopwords.html

```sql
mysql> CREATE TABLE `inner_stopword`  (
  `value` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
```

```shell
mysql> INSERT INTO `json_test`.`inner_stopword`(`value`) VALUES ('tt');
mysql> mysql> INSERT INTO `json_test`.`inner_stopword`(`value`) VALUES ('ttUrl')
```

```shell
# 设置自定义停用词表
mysql> SET GLOBAL innodb_ft_server_stopword_table = 'json_test/inner_stopword';
# 查看是否启用了停用词
mysql> SHOW GLOBAL VARIABLES like 'innodb_ft_enable_stopword';
+---------------------------+-------+
| Variable_name             | Value |
+---------------------------+-------+
| innodb_ft_enable_stopword | ON    |
+---------------------------+-------+
1 row in set (0.12 sec)
# 插入数据
-------------------使用上面的程序-------------------------------
# 创建索引
mysql> ALTER TABLE inner_alarm ADD FULLTEXT INDEX content_index (`content`) WITH PARSER ngram;
# INFORMATION_SCHEMA INNODB_FT_INDEX_TABLE：This table is empty initially. Before querying it, set the value of the innodb_ft_aux_table
mysql> SET GLOBAL innodb_ft_aux_table = 'json_test/inner_alarm';
```

```shell
mysql> select MATCH(content) AGAINST('tt') as score,content from inner_alarm where MATCH(content) AGAINST('tt')
```

![image-20200618171634753](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618171634753.png) 

```shell
select MATCH(content) AGAINST('ttUrl') as score,content from inner_alarm where MATCH(content) AGAINST('ttUrl')
```

![image-20200618171715149](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200618171715149.png) 

- ttUrl没有起作用 https://dev.mysql.com/doc/refman/5.7/en/fulltext-search-ngram.html

- #### ngram Parser Stopword Handling

  The built-in MySQL full-text parser compares words to entries in the stopword list. If a word is equal to an entry in the stopword list, the word is excluded from the index. For the ngram parser, stopword handling is performed differently. Instead of excluding tokens that are equal to entries in the stopword list, the ngram parser excludes tokens that *contain* stopwords. For example, assuming [`ngram_token_size=2`](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_ngram_token_size), a document that contains “a,b” is parsed to “a,” and “,b”. If a comma (“,”) is defined as a stopword, both “a,” and “,b” are excluded from the index because they contain a comma.

  By default, the ngram parser uses the default stopword list, which contains a list of English stopwords. For a stopword list applicable to Chinese, Japanese, or Korean, you must create your own. For information about creating a stopword list, see [Section 12.9.4, “Full-Text Stopwords”](https://dev.mysql.com/doc/refman/5.7/en/fulltext-stopwords.html).

  **Stopwords greater in length than [`ngram_token_size`](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_ngram_token_size) are ignored.**

- 结论如果是用text类型的字段直接存储json内容，可以使用ngram进行分词全文检索，但是无法排除json里的field的字面值对查询的影响，除非每个field名称都不长于ngram_token_size的设置。json中field就变为 uu、dt之类的了，严重影响程序的可维护性。

------



- PLAN B 插入或更新数据库时，将json的所有的value 取出来使用空格分隔组成一个字符串，并插入数据库，在这个拼接的查询字符串上创建fulltext索引

## 新建测试表

```sql
CREATE TABLE `inner_alarm`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `create_time` bigint(20) NULL DEFAULT 0 COMMENT '创建时间',
  `create_type` int(11) NULL DEFAULT NULL COMMENT '创建类型',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人id',
  `create_source_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建来源id',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容，json格式，根据type类型解析',
  `content_search` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'json内容查询字段',
  `content_version` int(11) NULL DEFAULT NULL COMMENT '内容版本，向前兼容',
  `first_discover_time` bigint(20) NULL DEFAULT 0 COMMENT '首次发现时间',
  `last_discover_time` bigint(20) NULL DEFAULT 0 COMMENT '最新发现时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `inner_status` int(11) NULL DEFAULT 0 COMMENT '内部状态',
  `device_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `software_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '软件id',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  `confirm_time` bigint(20) NULL DEFAULT NULL COMMENT '最后确认时间',
  `conform_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次确认人',
  `ignore_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次忽略时间',
  `ignore_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次忽略人',
  `handle_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次处置时间',
  `handle_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次处置人',
  `is_expired` int(11) NULL DEFAULT NULL COMMENT '是否已过期',
  `version` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '上次更新时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '上次更新人',
  `finish_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次完成时间',
  `finish_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次完成人',
  `reopen_time` bigint(20) NULL DEFAULT NULL COMMENT '最后一次重开时间',
  `reopen_type` int(11) NULL DEFAULT NULL COMMENT '最后一次重开类型',
  `reopen_user_id` bigint(20) NULL DEFAULT NULL COMMENT '最后一次重开人',
  `reopen_source_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后一次重开来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
```
## 插入测试数据

```java
package com.wxyzyu.data.generator;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.sql.*;
import java.util.Map;
import java.util.UUID;

/**
 * @author wxyzyu
 * @version 0.2
 */
public class AlarmDataGenerator {
    public static <object> void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://192.168.8.11:3306/json_test";
        String user = "root";
        String password = "Oa123456!";
        //建立连接
        try(Connection connection = DriverManager.getConnection(url, user, password)){
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement("INSERT INTO `json_test`.`inner_alarm`(`id`, `title`, `content`, `content_search`) VALUES (?, ?, ?, ?)");

            for (int i = 0; i < 40000; i++){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("url","www.baidu"+i+".com");
                jsonObject.put("name","name" + i);
                JSONArray array = new JSONArray();
                int size = i % 8;
                for (int j = 0; j < size + 1; j++){
                    array.add(UUID.randomUUID().toString().replace("-", ""));
                }
                jsonObject.put("array", array);
                jsonObject.put("desc", DescGenerator.generateDesc());
                ps.setObject(1,i);
                ps.setObject(2,"title" + i);
                ps.setObject(3,jsonObject.toJSONString());
                StringBuilder stringBuilder = new StringBuilder();
                getPlainText(stringBuilder, jsonObject);
                ps.setObject(4,stringBuilder.toString());

                ps.addBatch();

                if ((i + 1) % 100 == 0){
                    ps.executeBatch();
                    connection.commit();
                    System.out.println("已插入数据：" + (i + 1));
                }
            }
        } catch (Exception e){
            System.err.println(e);
        }
    }

    public static void getPlainText(StringBuilder stringBuilder ,Object value){
        if (JSONObject.class.isInstance(value)){
            JSONObject jsonObject = (JSONObject)value;
            Map map = jsonObject.getInnerMap();
            for (Object obj: map.values()) {
                getPlainText(stringBuilder, obj);
            }

        } else if (JSONArray.class.isInstance(value)){
            JSONArray jsonArray = (JSONArray)value;
            for (int i = 0; i < jsonArray.size(); i++){
                Object obj = jsonArray.get(i);
                getPlainText(stringBuilder, obj);
            }
        } else {
            String text = null;
            if (null != value && !"".equals(text = (String.valueOf(value).trim()))){
                stringBuilder.append(" ").append(text);
            }
        }
    }
}
```

## 创建索引

```
mysql> ALTER TABLE inner_alarm ADD FULLTEXT INDEX content_search_index (`content_search`) WITH PARSER ngram;
```

## 使用fulltext索引查询json内容

```shell
mysql> select  MATCH(content_search) AGAINST('+name9130' IN BOOLEAN MODE) as score,content from inner_alarm where MATCH(content_search) AGAINST('+name9130' IN BOOLEAN MODE)
```

![image-20200619114330688](Mysql5.7%20%E4%BD%BF%E7%94%A8text%E7%B1%BB%E5%9E%8B%E5%AD%97%E6%AE%B5%E4%BF%9D%E5%AD%98json%E6%95%B0%E6%8D%AE%EF%BC%8C%E5%B9%B6%E5%88%9B%E5%BB%BAfulltext%E7%B4%A2%E5%BC%95.assets/image-20200619114330688.png)

