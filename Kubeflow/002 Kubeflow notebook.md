如果想为notebook的pod指定环境变量可以使用PodDefault

```shell
$ vi custom-env.yaml
```

```yaml
apiVersion: "kubeflow.org/v1alpha1"
kind: PodDefault
metadata:
  name: http-proxy-setting
  namespace: kubeflow-user-example-com # 只对指定的namespace起作用
spec:
  selector:
    matchLabels:
      inner-http-proxy: "true" # 这个selector为必选，作用是创建pod的时候自动添加label，系统会给有这些label的注入环境变量或者volume
  desc: "add http proxy"
  env:
    - name: env1
      value: custom1
    - name: env2
      value: your-value
```

```shell
$ vi add-gcp-secret.yaml
```

```yaml
apiVersion: "kubeflow.org/v1alpha1"
kind: PodDefault
metadata:
  name: add-gcp-secret
  namespace: kubeflow-user-example-com
spec:
 selector:
  matchLabels:
    add-gcp-secret: "true"
 desc: "add gcp credential"
 volumeMounts:
 - name: secret-volume
   mountPath: /secret/gcp
 volumes:
 - name: secret-volume
   secret:
    secretName: gcp-secret

```

![image-20211207165526625](002%20Kubeflow%20notebook.assets/image-20211207165526625.png)