## 本地集群安装Kubeflow

- 截止2021-12-07官方安装指导都不支持kubernetes 1.22
- 使用https://github.com/kubeflow/manifests#installation  安装

```shell
$ https://github.com/kubeflow/manifests.git
$ cd manifests
$ while ! kustomize build example | kubectl apply -f -; do echo "Retrying to apply resources"; sleep 10; done
```

安装之后无法通过其他机器访问

```shell
# 将istio-ingressgateway 设置为NodePort方式访问
$ kubectl get svc istio-ingressgateway -n istio-system
NAME                   TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)                                                                   
istio-ingressgateway   NodePort   10.1.144.223   <none>        15021:31739/TCP,80:30533/TCP,443:30224/TCP,31400:31582/TCP,15443:31982/TCP 
```

在浏览器访问http://192.168.30.200:30533/

![image-20211207163236374](001%20Install%20Kubeflow.assets/image-20211207163236374.png)

无法创建nodebook，错误信息提示"Could not find CSRF cookie XSRF-TOKEN in the request"

参考：https://github.com/kubeflow/kubeflow/issues/5803

```shell
$ vi cert-manager.yaml
```

```yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: kubeflow-ingressgateway-certs
  namespace: istio-system
spec:
  secretName: kubeflow-ingressgateway-certs
  issuerRef:
    name: kubeflow-self-signing-issuer  # 这需要注意，要跟你自己kubeflow namespace下的ClusterIssuer同名 kubectl get ClusterIssuer -n istio-system
    kind: ClusterIssuer
  commonName: kubeflow.example.com
  dnsNames:
    - kubeflow.example.com
```

```shell
$ vi kubeflow-gateway.yaml
```

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: kubeflow-gateway
  namespace: kubeflow
spec:
  selector:
    istio: ingressgateway
  servers:
  - hosts:
    - "*"
    port:
      name: http
      number: 80
      protocol: HTTP
    # Upgrade HTTP to HTTPS
    tls:
      httpsRedirect: true
  - hosts:
    - "*"
    port:
      name: https
      number: 443
      protocol: HTTPS
    tls:
      mode: SIMPLE
      credentialName: kubeflow-ingressgateway-certs
```

```shell
$ kubectl apply -f cert-manager.yaml
$ kubectl apply -f kubeflow-gateway.yaml
```

在浏览器访问https://192.168.30.200:30224/ 可以正常创建notebook了

原理是使用cert-manager的自签发证书，通过istio的ingressgateway:30224转发到kubeflow-gateway443,kubeflow的centraldashboard UI是使用istio的VirtualService定义的，VirtualService指定gateways为kubeflow-gateway

```yaml
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  annotations: {}
  labels:
    app: centraldashboard
    app.kubernetes.io/component: centraldashboard
    app.kubernetes.io/name: centraldashboard
    kustomize.component: centraldashboard
  name: centraldashboard
  namespace: kubeflow
  resourceVersion: '7891'
spec:
  gateways:
    - kubeflow-gateway
  hosts:
    - '*'
  http:
    - match:
        - uri:
            prefix: /
      rewrite:
        uri: /
      route:
        - destination:
            host: centraldashboard.kubeflow.svc.cluster.local
            port:
              number: 80


```



