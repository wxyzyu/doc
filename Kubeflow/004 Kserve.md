## 先在kubernetes集群安装MinIO

## 创建租户

```shell
$ kubectl create namespace tenant-1
```

注意内网测试环境要禁用TLS，因为后面Kserve从s3接口加载训练好的模型的时候，如果启用了TLS则要校验证书，就会报错（[botocore.exceptions.SSLError: SSL validation failed](https://stackoverflow.com/questions/63557500/botocore-exceptions-sslerror-ssl-validation-failed-on-windows)），因为证书是MinIO自己签发的所以校验不通过

![image-20211209100544321](004%20Kserve.assets/image-20211209100544321.png) 

创建成功后保存credentials.json文件

```json
{
    "console": [
        {
            "access_key": "fVjxZL9pj2UCqbKP",
            "secret_key": "maXDQWgx4tDOX9W6wGvCoduefaHIjiYK"
        }
    ]
}
```

检查服务是否正常启动

```shell
$ kubectl get svc -n tenant-1
NAME                          TYPE           CLUSTER-IP     EXTERNAL-IP  PORT(S)          AGE
minio                         LoadBalancer   10.1.235.91    <pending>    80:31473/TCP     12h
tenant-1-console              LoadBalancer   10.1.188.107   <pending>    9090:31824/TCP   12h
tenant-1-hl                   ClusterIP      None           <none>       9000/TCP         12h
tenant-1-log-hl-svc           ClusterIP      None           <none>       5432/TCP         12h
tenant-1-log-search-api       ClusterIP      10.1.2.121     <none>       8080/TCP         12h
tenant-1-prometheus-hl-svc    ClusterIP      None           <none>       9090/TCP         12h
```



## 创建bucket并上传预训练的模型

训练模型参考[Deploy Tensorflow Model with InferenceService](https://kserve.github.io/website/modelserving/v1beta1/tensorflow/)

将tenant-1下的tenant-1-console service改为NodePort方式访问，用上面保存"access_key"、"secret_key"登录

![image-20211209101009433](004%20Kserve.assets/image-20211209101009433.png) 

在浏览器打开，并创建bucket，名称为"kserve-examples"

![image-20211209101327735](004%20Kserve.assets/image-20211209101327735.png) 

目录结构如下，必须要有1这个版本文件夹，否则会报错（[Tensorflow serving No versions of servable  found under base path](https://stackoverflow.com/questions/45544928/tensorflow-serving-no-versions-of-servable-model-found-under-base-path)）

```shell
kserve-examples/
└── mnist
    └── 1
        ├── saved_model.pb
        └── virables
            ├── variables.data-00000-of-00001
            └── variables.index
```

模型也可以自己定义

## 安装独立的Kserve服务

注意：如果以前安装过Kubeflow，需要检查istio-system namespace下面有没有 knative-ingress-gateway，因为Kserve生成的服务默认通过这个网关访问，但是Kubeflow安装的KFServing使用的网关是istio-ingressgateway

先安装Knative及Istio：[Installing Knative Serving using YAML files](https://knative.dev/docs/install/serving/install-serving-with-yaml/)

注意cert-manager版本

```shell
# Kserve v0.7 还不支持cert-manager v1.6.1，KServe引用了cert-manager v1.6.1废弃的Issuer和Certificate版本
$ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.3/cert-manager.yaml
```

再安装Kserve

```shell
$ kubectl apply -f https://github.com/kserve/kserve/releases/download/v0.7.0/kserve.yaml
```

## 将模型部署为服务

```shell
$ vi s3cert.yaml
```

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: s3creds
  annotations:
     serving.kserve.io/s3-endpoint: minio.tenant-1.svc.cluster.local  # s3接口服务minio
     serving.kserve.io/s3-usehttps: "0"
     serving.kserve.io/s3-region: ""
     serving.kserve.io/s3-useanoncredential: "false"
     serving.kserve.io/s3-verifyssl: "0"
type: Opaque
stringData: # use `stringData` for raw credential string or `data` for base64 encoded string
  AWS_ACCESS_KEY_ID: fVjxZL9pj2UCqbKP
  AWS_SECRET_ACCESS_KEY: maXDQWgx4tDOX9W6wGvCoduefaHIjiYK
```

```shell
$ kubectl apply -n tenant-1 -f s3cert.yaml
$ vi s3sa.yaml
```

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: sa
secrets:
- name: s3creds
```

```shell
$ kubectl apply -n tenant-1 -f s3sa.yaml
$ vi mnist-s3.yaml
```

```yaml
apiVersion: "serving.kserve.io/v1beta1"
kind: "InferenceService"
metadata:
  name: "mnist-s3"
spec:
  predictor:
    serviceAccountName: sa
    tensorflow:
      storageUri: "s3://kserve-examples/mnist"
```

```shell
$ kubectl apply -n tenant-1 -f mnist-s3.yaml
$ kubectl get isvc -n tenant-1
NAME       URL                                    READY   PREV   LATEST   PREVROLLEDOUTREVISION   LATESTREADYREVISION                AGE
mnist-s3   http://mnist-s3.tenant-1.example.com   True           100                              mnist-s3-predictor-default-00001   11h
```

## 测试部署的模型

```shell
$ export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
$ export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
$ MODEL_NAME=mnist-s3
$ SERVICE_HOSTNAME=$(kubectl get inferenceservice ${MODEL_NAME} -n tenant-1 -o jsonpath='{.status.url}' | cut -d "/" -f 3)
$ curl -v -H "Host: ${SERVICE_HOSTNAME}" http://${INGRESS_HOST}:${INGRESS_PORT}/v1/models/$MODEL_NAME

* About to connect() to 192.168.30.202 port 31810 (#0)
*   Trying 192.168.30.202...
* Connected to 192.168.30.202 (192.168.30.202) port 31810 (#0)
> GET /v1/models/mnist-s3 HTTP/1.1
> User-Agent: curl/7.29.0
> Accept: */*
> Host: mnist-s3.tenant-1.example.com
>
< HTTP/1.1 200 OK
< content-length: 154
< content-type: application/json
< date: Thu, 09 Dec 2021 03:09:30 GMT
< x-envoy-upstream-service-time: 19
< server: istio-envoy
<
{
 "model_version_status": [
  {
   "version": "1",
   "state": "AVAILABLE",
   "status": {
    "error_code": "OK",
    "error_message": ""
   }
  }
 ]
}
* Connection #0 to host 192.168.30.202 left intact
```

## Debug

参考[Kubeflow for Machine Learning](https://www.oreilly.com/library/view/kubeflow-for-machine/9781492050117/)

The request flow when hitting your inference service, illustrated in figure is as follows:

1. Traffic arrives through the Istio ingress gateway when traffic is external and through the Istio cluster local gateway when traffic is internal.
2. KFServing creates an Istio VirtualService to specify its top-level routing rules for all of its components. As such, traffic routes to that top-level VirtualService from the gateway.
3. Knative creates an Istio virtual service to configure the gateway to route the user traffic to the desired revision. Upon opening up the destination rules, you will see that the destination is a Kubernetes service for the latest ready Knative revision.
4. Once the revision pods are ready, the Kubernetes service will send the request to the queue-proxy. If the queue proxy has more requests than it can handle, based on the concurrency of the KFServing container, then the autoscaler will create more pods to handle the additional requests.
5. Lastly, the queue proxy will send traffic to the KFServing controller.

![image-20211209111414082](004%20Kserve.assets/image-20211209111414082.png) 
