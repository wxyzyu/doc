## 使用elyra/kf-notebook 插件图形化编辑pipeline

- 创建notebook时指定custome image为elyra/kf-notebook

![image-20211207165858373](003%20Kubeflow%20elyra%20plugin.assets/image-20211207165858373.png)

教程参考：https://elyra.readthedocs.io/en/stable/index.html、https://github.com/elyra-ai/examples/tree/master/pipelines/introduction-to-generic-pipelines

![image-20211207170236452](003%20Kubeflow%20elyra%20plugin.assets/image-20211207170236452.png)

![image-20211207170452094](003%20Kubeflow%20elyra%20plugin.assets/image-20211207170452094.png)