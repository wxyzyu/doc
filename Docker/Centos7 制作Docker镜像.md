## [Dockerfile 例子](https://docs.docker.com/get-started/part2/#sample-dockerfile)

```shell
# Use the official image as a parent image.
FROM node:current-slim

# Set the working directory.
WORKDIR /usr/src/app

# Copy the file from your host to your current location.
COPY package.json .

# Run the command inside your image filesystem.
RUN npm install

# Add metadata to the image to describe which port the container is listening on at runtime.
EXPOSE 8080

# Run the specified command within the container.
CMD [ "npm", "start" ]

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .
```

解释

The dockerfile defined in this example takes the following steps:

- Start `FROM` the pre-existing `node:current-slim` image. This is an *official image*, built by the node.js vendors and validated by Docker to be a high-quality image containing the Node.js Long Term Support (LTS) interpreter and basic dependencies.
- Use `WORKDIR` to specify that all subsequent actions should be taken from the directory `/usr/src/app` *in your image filesystem* (never the host’s filesystem).
- `COPY` the file `package.json` from your host to the present location (`.`) in your image (so in this case, to `/usr/src/app/package.json`)
- `RUN` the command `npm install` inside your image filesystem (which will read `package.json` to determine your app’s node dependencies, and install them)
- `COPY` in the rest of your app’s source code from your host to your image filesystem.

## [制作第一个Docker镜像](https://docs.docker.com/get-started/part2/)

```shell
shell> cd /opt/module
shell> mkdir dockertest
shell> cd dockertest
shell> git clone https://github.com/dockersamples/node-bulletin-board
Cloning into 'node-bulletin-board'...
remote: Enumerating objects: 124, done.
remote: Total 124 (delta 0), reused 0 (delta 0), pack-reused 124
Receiving objects: 100% (124/124), 185.00 KiB | 135.00 KiB/s, done.
Resolving deltas: 100% (54/54), done.
shell> cd node-bulletin-board/bulletin-board-app
shell> su
shell> docker build --tag bulletinboard:1.0 .
Sending build context to Docker daemon  45.57kB
Step 1/7 : FROM node:current-slim
current-slim: Pulling from library/node
babf97a3f00a: Pull complete 
d81eb1809d95: Pull complete 
1d14a5c3c182: Pull complete 
21d157458a67: Pull complete 
b358ef6b56c5: Pull complete 
Digest: sha256:8551aaff36006da6e93535b60a4ec84efbbffde072039f764643f763ca86feb4
Status: Downloaded newer image for node:current-slim
 ---> c0f0d070c334
Step 2/7 : WORKDIR /usr/src/app
 ---> [Warning] IPv4 forwarding is disabled. Networking will not work.
 ---> Running in 034d3cdf243a
Removing intermediate container 034d3cdf243a
 ---> 2a6b3b075344
Step 3/7 : COPY package.json .
 ---> 2491a226d7be
Step 4/7 : RUN npm install
 ---> [Warning] IPv4 forwarding is disabled. Networking will not work.
 ---> Running in 76d5caa0710e
 # 解决无法下载依赖的问题
shell> vim  /usr/lib/sysctl.d/00-system.conf
# 添加 net.ipv4.ip_forward=1
shell> systemctl restart network
shell> docker build --tag bulletinboard:1.0 .
Sending build context to Docker daemon  45.57kB
Step 1/7 : FROM node:current-slim
 ---> c0f0d070c334
Step 2/7 : WORKDIR /usr/src/app
 ---> Using cache
 ---> 2a6b3b075344
Step 3/7 : COPY package.json .
 ---> Using cache
 ---> 2491a226d7be
Step 4/7 : RUN npm install
 ---> Running in f5251ff03603

> ejs@2.7.4 postinstall /usr/src/app/node_modules/ejs
> node ./postinstall.js

Thank you for installing EJS: built with the Jake JavaScript build tool (https://jakejs.com/)

npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN vue-event-bulletin@1.0.0 No repository field.
npm WARN The package morgan is included as both a dev and production dependency.

added 91 packages from 168 contributors and audited 92 packages in 7.909s
found 0 vulnerabilities

Removing intermediate container f5251ff03603
 ---> 7f197b241ba5
Step 5/7 : EXPOSE 8080
 ---> Running in e3a9abaa472a
Removing intermediate container e3a9abaa472a
 ---> e616337c592e
Step 6/7 : CMD [ "npm", "start" ]
 ---> Running in 50e4f8565ec7
Removing intermediate container 50e4f8565ec7
 ---> 8d627ce0e951
Step 7/7 : COPY . .
 ---> 4a6c6b25ab27
Successfully built 4a6c6b25ab27
Successfully tagged bulletinboard:1.0

```

## 运行image

```shell
shell> docker run --privileged=true --publish 8000:8080 --detach --name bb bulletinboard:1.0
4412fd2c0164c6bf6cbb10c8d103c5622d782aa0db8d7932b96ae212852b8541
[root@mesos bulletin-board-app]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
4412fd2c0164        bulletinboard:1.0   "docker-entrypoint.s…"   8 seconds ago       Up 7 seconds        0.0.0.0:8000->8080/tcp   bb

```

```shell
# 设置ip转发
shell> sysctl net.ipv4.ip_forward
net.ipv4.ip_forward = 0
shell> cat /etc/sysctl.conf
# sysctl settings are defined through files in
# /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
#
# Vendors settings live in /usr/lib/sysctl.d/.
# To override a whole file, create a new file with the same in
# /etc/sysctl.d/ and put new settings there. To override
# only specific settings, add a file with a lexically later
# name in /etc/sysctl.d/ and put new settings there.
#
# For more information, see sysctl.conf(5) and sysctl.d(5).
shell> cd /usr/lib/sysctl.d/
shell> ll
total 12
-rw-r--r--. 1 root root  316 Oct 22 11:21 00-system.conf
-rw-r--r--. 1 root root 1810 Apr  2  2020 10-default-yama-scope.conf
-rw-r--r--. 1 root root 1205 Aug  7 01:30 50-default.conf
shell> vim 00-system.conf
# 添加 net.ipv4.ip_forward=1
shell> systemctl restart network
shell> sysctl net.ipv4.ip_forward
net.ipv4.ip_forward = 1
```

```shell
# centos7 默认会用firewall,需要放开端口，并且添加转发规则
shell> docker inspect bb | grep Address
            "LinkLocalIPv6Address": "",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "GlobalIPv6Address": "",
            "IPAddress": "172.17.0.2",
            "MacAddress": "02:42:ac:11:00:02",
                    "IPAddress": "172.17.0.2",
                    "GlobalIPv6Address": "",
                    "MacAddress": "02:42:ac:11:00:02",
shell> firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33
  sources: 
  services: dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
shell> firewall-cmd --permanent --add-port=8080/tcp
success
shell> firewall-cmd --add-forward-port=port=8080:proto=tcp:toport=8080:toaddr=172.17.0.2 --permanent
success
shell> firewall-cmd --reload
success
shell>firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33
  sources: 
  services: dhcpv6-client ssh
  ports: 8080/tcp
  protocols: 
  masquerade: no
  forward-ports: port=8080:proto=tcp:toport=8080:toaddr=172.17.0.2
  source-ports: 
  icmp-blocks: 
  rich rules: 

```

## 删除转发规则

```
shell> firewall-cmd --permanent --remove-forward-port=port=8080:proto=tcp:toport=8080:toaddr=172.17.0.2
```

## 另一种方式解决从外部无法访问Docker

```shell
# 启动docker镜像时，docker会向iptables里添加转发的规则，也就是将对宿主机8888端口的访问转发到docker 容器中的8080端口上，因为centos7现在默认使用的是firewall，所以docker添加规则失败
shell> docker run -it --rm -p 8888:8080 tomcat:8-jdk8-adoptopenjdk-hotspot
docker: Error response from daemon: driver failed programming external connectivity on endpoint mystifying_cray (9c265b8aab33eb79789199bf5d5be2aa173a1634a1bbeb96e3ec8bef56bcd64f):  (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 8888 -j DNAT --to-destination 172.17.0.3:8080 ! -i docker0: iptables: No chain/target/match by that name.
 (exit status 1)).
# 禁用firewall，安装并启用iptables
shell> systemctl stop firewalld
shell> systemctl mask firewalld
Created symlink from /etc/systemd/system/firewalld.service to /dev/null.
shell> yum install iptables-services
shell> systemctl enable iptables
Created symlink from /etc/systemd/systemctl start  iptables
shell> systemctl start  iptables
shell> service iptables status
Redirecting to /bin/systemctl status iptables.service
● iptables.service - IPv4 firewall with iptables
   Loaded: loaded (/usr/lib/systemd/system/iptables.service; enabled; vendor preset: disabled)
   Active: active (exited) since Thu 2020-10-22 14:49:20 CST; 12s ago
  Process: 8055 ExecStart=/usr/libexec/iptables/iptables.init start (code=exited, status=0/SUCCESS)
 Main PID: 8055 (code=exited, status=0/SUCCESS)
/basic.target.wants/iptables.service to /usr/lib/systemd/system/iptables.service.
shell> systemctl restart docker
shell> docker run -it --rm -p 8888:8080 tomcat:8-jdk8-adoptopenjdk-hotspot
shell> iptables -L -t nat
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         
DOCKER     all  --  anywhere             anywhere             ADDRTYPE match dst-type LOCAL

Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
DOCKER     all  --  anywhere            !loopback/8           ADDRTYPE match dst-type LOCAL

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
MASQUERADE  all  --  172.17.0.0/16        anywhere            

Chain DOCKER (2 references)
target     prot opt source               destination         
RETURN     all  --  anywhere             anywhere   
```



## 验证

![image-20201022132621047](Centos7%20%E5%88%B6%E4%BD%9CDocker%E9%95%9C%E5%83%8F.assets/image-20201022132621047.png)

