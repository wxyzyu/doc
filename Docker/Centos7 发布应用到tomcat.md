## 使IDEA可以与Docker连接

```shell
shell> vi /lib/systemd/system/docker.service
#将ExecStart改为如下
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock
shell> systemctl daemon-reload
shell> systemctl restart docker
shell> firewall-cmd --zone=public --add-port=2375/tcp --permanent
shell> firewall-cmd --reload
```

![image-20201023170730340](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023170730340.png)

- 如上图显示，docker连接成功

![image-20201023170907383](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023170907383.png) 

- 双击docker，显示出容器和镜像

## 新建Maven web工程

![image-20201023171434547](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023171434547.png) 

## 生成war包

- 执行install生成项目的war包

![image-20201023171756885](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023171756885.png) 

- 生成的war包在“target/”下

![image-20201023171914636](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023171914636.png) 

## 在根目录创建DockerFile

```dockerfile
FROM tomcat:8-jdk8-adoptopenjdk-hotspot

ADD target/*.war /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]
```

- [参考链接](https://www.cprime.com/resources/blog/deploying-your-first-web-app-to-tomcat-on-docker/)

The FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions.

The ADD instruction copies new files, directories or remote file URLs from <src> and adds them to the filesystem of the image at the path <dest>.

In our case, we are adding the sample webapp and placing it in the folder /usr/local/tomcat/webapps/ on the container. That is because according to the Tomcat documentation, the War should be placed under CATALINA_BASE/webapps. It will be automatically expanded and deployed. From the Tomcat image documentation, we know that the default path CATALINA_BASE corresponds to /usr/local/tomcat on the container.

The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime.

The CMD instruction specifies what to run when the container (not the image) is run. In our case, TomCat server is started by running the shell script that starts the web container. There can only be one CMD instruction in a Dockerfile.

## 生成镜像

- 添加Run/Debug Configurations

![image-20201023172547632](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023172547632.png) 

- 运行，因为勾选了Run built image，所以创建完镜像之后，会启动容器

![image-20201023172756038](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023172756038.png) 

- 也可以去掉 Run built image 的勾选，手动启动容器

```
shell> docker run -itd -p 8080:8080 --name docker-tomcat-maven docker-tomcat-maven:0.1
```

## 在浏览器访问

![image-20201023173721678](Centos7%20%E5%8F%91%E5%B8%83%E5%BA%94%E7%94%A8%E5%88%B0tomcat.assets/image-20201023173721678.png) 