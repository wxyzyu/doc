## 控制Docker compose service 启动顺序

- You can control the order of service startup and shutdown with the [depends_on](https://docs.docker.com/compose/compose-file/compose-file-v3/#depends_on) option. Compose always starts and stops containers in dependency order, where dependencies are determined by `depends_on`, `links`, `volumes_from`, and `network_mode: "service:..."`.

- However, for startup Compose does not wait until a container is “ready” (whatever that means for your particular application) - only until it’s running. There’s a good reason for this.

## 方案1，[参考链接](https://github.com/huzhenghui/Docker-Compose-Startup-Order/blob/master/docker-compose.yml)

- 因为部署时私有注册中心管理接口不可访问，为了减少镜像的大小，所以将中间件跟程序分别制成镜像。启动时需要先将新版本程序镜像中的程序覆盖现有版本的程序，然后再启Tomcat之类的服务器。所以要保证覆盖操作执行完之后再启动服务器。
- 如果需要重启服务不能直接调用docker-compose up，需要先docker-compose down，再docker-compose up。因为重启的时候，rebuild container的时间不定，很可能造成touch /usr/src/app/update_test_update_flag 这个命令已经执行完了，但是后面的app容器还没有创建好，导致/usr/src/app/update_test_update_flag -ot /proc/1/cmdline一值为true，app服务一直启动不起来。现在改为只判断/usr/src/app/app_test_update_flag是否存在，每次启动update服务的时候都会执行rm -rf /usr/src/app/*，执行完程序更新之后执行touch /usr/src/app/update_test_update_flag

```yaml
version: "3.9"
services:
  update:
    image: update:v1
    ports:
      - "5000:5000"
    volumes:
      - app:/usr/src/app
    command: |
      sh -c '
      echo "service ============update============ starts at " $$(date "+%Y-%m-%d %H:%M:%S");
      rm -rf /usr/src/app/*;
      cp /usr/src/update/* /usr/src/app/;
      echo "service ============update============ finish at " $$(date "+%Y-%m-%d %H:%M:%S");
      touch /usr/src/app/update_test_update_flag;
      echo "logtime:" $$(date "+%Y-%m-%d %H:%M:%S") " file /usr/src/app/update_test_update_flag touched at" $$(date -r /usr/src/app/update_test_update_flag "+%Y-%m-%d %H:%M:%S");
      ping www.baidu.com >/dev/null 2>&1;'
  app:
    image: openjdk:17-jdk-alpine3.13
    volumes:
      - app:/usr/src/app
    working_dir: /usr/src/app
    restart: always
    depends_on:
      - update
    command: |
      sh -c '
      echo "service ============app============ prepares to start at " $$(date "+%Y-%m-%d %H:%M:%S");
      while [[ ! -f /usr/src/app/update_test_update_flag ]]; do echo "===========logtime:" $$(date "+%Y-%m-%d %H:%M:%S") " file /proc/1/cmdline touched at" $$(date -r /proc/1/cmdline "+%Y-%m-%d %H:%M:%S") & sleep 1; done;
      echo "service ============app============ starts at " $$(date "+%Y-%m-%d %H:%M:%S");
      rm -f /usr/src/app/update_test_update_flag;
      java -jar app.jar;
      echo "service ============app============ up at " $$(date "+%Y-%m-%d %H:%M:%S");
      touch /usr/src/app/app_test_update_flag;
      echo "logtime:" $$(date "+%Y-%m-%d %H:%M:%S") " file /usr/src/app/app_test_update_flag touched at" $$(date -r /usr/src/app/app_test_update_flag "+%Y-%m-%d %H:%M:%S");
      ping www.baidu.com >/dev/null 2>&1;'
  app2:
    image: openjdk:17-jdk-alpine3.13
    volumes:
      - app:/usr/src/app
    working_dir: /usr/src/app
    restart: always
    depends_on:
      - app
    command: |
      sh -c '
      echo "service ============app============ prepares to start at " $$(date "+%Y-%m-%d %H:%M:%S");
      while [[ ! -f /usr/src/app/app_test_update_flag ]]; do echo "===========logtime:" $$(date "+%Y-%m-%d %H:%M:%S") " file /proc/1/cmdline touched at" $$(date -r /proc/1/cmdline "+%Y-%m-%d %H:%M:%S") & sleep 1; done;
      echo "service ============app============ starts at " $$(date "+%Y-%m-%d %H:%M:%S");
      rm -f /usr/src/app/app_test_update_flag;
      java -jar app.jar;
      echo "service ============app============ up at " $$(date "+%Y-%m-%d %H:%M:%S");
      touch /usr/src/app/app2_test_update_flag;
      echo "logtime:" $$(date "+%Y-%m-%d %H:%M:%S") " file /usr/src/app/app2_test_update_flag touched at" $$(date -r /usr/src/app/app2_test_update_flag "+%Y-%m-%d %H:%M:%S");
      ping www.baidu.com >/dev/null 2>&1;'
volumes:
  app:
```

## 方案2，[参考链接](https://docs.docker.com/compose/startup-order/)

- 使用[wait-for-it.sh](https://github.com/vishnubob/wait-for-it)检查对应服务的端口可以访问之后再启动后续服务

```dockerfile
version: "2"
services:
  web:
    build: .
    ports:
      - "80:8000"
    depends_on:
      - "db"
    command: ["./wait-for-it.sh", "db:5432", "--", "python", "app.py"]
  db:
    image: postgres
```

