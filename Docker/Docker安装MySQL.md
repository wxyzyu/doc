## 编写MySQL配置文件和Docker compose文件，[参考](https://registry.hub.docker.com/_/mysql)
- /docker-entrypoint-initdb.d：When a container is started for the first time, a new database with the specified name will be created and initialized with the provided configuration variables. Furthermore, it will execute files with extensions `.sh`, `.sql` and `.sql.gz` that are found in `/docker-entrypoint-initdb.d`. Files will be executed in alphabetical order. You can easily populate your `mysql` services by [mounting a SQL dump into that directory](https://docs.docker.com/engine/tutorials/dockervolumes/#mount-a-host-file-as-a-data-volume) and provide [custom images](https://docs.docker.com/reference/builder/) with contributed data. SQL files will be imported by default to the database specified by the `MYSQL_DATABASE` variable.
- /etc/mysql/my.cnf：镜像中默认的配置文件，不需要修改

```sh
shell> cat /etc/mysql/my.cnf
...
#
# The MySQL  Server configuration file.
#
# For explanations see
# http://dev.mysql.com/doc/mysql/en/server-system-variables.html

# * IMPORTANT: Additional settings that can override those from this file!
#   The files must end with '.cnf', otherwise they'll be ignored.
#
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

shell> ls -l /etc/mysql/conf.d
total 12
-rw-r--r--. 1 root root 43 Mar 12 11:21 docker.cnf
-rw-r--r--. 1 root root  8 Jul  9  2016 mysql.cnf
-rw-r--r--. 1 root root 55 Jul  9  2016 mysqldump.cnf

shell> ls -l /etc/mysql/mysql.conf.d 
total 4
-rw-r--r--. 1 root root 1606 Mar 16 10:15 mysqld.cnf
```

- /etc/mysql/conf.d/mysql.cnf：镜像中默认的配置文件，不需要修改

```sh
shell> cat /etc/mysql/conf.d/mysql.cnf
[mysql]
```

- /etc/mysql/mysql.conf.d/mysqld.cnf：需要修改，支持binlog.将此文件放在/opt/modules/mysql/mysqlconfig/mysqld.cnf

```sh
shell> cat /etc/mysql/mysql.conf.d/mysqld.cnf
...
#
# The MySQL  Server configuration file.
#
# For explanations see
# http://dev.mysql.com/doc/mysql/en/server-system-variables.html

[mysqld]
pid-file        = /var/run/mysqld/mysqld.pid
socket          = /var/run/mysqld/mysqld.sock
datadir         = /var/lib/mysql
log-error       = /var/log/mysql/error.log
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

bind-address	= 0.0.0.0

log-bin=mysql_bin
binlog-format=Row
server-id=1

explicit_defaults_for_timestamp=true

```

- docker-compose-mysql.yml

```yaml
version: "3.9"
services:
  db:
    container_name: test_db
    image: mysql:5.7.33
    ports:
      - "3306:3306"
    command: --default-authentication-plugin=mysql_native_password --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
    restart: always
    volumes: 
      - /opt/modules/mysql/mysqldata:/var/lib/mysql
      - /opt/modules/mysql/mysqllog/error.log:/var/log/mysql/error.log
      - /opt/modules/mysql/mysqlconfig/mysqld.cnf:/etc/mysql/mysql.conf.d/mysqld.cnf
      - /opt/modules/mysql/mysqlinit:/docker-entrypoint-initdb.d
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: test
```

## 启动MySQL

```sh
shell> docker-compose  -f docker-compose-mysql.yml  up
Pulling db (mysql:5.7.33)...
5.7.33: Pulling from library/mysql
a4feded82f54: Pull complete
...
1604d620cec0: Pull complete
Digest: sha256:f3515b6a6502d872d5a37db78e4d225c0fcbf8da65d1faf8ce4609c92e2cbaf0
Status: Downloaded newer image for mysql:5.7.33
Creating test_db ... done
Attaching to test_db
db_1  | 2021-03-16 09:36:52+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.33-1debian9 started.
db_1  | 2021-03-16 09:36:52+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
db_1  | 2021-03-16 09:36:52+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.33-1debian9 started.
...
db_1  | 2021-03-16 09:37:17+00:00 [Note] [Entrypoint]: Creating database test
db_1  | 
db_1  | 2021-03-16 09:37:17+00:00 [Note] [Entrypoint]: /usr/local/bin/docker-entrypoint.sh: running /docker-entrypoint-initdb.d/test.sql
```

## 测试MySQL

```sh
shell> docker exec -it  test_db bash
root@579342a72fe9:/# mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 1
Server version: 5.7.33-log MySQL Community Server (GPL)

Copyright (c) 2000, 2021, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show variables like '%log_bin%';
+---------------------------------+--------------------------------+
| Variable_name                   | Value                          |
+---------------------------------+--------------------------------+
| log_bin                         | ON                             |
| log_bin_basename                | /var/lib/mysql/mysql_bin       |
| log_bin_index                   | /var/lib/mysql/mysql_bin.index |
| log_bin_trust_function_creators | OFF                            |
| log_bin_use_v1_row_events       | OFF                            |
| sql_log_bin                     | ON                             |
+---------------------------------+--------------------------------+
6 rows in set (0.00 sec)

```

## 注意事项

- 如果想将整个配置文件目录挂载进mysql容器，就必确认目录权限是755，要不然mysql就算能读取到配置文件，配置文件也不生效。

```yaml
version: "3.9"
services:
  db:
    container_name: test_db
    image: mysql:5.7.33
    ports:
      - "3306:3306"
    entrypoint:
      sh -c '
      ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime;
      echo "Asia/Shanghai" > /etc/timezone;
      chmod 755 /etc/mysql/mysql.conf.d/;
      docker-entrypoint.sh --default-authentication-plugin=mysql_native_password --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci;'
    restart: always
    volumes: 
      - /opt/modules/mysql/mysqldata:/var/lib/mysql
      - /opt/modules/mysql/mysqllog/error.log:/var/log/mysql/error.log
      - /opt/modules/mysql/mysqlconfig/:/etc/mysql/mysql.conf.d/
      - /opt/modules/mysql/mysqlinit:/docker-entrypoint-initdb.d
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: test
```

- 如果宿主机上的/opt/modules/mysql/mysqlconfig/文件夹权限是644，并且docker-compose文件中没有执行chmod 755 /etc/mysql/mysql.conf.d/，则启动时会有如下提示，说明配置文件没有起作用，binlog也没有打开

```shell
...
test_db    | 2021-04-20T04:53:36.742361Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
...

```

```shell
shell> docker exec -it sic_db bash
root@3d893eacb723:/# mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 7
Server version: 5.7.33 MySQL Community Server (GPL)

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show variables like '%log_bin%';
+---------------------------------+-------+
| Variable_name                   | Value |
+---------------------------------+-------+
| log_bin                         | OFF   |
| log_bin_basename                |       |
| log_bin_index                   |       |
| log_bin_trust_function_creators | OFF   |
| log_bin_use_v1_row_events       | OFF   |
| sql_log_bin                     | ON    |
+---------------------------------+-------+
6 rows in set (0.00 sec)
```

- 如果宿主机上的/opt/modules/mysql/mysqlconfig/文件夹权限是644，并且docker-compose文件中执行了chmod 755 /etc/mysql/mysql.conf.d/，则配置文件就会生效

```shell
shell> docker exec -it sic3-mysql bash
root@sic3-mysql:/# ls -al /etc/mysql/mysql.conf.d/
total 4
drwxr-xr-x. 2 root root   24 Apr 14 14:07 .
drwxr-xr-x. 4 root root   94 Mar 12 19:20 ..
-rw-r--r--. 1 root root 1966 Apr 20 11:02 mysqld.cnf #权限是644
root@sic3-mysql:/# ls -al /etc/mysql/             
total 8
drwxr-xr-x. 4 root root   94 Mar 12 19:20 .
drwxr-xr-x. 1 root root   39 Apr 20 13:03 ..
drwxr-xr-x. 2 root root   62 Mar 12 19:20 conf.d
lrwxrwxrwx. 1 root root   24 Mar 12 19:20 my.cnf -> /etc/alternatives/my.cnf
-rw-r--r--. 1 root root  839 Aug  3  2016 my.cnf.fallback
-rw-r--r--. 1 root root 1215 Dec 10 11:18 mysql.cnf
drwxr-xr-x. 2 root root   24 Apr 14 14:07 mysql.conf.d #权限是755
root@sic3-mysql:/# mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 271
Server version: 5.7.33-log MySQL Community Server (GPL)

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show variables like '%log_bin%';
+---------------------------------+--------------------------------+
| Variable_name                   | Value                          |
+---------------------------------+--------------------------------+
| log_bin                         | ON                             |
| log_bin_basename                | /var/lib/mysql/mysql_bin       |
| log_bin_index                   | /var/lib/mysql/mysql_bin.index |
| log_bin_trust_function_creators | OFF                            |
| log_bin_use_v1_row_events       | OFF                            |
| sql_log_bin                     | ON                             |
+---------------------------------+--------------------------------+
6 rows in set (0.00 sec)

mysql> 


```

- 另一种方式就是手动将宿主机的/opt/modules/mysql/mysqlconfig/文件夹的权限改为755，mysql也可以正常读取配置文件。不过还是建议只将mysqld.cnf这单个文件挂载进容器，这样不论/opt/modules/mysql/mysqlconfig/的权限是644还是755都不会影响mysql读取配置文件



