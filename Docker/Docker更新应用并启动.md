## 1制作更新镜像

- 文件结构

```sh
shell> tree .
.
├── app
│   ├── app.jar
│   └── start.sh
└── Dockerfile
```

- Dockerfile

```dockerfile
FROM alpine:3.12

ADD app/* /usr/src/update/

WORKDIR /usr/src/app/

CMD ["echo", "copied"]
```

- 构建镜像

```sh
shell> docker build -t update:v1
```

## 2运行更新镜像，将更新程序复制到程序volume

- 关于Docker的volume
- 可以使用docker volume create my-volume命令创建my-volume
- 也可以在docker run -v my-volume:/container/path 启动容器时指定，docker会自动创建my-volume
- docker volume ls命令查看所有volume
- docker volume inspect my-volume查看这个volume的具体信息

```sh
shell> docker volume inspect my-volume
[
    {
        "CreatedAt": "2021-03-09T02:05:00-05:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/my-volume/_data",
        "Name": "my-volume",
        "Options": {},
        "Scope": "local"
    }
]
```

- 如果my-volume不存在或者my-volume中为空，则执行docker run -v my-volume:/container/path之后会把/container/path中的文件都复制到my-volume中
- 如果my-volume中存在文件，则执行docker run -v my-volume:/container/path之后的效果等同于把/container/path中的文件全部删除，然后将my-volume中的文件全部复制到/container/path中

```sh
shell> docker run -it -v app:/usr/src/app --rm --name updatev1   update:v1 sh -c "rm -rf /usr/src/app/* & cp /usr/src/update/* /usr/src/app/"
```

## 3使用OpenJDK镜像运行程序

```sh
shell> docker run -it -v app:/usr/src/app --rm --name app --workdir /usr/src/app  openjdk:17-jdk-alpine3.12 sh -c "java -jar app.jar"
------current version v1------
```

## 4重新制作更新镜像

- 只更新app.jar文件，运行java -jar app.jar会输出“------current version v2------”

```sh
shell> docker build -t update:v2
```

## 5重新运行更新镜像，更新应用程序

```sh
shell> docker run -it -v app:/usr/src/app --rm --name updatev2   update:v2 sh -c "rm -rf /usr/src/app/* & cp /usr/src/update/* /usr/src/app/"
```

## 6重新使用OpenJDK镜像运行程序

```sh
shell> docker run -it -v app:/usr/src/app --rm --name app --workdir /usr/src/app  openjdk:17-jdk-alpine3.12 sh -c "java -jar app.jar"
------current version v2------
```

## 7总结

- 由于private registry不可访问，所以需要将测试完成的image放到能访问private registry的服务器，然后docker push给private registry。调查过使用docker save保存的image之后，发现没有办法手动删除image.tar文件中已经在private registry中存在的layer，因为删除之后docker load的时候会报错。为了减少每次生成image的大小，使用一个image分发应用程序，使用OpenJDK image或者Tomcat image运行程序。每次只更新应用image，image中除了基础的5M左右的Linux系统layer之外就只剩下应用layer了。