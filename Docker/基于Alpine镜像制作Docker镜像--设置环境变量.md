## 1Dockerfile：使用自定义脚本加载环境变量

```dockerfile
FROM alpine:3.7

ENV ENV="${HOME}/.ashrc"

RUN echo 'HTTP_PROXY="http://127.0.0.1:3001"' > "$ENV"
```

## 运行

```shell
shell> docker build -f Dockerfile . -t test
Sending build context to Docker daemon 2.048 kB
Step 1/3 : FROM alpine:3.7
Trying to pull repository docker.io/library/alpine ... 
3.7: Pulling from docker.io/library/alpine
5d20c808ce19: Pull complete 
Digest: sha256:8421d9a84432575381bfabd248f1eb56f3aa21d9d7cd2511583c68c9b7511d10
Status: Downloaded newer image for docker.io/alpine:3.7
 ---> 6d1ef012b567
Step 2/3 : ENV ENV "${HOME}/.ashrc"
 ---> Running in 6cb4fac727d9
 ---> 66b56aa74928
Removing intermediate container 6cb4fac727d9
Step 3/3 : RUN echo 'HTTP_PROXY="http://127.0.0.1:3001"' > "$ENV"
 ---> Running in 6e1b17a6cdd6

 ---> 4a571652198c
Removing intermediate container 6e1b17a6cdd6
Successfully built 4a571652198c

shell> docker run --rm -it test sh
/ # echo $HTTP_PROXY
http://127.0.0.1:3001
/ # exit

```

## 2Dockerfile：使用/etc/profile加载环境变量1

```dockerfile
FROM alpine:3.7

RUN echo 'HTTP_PROXY="http://127.0.0.1:3001"' > /etc/profile
```

## 运行

```shell
docker build -f Dockerfile . -t test2
Sending build context to Docker daemon 2.048 kB
Step 1/2 : FROM alpine:3.7
 ---> 6d1ef012b567
Step 2/2 : RUN echo 'HTTP_PROXY="http://127.0.0.1:3001"' > /etc/profile
 ---> Running in a1f7bdc56516

 ---> 959686ab126c
Removing intermediate container a1f7bdc56516
Successfully built 959686ab126c
# 使用sh -l 登录
shell> docker run --rm -it test2 sh -l
/ # echo $HTTP_PROXY
http://127.0.0.1:3001
/ # exit

```

## 3Dockerfile：使用/etc/profile加载环境变量2

```dockerfile
FROM alpine:3.7

ENV ENV="/etc/profile"

RUN echo 'HTTP_PROXY="http://127.0.0.1:3001"' > /etc/profile
```

## 运行

```shell
shell> docker build -f Dockerfile . -t test3
Sending build context to Docker daemon 2.048 kB
Step 1/3 : FROM alpine:3.7
 ---> 6d1ef012b567
Step 2/3 : ENV ENV "/etc/profile"
 ---> Running in 590bdc53cb5e
 ---> 34cee5b715c9
Removing intermediate container 590bdc53cb5e
Step 3/3 : RUN echo 'HTTP_PROXY="http://127.0.0.1:3001"' > /etc/profile
 ---> Running in b456b7e42ece

 ---> 2540853076ab
Removing intermediate container b456b7e42ece
Successfully built 2540853076ab
shell> docker run --rm -it test3 sh
/ # echo $HTTP_PROXY
http://127.0.0.1:3001
/ # exit
```

