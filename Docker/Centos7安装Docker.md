## Centos 7 安装Docker

参考：https://docs.docker.com/engine/install/centos/#install-from-a-package

### 1通过下载安装文件安装

```shell
# 下载链接：https://download.docker.com/linux/centos/7/x86_64/stable/Packages/
shell> cd /opt/module
shell> mkdir dockertest
shell> cd dockertest/
shell> pwd
/opt/module/dockertest
# 下载最新的三个rpm
shell> wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.3.7-3.1.el7.x86_64.rpm
shell> wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-19.03.9-3.el7.x86_64.rpm
shell> wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-cli-19.03.9-3.el7.x86_64.rpm
shell> ll
ll
total 93472
-rw-rw-r--. 1 wxyzyu wxyzyu 30374084 Oct  9 04:37 containerd.io-1.3.7-3.1.el7.x86_64.rpm
-rw-rw-r--. 1 wxyzyu wxyzyu 25286180 Jul 28 23:51 docker-ce-19.03.9-3.el7.x86_64.rpm
-rw-rw-r--. 1 wxyzyu wxyzyu 40047696 Jul 28 23:51 docker-ce-cli-19.03.9-3.el7.x86_64.rpm
# 安装三个rpm
shell> sudo yum install *.rpm
```

![image-20201021112938427](dockerinstall.assets/image-20201021112938427.png)

### 2通过命令行安装

```shell
# 添加yum源报错
shell> sudo yum-config-manager  --add-repo  https://download.docker.com/linux/centos/docker-ce.repo
  File "/bin/yum-config-manager", line 135
    except yum.Errors.RepoError, e:
                               ^
SyntaxError: invalid syntax
shell> python -V
Python 3.6.8
shell> su
shell> vim /usr/bin/yum-config-manager
# 修改为python2
#!/usr/bin/python2 -tt

import os, os.path
...
shell> sudo yum install docker-ce docker-ce-cli containerd.io
```

![image-20201021115212748](dockerinstall.assets/image-20201021115212748.png)

### 3启动Docker

```shell
shell> sudo systemctl start docker
shell> service docker status
Redirecting to /bin/systemctl status docker.service
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-10-21 19:55:07 CST; 9s ago
     Docs: https://docs.docker.com
 Main PID: 2020 (dockerd)
    Tasks: 10
   Memory: 37.8M
   CGroup: /system.slice/docker.service
           └─2020 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

4测试Docker

```shell
shell> sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:8c5aeeb6a5f3ba4883347d3747a7249f491766ca1caa47e5da5dfcf6b9b717c0
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

```

