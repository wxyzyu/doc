## 首先确保安装了WSL2

参考：https://docs.microsoft.com/zh-cn/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package

打开 PowerShell，然后在安装新的 Linux 发行版时运行以下命令，将 WSL 2 设置为默认版本：

```shell
$ wsl --set-default-version 2
```

## 安装Linux发行版

参考：https://www.microsoft.com/en-us/p/ubuntu-1804-lts/9n9tngvndl3q?rtc=1&activetab=pivot:overviewtab

## 安装Docker desktop

参考：https://docs.docker.com/desktop/windows/install/

## 创建Quarkus项目

参考：http://quarkus.io/guides/getting-started-reactive

## BUILDING A NATIVE EXECUTABLE

参考：http://quarkus.io/guides/building-native-image

## 解决问题

1. ```shell
   E:\projects\quarkus-2> ./mvnw package -Pnative -Dquarkus.native.container-build=true -Dquarkus.container-image.build=true
   [INFO] Scanning for projects...
   [INFO]
   [INFO] -----------------------< com.example:quarkus-2 >------------------------
   [INFO] Building quarkus-2 1.0-SNAPSHOT
   [INFO] --------------------------------[ jar ]---------------------------------
   [INFO] ------------------------------------------------------------------------
   [INFO] BUILD FAILURE
   [INFO] ------------------------------------------------------------------------
   [INFO] Total time:  3.568 s
   [INFO] Finished at: 2021-12-20T13:14:30+08:00
   [INFO] ------------------------------------------------------------------------
   [ERROR] Unknown lifecycle phase ".native.container-build=true". You must specify a valid lifecycle phase or a goal in the format <plugin-prefix>:<goal> or <plugin-group-id>:<plugin-artifact-id>[:<plugin-version>]:<goal>. Available lifecycle phases are: validate, initialize, generate-sources, process-sources, ge
   nerate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy,
    pre-clean, clean, post-clean, pre-site, site, post-site, site-deploy. -> [Help 1]
   [ERROR]
   [ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
   [ERROR] Re-run Maven using the -X switch to enable full debug logging.
   [ERROR]
   [ERROR] For more information about the errors and possible solutions, please read the following articles:
   [ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/LifecyclePhaseNotFoundException
   
   
   
   # 使用如下命令
    E:\projects\quarkus-2> ./mvnw package -Pnative --define quarkus.native.container-build=true --define quarkus.container-image.build=true
   ```

2. ```shell
   E:\projects\quarkus-2> docker build -f src/main/docker/Dockerfile.native -t quarkus-quickstart/getting-started .
   [+] Building 1.4s (3/3) FINISHED
   ......
   ------
    > [internal] load metadata for registry.access.redhat.com/ubi8/ubi-minimal:8.4:
   ------
   failed to solve with frontend dockerfile.v0: failed to create LLB definition: failed to do request: Head "https://registry.access.redhat.com/v2/ubi8/ubi-minimal/manifests/8.4": dial tcp: lookup registry.access.redhat.com on 192.168.65.5:53: no such host
   
   
   # 使用如下命令
   E:\projects\quarkus-2> docker search registry.access.redhat.com/ubi
   NAME                      DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
   ubi7/ubi-minimal          The Universal Base Image Init is designed to…   0
   ubi7/ubi-init             The Universal Base Image Init is designed to…   0
   ubi8-minimal              The Universal Base Image Minimal is a stripp…   0
   ubi7                      The Universal Base Image is designed and eng…   0
   ubi7-minimal              The Universal Base Image Minimal is a stripp…   0
   ubi8/ubi-micro            Provides the latest release of Micro Univers…   0
   ubi9-beta/ubi             Provides the latest release of Red Hat Unive…   0
   ubi9-beta/ubi-minimal     Provides the latest release of the Minimal R…   0
   ubi7/ubi                  The Universal Base Image is designed and eng…   0
   ubi8/ubi                  Provides the latest release of the Red Hat U…   0
   ubi8/ubi-minimal          Provides the latest release of the Minimal R…   0
   ubi8/ubi-init             Provides the latest release of the Red Hat U…   0
   
   
   # 之后再制作镜像就可以了
   E:\projects\quarkus-2> docker build -f src/main/docker/Dockerfile.native -t quarkus-quickstart/getting-started .
   [+] Building 12.8s (9/9) FINISHED
   ......
   ```

