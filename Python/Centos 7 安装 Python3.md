## 安装Python3

```shell
shell> yum install python3
```

![image-20200623154628899](Centos%207%20%E5%AE%89%E8%A3%85%20Python3.assets/image-20200623154628899.png)

```shell
shell> python -V
Python 2.7.5
shell> python3
Python 3.6.8 (default, Apr  2 2020, 13:34:55) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-39)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
shell> which python
/usr/bin/python
shell> ll /usr/bin/python*
lrwxrwxrwx. 1 root root     7 Apr 19 07:48 /usr/bin/python -> python2
lrwxrwxrwx. 1 root root     9 Apr 19 07:48 /usr/bin/python2 -> python2.7
-rwxr-xr-x. 1 root root  7216 Aug  7  2019 /usr/bin/python2.7
lrwxrwxrwx. 1 root root     9 Jun 23 23:39 /usr/bin/python3 -> python3.6
-rwxr-xr-x. 2 root root 11336 Apr  2 21:37 /usr/bin/python3.6
-rwxr-xr-x. 2 root root 11336 Apr  2 21:37 /usr/bin/python3.6m
```

## 切换软连接

```shell
shell> cd /usr/bin/
shell> rm /usr/bin/python
rm: remove symbolic link ‘/usr/bin/python’? y
shell> ll python*
lrwxrwxrwx. 1 root root     9 Apr 19 07:48 python2 -> python2.7
-rwxr-xr-x. 1 root root  7216 Aug  7  2019 python2.7
lrwxrwxrwx. 1 root root     9 Jun 23 23:39 python3 -> python3.6
-rwxr-xr-x. 2 root root 11336 Apr  2 21:37 python3.6
-rwxr-xr-x. 2 root root 11336 Apr  2 21:37 python3.6m
shell> ln -s /usr/bin/python3.6 /usr/bin/python
shell> python
Python 3.6.8 (default, Apr  2 2020, 13:34:55) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-39)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 

```

## 修复yum对Python2的引用

```shell
shell> vim /usr/bin/yum
# 这里改为Python2
#!/usr/bin/python2
import sys
try:
    import yum
except ImportError:
    print >> sys.stderr, """\
There was a problem importing one of the Python modules
required to run yum. The error leading to this problem was:

...
```

```shell
shell> vim /usr/libexec/urlgrabber-ext-down

# 这里改为Python2
#! /usr/bin/python2
#  A very simple external downloader
#  Copyright 2011-2012 Zdenek Pavlas

#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the
#      Free Software Foundation, Inc.,
#      59 Temple Place, Suite 330,
#      Boston, MA  02111-1307  USA
```

## 配置pip软连接

```shell
shell> ll pip*
-rwxr-xr-x. 1 root root 407 Mar 18 07:38 pip3
lrwxrwxrwx. 1 root root   9 Jun 23 23:39 pip-3 -> ./pip-3.6
lrwxrwxrwx. 1 root root   8 Jun 23 23:39 pip-3.6 -> ./pip3.6
-rwxr-xr-x. 1 root root 407 Mar 18 07:38 pip3.6
shell> ln -s /usr/bin/pip3 /usr/bin/pip
```

## pip使用国内镜像

```shell
 shell> mkdir ~/.pip
 shell> cd ~/.pip
 shell> vim pip.conf
 
[global]
index-url = http://mirrors.aliyun.com/pypi/simple
[install]
trusted-host = mirrors.aliyun.com
```

