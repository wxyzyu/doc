## 创建mysqly用户

```sql
mysql> CREATE USER 'usr_replica'@'%'  IDENTIFIED BY 'mysql_password';
mysql> GRANT ALL ON maxwell.* TO 'usr_replica';
mysql> GRANT RELOAD ON *.* to 'usr_replica';
mysql> GRANT REPLICATION CLIENT ON *.* to 'usr_replica';
mysql> GRANT REPLICATION SLAVE ON *.* to 'usr_replica';
mysql> GRANT SELECT ON *.* to 'usr_replica';
mysql> FLUSH PRIVILEGES;
mysql> show GRANTS for 'usr_replica'@'%';
+-----------------------------------------------------------------------------------------+
| Grants for usr_replica@%                                                                |
+-----------------------------------------------------------------------------------------+
| GRANT SELECT, RELOAD, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'usr_replica'@'%' |
| GRANT ALL PRIVILEGES ON `maxwell`.* TO 'usr_replica'@'%'                              |
+-----------------------------------------------------------------------------------------+
2 rows in set (0.01 sec)

```

## [下载源代码](https://github.com/zendesk/maxwell)

```shell
https://github.com/zendesk/maxwell.git
```

## 导入IDEA

![image-20200811165222891](Centos7%20Mysql5.7%E4%BD%BF%E7%94%A8Maxwell%E5%90%8C%E6%AD%A5%E6%95%B0%E6%8D%AE%E5%88%B0Elasticsearch7.8.assets/image-20200811165222891.png) 