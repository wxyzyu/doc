## [下载源代码](https://github.com/zendesk/maxwell)

```shell
https://github.com/zendesk/maxwell.git
```

## 导入IDEA

![image-20200811165222891](Debug%20Maxwell.assets/image-20200811165222891.png) 

## 修改配置文件

![image-20200811170221579](Debug%20Maxwell.assets/image-20200811170221579.png) 

- 将config.properties.example 复制并命名为config.properties
- 修改config.properties

```properties
log_level=info
# mysql login info
host=192.168.8.11
user=usr_replica
password=password

producer=stdout
```

## 添加启动项

![image-20200811171413794](Debug%20Maxwell.assets/image-20200811171413794.png) 

- 主程序入口可以从bin/maxwell脚本查看，为com.zendesk.maxwell.Maxwell
- VM options参考bin/maxwell脚本

```shell
-Dfile.encoding=UTF-8 -Dlog4j.shutdownCallbackRegistry=com.djdch.log4j.StaticShutdownCallbackRegistry
```

## 复制sql文件到target目录，按照下图复制过去

![image-20200811172337576](Debug%20Maxwell.assets/image-20200811172337576.png) 

- maxwell第一次启动会去配置文件配置的数据库创建maxwell自己的schema，如果不将文件复制过去会导致找不到sql文件而启动失败