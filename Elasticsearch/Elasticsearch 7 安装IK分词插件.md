## Centos 7 Elasticsearch7.8安装ik分词器

```shell
shell>  cd /opt/module/elasticsearch-7.8.0/
shell> ./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.8.0/elasticsearch-analysis-ik-7.8.0.zip
shell> ./bin/elasticsearch-plugin list
analysis-ik
shell> ps -ef | grep elastic
# restart elasticsearch
shell> kill -9 XXXX XXXX XXXX
shell> ./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_1 -Epath.data=/opt/module/esdata/node_1_data -Epath.logs=/opt/module/esdata/node_1_logs
    ./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_2 -Epath.data=/opt/module/esdata/node_2_data -Epath.logs=/opt/module/esdata/node_2_logs
    ./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_3 -Epath.data=/opt/module/esdata/node_3_data -Epath.logs=/opt/module/esdata/node_3_logs
```

## 测试分词器

```shell
shell> curl -XGET "http://192.168.8.11:9200/_analyze" -H 'Content-Type: application/json' -d'{  "analyzer": "ik_smart",  "text": "《资本论》全书共三卷，以剩余价值为中心，对资本主义进行了彻底的批判。第一卷研究了资本的生产过程，分析了剩余价值的生产问题。第二卷在资本生产过程的基础上研究了资本的流通过程，分析了剩余价值的实现问题。第三卷讲述了资本主义生产的总过程，分别研究了资本和剩余价值的具体形式。这一卷讲述的内.达到了资本的生产过程、流通过程和分配过程的高度统一，分析了剩余价值的分配问题。"}'
{
    "tokens": [
        {
            "token": "资本论",
            "start_offset": 1,
            "end_offset": 4,
            "type": "CN_WORD",
            "position": 0
        },
        {
            "token": "全书",
            "start_offset": 5,
            "end_offset": 7,
            "type": "CN_WORD",
            "position": 1
        },
        {
            "token": "共",
            "start_offset": 7,
            "end_offset": 8,
            "type": "CN_CHAR",
            "position": 2
        },
        {
            "token": "三卷",
            "start_offset": 8,
            "end_offset": 10,
            "type": "TYPE_CQUAN",
            "position": 3
        },
        {
            "token": "以",
            "start_offset": 11,
            "end_offset": 12,
            "type": "CN_CHAR",
            "position": 4
        },
        {
            "token": "剩余价值",
            "start_offset": 12,
            "end_offset": 16,
            "type": "CN_WORD",
            "position": 5
        },
        {
            "token": "为",
            "start_offset": 16,
            "end_offset": 17,
            "type": "CN_CHAR",
            "position": 6
        },
        {
            "token": "中心",
            "start_offset": 17,
            "end_offset": 19,
            "type": "CN_WORD",
            "position": 7
        },
        {
            "token": "对",
            "start_offset": 20,
            "end_offset": 21,
            "type": "CN_CHAR",
            "position": 8
        },
        {
            "token": "资本主义",
            "start_offset": 21,
            "end_offset": 25,
            "type": "CN_WORD",
            "position": 9
        },
        {
            "token": "进行了",
            "start_offset": 25,
            "end_offset": 28,
            "type": "CN_WORD",
            "position": 10
        },
        {
            "token": "彻底",
            "start_offset": 28,
            "end_offset": 30,
            "type": "CN_WORD",
            "position": 11
        },
        {
            "token": "的",
            "start_offset": 30,
            "end_offset": 31,
            "type": "CN_CHAR",
            "position": 12
        },
        {
            "token": "批判",
            "start_offset": 31,
            "end_offset": 33,
            "type": "CN_WORD",
            "position": 13
        },
        {
            "token": "第一卷",
            "start_offset": 34,
            "end_offset": 37,
            "type": "CN_WORD",
            "position": 14
        },
        {
            "token": "研究",
            "start_offset": 37,
            "end_offset": 39,
            "type": "CN_WORD",
            "position": 15
        },
        {
            "token": "了",
            "start_offset": 39,
            "end_offset": 40,
            "type": "CN_CHAR",
            "position": 16
        },
        {
            "token": "资本",
            "start_offset": 40,
            "end_offset": 42,
            "type": "CN_WORD",
            "position": 17
        },
        {
            "token": "的",
            "start_offset": 42,
            "end_offset": 43,
            "type": "CN_CHAR",
            "position": 18
        },
        {
            "token": "生产过程",
            "start_offset": 43,
            "end_offset": 47,
            "type": "CN_WORD",
            "position": 19
        },
        {
            "token": "分析",
            "start_offset": 48,
            "end_offset": 50,
            "type": "CN_WORD",
            "position": 20
        },
        {
            "token": "了",
            "start_offset": 50,
            "end_offset": 51,
            "type": "CN_CHAR",
            "position": 21
        },
        {
            "token": "剩余价值",
            "start_offset": 51,
            "end_offset": 55,
            "type": "CN_WORD",
            "position": 22
        },
        {
            "token": "的",
            "start_offset": 55,
            "end_offset": 56,
            "type": "CN_CHAR",
            "position": 23
        },
        {
            "token": "生产",
            "start_offset": 56,
            "end_offset": 58,
            "type": "CN_WORD",
            "position": 24
        },
        {
            "token": "问题",
            "start_offset": 58,
            "end_offset": 60,
            "type": "CN_WORD",
            "position": 25
        },
        {
            "token": "第二卷",
            "start_offset": 61,
            "end_offset": 64,
            "type": "CN_WORD",
            "position": 26
        },
        {
            "token": "在",
            "start_offset": 64,
            "end_offset": 65,
            "type": "CN_CHAR",
            "position": 27
        },
        {
            "token": "资本",
            "start_offset": 65,
            "end_offset": 67,
            "type": "CN_WORD",
            "position": 28
        },
        {
            "token": "生产过程",
            "start_offset": 67,
            "end_offset": 71,
            "type": "CN_WORD",
            "position": 29
        },
        {
            "token": "的",
            "start_offset": 71,
            "end_offset": 72,
            "type": "CN_CHAR",
            "position": 30
        },
        {
            "token": "基础上",
            "start_offset": 72,
            "end_offset": 75,
            "type": "CN_WORD",
            "position": 31
        },
        {
            "token": "研究",
            "start_offset": 75,
            "end_offset": 77,
            "type": "CN_WORD",
            "position": 32
        },
        {
            "token": "了",
            "start_offset": 77,
            "end_offset": 78,
            "type": "CN_CHAR",
            "position": 33
        },
        {
            "token": "资本",
            "start_offset": 78,
            "end_offset": 80,
            "type": "CN_WORD",
            "position": 34
        },
        {
            "token": "的",
            "start_offset": 80,
            "end_offset": 81,
            "type": "CN_CHAR",
            "position": 35
        },
        {
            "token": "流通",
            "start_offset": 81,
            "end_offset": 83,
            "type": "CN_WORD",
            "position": 36
        },
        {
            "token": "过程",
            "start_offset": 83,
            "end_offset": 85,
            "type": "CN_WORD",
            "position": 37
        },
        {
            "token": "分析",
            "start_offset": 86,
            "end_offset": 88,
            "type": "CN_WORD",
            "position": 38
        },
        {
            "token": "了",
            "start_offset": 88,
            "end_offset": 89,
            "type": "CN_CHAR",
            "position": 39
        },
        {
            "token": "剩余价值",
            "start_offset": 89,
            "end_offset": 93,
            "type": "CN_WORD",
            "position": 40
        },
        {
            "token": "的",
            "start_offset": 93,
            "end_offset": 94,
            "type": "CN_CHAR",
            "position": 41
        },
        {
            "token": "实现",
            "start_offset": 94,
            "end_offset": 96,
            "type": "CN_WORD",
            "position": 42
        },
        {
            "token": "问题",
            "start_offset": 96,
            "end_offset": 98,
            "type": "CN_WORD",
            "position": 43
        },
        {
            "token": "第三卷",
            "start_offset": 99,
            "end_offset": 102,
            "type": "CN_WORD",
            "position": 44
        },
        {
            "token": "讲述",
            "start_offset": 102,
            "end_offset": 104,
            "type": "CN_WORD",
            "position": 45
        },
        {
            "token": "了",
            "start_offset": 104,
            "end_offset": 105,
            "type": "CN_CHAR",
            "position": 46
        },
        {
            "token": "资本主义",
            "start_offset": 105,
            "end_offset": 109,
            "type": "CN_WORD",
            "position": 47
        },
        {
            "token": "生产",
            "start_offset": 109,
            "end_offset": 111,
            "type": "CN_WORD",
            "position": 48
        },
        {
            "token": "的",
            "start_offset": 111,
            "end_offset": 112,
            "type": "CN_CHAR",
            "position": 49
        },
        {
            "token": "总",
            "start_offset": 112,
            "end_offset": 113,
            "type": "CN_CHAR",
            "position": 50
        },
        {
            "token": "过程",
            "start_offset": 113,
            "end_offset": 115,
            "type": "CN_WORD",
            "position": 51
        },
        {
            "token": "分别",
            "start_offset": 116,
            "end_offset": 118,
            "type": "CN_WORD",
            "position": 52
        },
        {
            "token": "研究",
            "start_offset": 118,
            "end_offset": 120,
            "type": "CN_WORD",
            "position": 53
        },
        {
            "token": "了",
            "start_offset": 120,
            "end_offset": 121,
            "type": "CN_CHAR",
            "position": 54
        },
        {
            "token": "资本",
            "start_offset": 121,
            "end_offset": 123,
            "type": "CN_WORD",
            "position": 55
        },
        {
            "token": "和",
            "start_offset": 123,
            "end_offset": 124,
            "type": "CN_CHAR",
            "position": 56
        },
        {
            "token": "剩余价值",
            "start_offset": 124,
            "end_offset": 128,
            "type": "CN_WORD",
            "position": 57
        },
        {
            "token": "的",
            "start_offset": 128,
            "end_offset": 129,
            "type": "CN_CHAR",
            "position": 58
        },
        {
            "token": "具体",
            "start_offset": 129,
            "end_offset": 131,
            "type": "CN_WORD",
            "position": 59
        },
        {
            "token": "形式",
            "start_offset": 131,
            "end_offset": 133,
            "type": "CN_WORD",
            "position": 60
        },
        {
            "token": "这一",
            "start_offset": 134,
            "end_offset": 136,
            "type": "CN_WORD",
            "position": 61
        },
        {
            "token": "卷",
            "start_offset": 136,
            "end_offset": 137,
            "type": "COUNT",
            "position": 62
        },
        {
            "token": "讲述",
            "start_offset": 137,
            "end_offset": 139,
            "type": "CN_WORD",
            "position": 63
        },
        {
            "token": "的",
            "start_offset": 139,
            "end_offset": 140,
            "type": "CN_CHAR",
            "position": 64
        },
        {
            "token": "内",
            "start_offset": 140,
            "end_offset": 141,
            "type": "CN_CHAR",
            "position": 65
        },
        {
            "token": "达",
            "start_offset": 142,
            "end_offset": 143,
            "type": "CN_CHAR",
            "position": 66
        },
        {
            "token": "到了",
            "start_offset": 143,
            "end_offset": 145,
            "type": "CN_WORD",
            "position": 67
        },
        {
            "token": "资本",
            "start_offset": 145,
            "end_offset": 147,
            "type": "CN_WORD",
            "position": 68
        },
        {
            "token": "的",
            "start_offset": 147,
            "end_offset": 148,
            "type": "CN_CHAR",
            "position": 69
        },
        {
            "token": "生产过程",
            "start_offset": 148,
            "end_offset": 152,
            "type": "CN_WORD",
            "position": 70
        },
        {
            "token": "流通",
            "start_offset": 153,
            "end_offset": 155,
            "type": "CN_WORD",
            "position": 71
        },
        {
            "token": "过程",
            "start_offset": 155,
            "end_offset": 157,
            "type": "CN_WORD",
            "position": 72
        },
        {
            "token": "和",
            "start_offset": 157,
            "end_offset": 158,
            "type": "CN_CHAR",
            "position": 73
        },
        {
            "token": "分配",
            "start_offset": 158,
            "end_offset": 160,
            "type": "CN_WORD",
            "position": 74
        },
        {
            "token": "过程",
            "start_offset": 160,
            "end_offset": 162,
            "type": "CN_WORD",
            "position": 75
        },
        {
            "token": "的",
            "start_offset": 162,
            "end_offset": 163,
            "type": "CN_CHAR",
            "position": 76
        },
        {
            "token": "高度",
            "start_offset": 163,
            "end_offset": 165,
            "type": "CN_WORD",
            "position": 77
        },
        {
            "token": "统一",
            "start_offset": 165,
            "end_offset": 167,
            "type": "CN_WORD",
            "position": 78
        },
        {
            "token": "分析",
            "start_offset": 168,
            "end_offset": 170,
            "type": "CN_WORD",
            "position": 79
        },
        {
            "token": "了",
            "start_offset": 170,
            "end_offset": 171,
            "type": "CN_CHAR",
            "position": 80
        },
        {
            "token": "剩余价值",
            "start_offset": 171,
            "end_offset": 175,
            "type": "CN_WORD",
            "position": 81
        },
        {
            "token": "的",
            "start_offset": 175,
            "end_offset": 176,
            "type": "CN_CHAR",
            "position": 82
        },
        {
            "token": "分配",
            "start_offset": 176,
            "end_offset": 178,
            "type": "CN_WORD",
            "position": 83
        },
        {
            "token": "问题",
            "start_offset": 178,
            "end_offset": 180,
            "type": "CN_WORD",
            "position": 84
        }
    ]
}
```

