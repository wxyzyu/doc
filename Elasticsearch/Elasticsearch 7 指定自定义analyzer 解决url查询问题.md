## 问题：域名如果包含字母和数字无法查询到，例如https://site.ip138.com/bn10086.com/beian.htm

web_url字段指定分词器为url_analyzer。其他字段没有指定，会在存储的时候默认使用ik_max_word分词器，在查询的时候默认使用ik_smart分词器

```json
PUT analyzer_url_test
{
  "settings": {
    "analysis": {
      "analyzer": {
        "default_search" : {
          "type": "custom",
          "tokenizer": "ik_smart",
          "filter": [
            "lowercase",
            "stopwords_filter"
          ]
        },
        "default" : {
          "type": "custom",
          "tokenizer": "ik_max_word",
          "filter": [
            "lowercase",
            "stopwords_filter"
          ]
        },
        "url_analyzer": { 
          "type": "custom",
          "tokenizer": "url_tokenizer",
          "filter": [
            "lowercase",
            "stopwords_filter"
          ]
        }
      },
      "tokenizer": {
        "url_tokenizer":{
            "type": "char_group",
            "tokenize_on_chars": [
              ":",
              "/",
              ".",
              "?",
              "=",
              "#"
            ]
          }
      },
      "filter" : {
        "stopwords_filter" : {
          "type" : "stop",
          "stopwords" : ["https", "http", "ftp", "www", "html","com", "cn"]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "web_url" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          },
          "analyzer" : "url_analyzer",
          # serach_analyzer与analyzer设置同样的值，生成的mapping会忽略掉search_analyzer属性
          # 参考https://www.elastic.co/guide/en/elasticsearch/reference/7.10/search-analyzer.html
          # By default, queries will use the analyzer defined in the field mapping, 
          # but this can be overridden with the search_analyzer setting
          "search_analyzer" : "url_analyzer"
        }
    }
  }
}
```

- 插入测试数据


```
POST analyzer_url_test/_doc/1
{
  
    "web_url":"https://site.ip138.com/bn10086.com/beian.htm?param=123456#bock1",
    "test":"https://site.ip138.com/bn10086.com/beian.htm?param=123456#bock1"
  
}
```

- 查看索引信息


```json
GET analyzer_url_test
{
  "analyzer_url_test" : {
    "aliases" : { },
    "mappings" : {
      "properties" : {
        "test" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          },
          "analyzer" : "default",
          "search_analyzer" : "default_search"
        },
        "web_url" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          },
          "analyzer" : "url_analyzer"
           # 这里因为建立索引是analyzer跟search_analyzer指定的是相同的值，所以search_analyzer属性被忽略了
        }
      }
    },
    "settings" : {
      "index" : {
        "number_of_shards" : "1",
        "provided_name" : "analyzer_url_test",
        "creation_date" : "1610551610548",
        "analysis" : {
          "filter" : {
            "stopwords_filter" : {
              "type" : "stop",
              "stopwords" : [
                "http",
                "ftp",
                "www",
                "html",
                "com"
              ]
            }
          },
          "analyzer" : {
            "default_search" : {
              "type" : "ik_smart"
            },
            "default" : {
              "type" : "ik_max_word"
            },
            "url_analyzer" : {
              "filter" : [
                "lowercase",
                "stopwords_filter"
              ],
              "type" : "custom",
              "tokenizer" : "url_tokenizer"
            }
          },
          "tokenizer" : {
            "url_tokenizer" : {
              "type" : "char_group",
              "tokenize_on_chars" : [
                "/",
                ".",
                "?",
                "=",
                "#"
              ]
            }
          }
        },
        "number_of_replicas" : "1",
        "uuid" : "9JkZPOaCTWmHOyuYvsMDrg",
        "version" : {
          "created" : "7080099"
        }
      }
    }
  }
}

```

- 测试查询


```json
GET analyzer_url_test/_search
{
  "query": {
    "match": {
      "web_url": {
        "query": "ip138"
      }
    }
  }
}
#查询结果
{
  "took" : 445,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.6931471,
    "hits" : [
      {
        "_index" : "analyzer_url_test",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 0.6931471,
        "_source" : {
          "web_url" : "https://site.ip138.com/bn10086.com/beian.htm?param=123456#bock1",
          "test" : "https://site.ip138.com/bn10086.com/beian.htm?param=123456#bock1"
        }
      }
    ]
  }
}

```

```json
GET analyzer_url_test/_search
{
  "query": {
    "match": {
      "test": {
        "query": "ip138"
      }
    }
  }
}

#查询结果
{
  "took" : 2,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 0,
      "relation" : "eq"
    },
    "max_score" : null,
    "hits" : [ ]
  }
}
```

- 自定义analyzer测试方法


```json
GET _analyze
{
  "tokenizer": {
    "type": "char_group",
    "tokenize_on_chars": [
      ":",
      "/",
      ".",
      "?",
      "=",
      "#"
    ]
  },
  "filter" : [{
        
          "type" : "stop",
          "stopwords" : ["https", "http", "ftp", "www", "html","com", "cn"]
        
      },"lowercase"],
  "text" : "https://site.ip138.com/bn10086.com/beian.htm?param=123456#bock1"
}


#分词结果
{
  "tokens" : [
    {
      "token" : "https:",
      "start_offset" : 0,
      "end_offset" : 6,
      "type" : "word",
      "position" : 0
    },
    {
      "token" : "site",
      "start_offset" : 8,
      "end_offset" : 12,
      "type" : "word",
      "position" : 1
    },
    {
      "token" : "ip138",
      "start_offset" : 13,
      "end_offset" : 18,
      "type" : "word",
      "position" : 2
    },
    {
      "token" : "bn10086",
      "start_offset" : 23,
      "end_offset" : 30,
      "type" : "word",
      "position" : 4
    },
    {
      "token" : "beian",
      "start_offset" : 35,
      "end_offset" : 40,
      "type" : "word",
      "position" : 6
    },
    {
      "token" : "htm",
      "start_offset" : 41,
      "end_offset" : 44,
      "type" : "word",
      "position" : 7
    },
    {
      "token" : "param",
      "start_offset" : 45,
      "end_offset" : 50,
      "type" : "word",
      "position" : 8
    },
    {
      "token" : "123456",
      "start_offset" : 51,
      "end_offset" : 57,
      "type" : "word",
      "position" : 9
    },
    {
      "token" : "bock1",
      "start_offset" : 58,
      "end_offset" : 63,
      "type" : "word",
      "position" : 10
    }
  ]
}
```

- 预置analyzer测试方法


```json
GET _analyze
{
  "analyzer": "ik_max_word",
  "text": "https://site.ip138.com/bn10086.com/beian.htm?param=123456#bock1"
}

#分词结果
{
  "tokens" : [
    {
      "token" : "https",
      "start_offset" : 0,
      "end_offset" : 5,
      "type" : "ENGLISH",
      "position" : 0
    },
    {
      "token" : "site.ip138.com",
      "start_offset" : 8,
      "end_offset" : 22,
      "type" : "LETTER",
      "position" : 1
    },
    {
      "token" : "site",
      "start_offset" : 8,
      "end_offset" : 12,
      "type" : "ENGLISH",
      "position" : 2
    },
    {
      "token" : "ip",
      "start_offset" : 13,
      "end_offset" : 15,
      "type" : "ENGLISH",
      "position" : 3
    },
    {
      "token" : "138",
      "start_offset" : 15,
      "end_offset" : 18,
      "type" : "ARABIC",
      "position" : 4
    },
    {
      "token" : "com",
      "start_offset" : 19,
      "end_offset" : 22,
      "type" : "ENGLISH",
      "position" : 5
    },
    {
      "token" : "bn10086.com",
      "start_offset" : 23,
      "end_offset" : 34,
      "type" : "LETTER",
      "position" : 6
    },
    {
      "token" : "bn",
      "start_offset" : 23,
      "end_offset" : 25,
      "type" : "ENGLISH",
      "position" : 7
    },
    {
      "token" : "10086",
      "start_offset" : 25,
      "end_offset" : 30,
      "type" : "ARABIC",
      "position" : 8
    },
    {
      "token" : "com",
      "start_offset" : 31,
      "end_offset" : 34,
      "type" : "ENGLISH",
      "position" : 9
    },
    {
      "token" : "beian.htm",
      "start_offset" : 35,
      "end_offset" : 44,
      "type" : "LETTER",
      "position" : 10
    },
    {
      "token" : "beian",
      "start_offset" : 35,
      "end_offset" : 40,
      "type" : "ENGLISH",
      "position" : 11
    },
    {
      "token" : "htm",
      "start_offset" : 41,
      "end_offset" : 44,
      "type" : "ENGLISH",
      "position" : 12
    },
    {
      "token" : "param",
      "start_offset" : 45,
      "end_offset" : 50,
      "type" : "ENGLISH",
      "position" : 13
    },
    {
      "token" : "123456#bock1",
      "start_offset" : 51,
      "end_offset" : 63,
      "type" : "LETTER",
      "position" : 14
    },
    {
      "token" : "123456",
      "start_offset" : 51,
      "end_offset" : 57,
      "type" : "ARABIC",
      "position" : 15
    },
    {
      "token" : "bock",
      "start_offset" : 58,
      "end_offset" : 62,
      "type" : "ENGLISH",
      "position" : 16
    },
    {
      "token" : "1",
      "start_offset" : 62,
      "end_offset" : 63,
      "type" : "ARABIC",
      "position" : 17
    }
  ]
}
```

- 更新索引分词器的步骤

```
① mysql> set global read_only=1;设置数据库只读
② POST analyzer_url_test/_close  关闭索引
③ 更新索引的设置
PUT analyzer_url_test/_settings
{
  "analysis": {
      "analyzer": {
        "default_search" : {
          "type": "custom",
          "tokenizer": "ik_smart",
          "filter": [
            "lowercase",
            "stopwords_filter"
          ]
        },
        "default" : {
          "type": "custom",
          "tokenizer": "ik_max_word",
          "filter": [
            "lowercase",
            "stopwords_filter"
          ]
        },
        "url_analyzer": { 
          "type": "custom",
          "tokenizer": "url_tokenizer",
          "filter": [
            "lowercase",
            "stopwords_filter"
          ]
        }
      },
      "tokenizer": {
        "url_tokenizer":{
            "type": "char_group",
            "tokenize_on_chars": [
              ":",
              "/",
              ".",
              "?",
              "=",
              "#"
            ]
          }
      },
      "filter" : {
        "stopwords_filter" : {
          "type" : "stop",
          "stopwords" : ["https", "http", "ftp", "www", "html","com", "cn"]
        }
      }
    }
}
④ POST analyzer_url_test/_open 重新打开索引
⑤ mysql> set global read_only=0; 恢复数据库可读写
```

