## 创建索引

```http
PUT /inner_test
{
  "mappings": {
    "properties": {
      "title" : {
        "type": "text"
      },
      "content" : {
        "type": "object"
      },
      "users":{
        "type": "nested"
      }
    }
  }
}
```

## 插入测试数据

```http
PUT inner_test/_doc/1
{
  "title":"test",
  "content":{
    "key1":"value1"
  },
  "users":[
    {
      "id":"1",
      "status":"0"
    },
    {
      "id":"2",
      "status":"0"
    }
  ]
}
```

```http
GET inner_test/_doc/1
{
  "_index" : "inner_test",
  "_type" : "_doc",
  "_id" : "1",
  "_version" : 1,
  "_seq_no" : 0,
  "_primary_term" : 1,
  "found" : true,
  "_source" : {
    "title" : "test",
    "content" : {
      "key1" : "value1"
    },
    "users" : [
      {
        "id" : "1",
        "status" : "0"
      },
      {
        "id" : "2",
        "status" : "0"
      }
    ]
  }
}
```



## 更新nested，不存在就插入

```http
POST inner_test/_update/1
{
  "script": {
    "source": "def target = ctx._source.users.find(user -> user.id == params.userId); if(null == target){ctx._source.users.add(params.user) } ",
    "params": {
      "userId": "3",
      "user": {
        "id": "3",
        "status": "0"
      }
    }
  }
}
```

## 更新nested，不存在就插入，存在就用参数对象更新

```http
POST inner_test/_update/1
{
  "script": {
    "source": "def target = ctx._source.users.find(user -> user.id == params.userId); if(null == target){ctx._source.users.add(params.user) } else {for(pro in params.user.entrySet()){target[pro.getKey()]=pro.getValue()}} ",
    "params": {
      "userId": "3",
      "user": {
        "id": "3",
        "status": "1",
        "age":20
      }
    }
 }
```

## nested查询

```http
GET inner_test/_search
{
  "query": {
    "nested": {
      "path": "users",
      "query": {
        "bool": {
          "must": [
            { "match": { "users.id": "3" }},
            { "match": { "users.status":  "1" }} 
          ]
        }
      }
    }
  }
}
```

