## 使用压缩包安装Elasticsearch

```shell
shell> wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.8.0-linux-x86_64.tar.gz
shell> wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.8.0-linux-x86_64.tar.gz.sha512
shell> sudo yum install perl-Digest-SHA
shell> shasum -a 512 -c elasticsearch-7.8.0-linux-x86_64.tar.gz.sha512
elasticsearch-7.8.0-linux-x86_64.tar.gz: OK
shell> tar -xzvf elasticsearch-7.8.0-linux-x86_64.tar.gz
```

## 启动Elasticsearch

```shell
shell> cd elasticsearch-7.8.0/
shell> ./bin/elasticsearch
shell> curl -X GET "localhost:9200/?pretty"
{
  "name" : "elasticsearch",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "IVvYqDS_S36YacFOKFXeeg",
  "version" : {
    "number" : "7.8.0",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "757314695644ea9a1dc2fecd26d1a43856725e65",
    "build_date" : "2020-06-14T19:35:50.234439Z",
    "build_snapshot" : false,
    "lucene_version" : "8.5.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

## 配置Elasticsearch

```
shell> vim /opt/module/elasticsearch-7.8.0/config/elasticsearch.yml
```

###  Configuring Elasticsearch on the command line

Elasticsearch loads its configuration from the `$ES_HOME/config/elasticsearch.yml` file by default. The format of this config file is explained in [*Configuring Elasticsearch*](https://www.elastic.co/guide/en/elasticsearch/reference/7.8/settings.html).

Any settings that can be specified in the config file can also be specified on the command line, using the `-E` syntax as follows:

```sh
./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_1 -Epath.data=/opt/module/esdata/node_1_data -Epath.logs=/opt/module/esdata/node_1_logs
```

Typically, any cluster-wide settings (like `cluster.name`) should be added to the `elasticsearch.yml` config file, while any node-specific settings such as `node.name` could be specified on the command line.

### Directory layout of archives

The archive distributions are entirely self-contained. All files and directories are, by default, contained within `$ES_HOME` — the directory created when unpacking the archive.

This is very convenient because you don’t have to create any directories to start using Elasticsearch, and uninstalling Elasticsearch is as easy as removing the `$ES_HOME` directory. However, it is advisable to change the default locations of the config directory, the data directory, and the logs directory so that you do not delete important data later on.

| Type        | Description                                                  | Default Location                           | Setting        |
| ----------- | ------------------------------------------------------------ | ------------------------------------------ | -------------- |
| **home**    | Elasticsearch home directory or `$ES_HOME`                   | Directory created by unpacking the archive |                |
| **bin**     | Binary scripts including `elasticsearch` to start a node and `elasticsearch-plugin` to install plugins | `$ES_HOME/bin`                             |                |
| **conf**    | Configuration files including `elasticsearch.yml`            | `$ES_HOME/config`                          | `ES_PATH_CONF` |
| **data**    | The location of the data files of each index / shard allocated on the node. Can hold multiple locations. | `$ES_HOME/data`                            | `path.data`    |
| **logs**    | Log files location.                                          | `$ES_HOME/logs`                            | `path.logs`    |
| **plugins** | Plugin files location. Each plugin will be contained in a subdirectory. | `$ES_HOME/plugins`                         |                |
| **repo**    | Shared file system repository locations. Can hold multiple locations. A file system repository can be placed in to any subdirectory of any directory specified here. |                                            |                |

```sh
shell> ulimit -n 65535
shell> ulimit -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 11215
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 65535
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 11215
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited
shell> sysctl -w vm.max_map_count=262144
vm.max_map_count = 262144
# 线程至少4096
#shell> ulimit -u 4096
```

```shell
shell> vim config/elasticsearch.yml
# 允许外网访问
network.host: 0.0.0.0
# 
discovery.seed_hosts: ["127.0.0.1"]

cluster.initial_master_nodes: ["node_1","node_2","node_3"]
```

## 启动集群

```shell
shell> cd /opt/module
shell> mkdir esdata
shell> cd esdata
shell> mkdir node_1_data node_2_data node_3_data node_1_logs node_2_logs node_3_logs
```

```shell
shell>
	./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_1 -Epath.data=/opt/module/esdata/node_1_data -Epath.logs=/opt/module/esdata/node_1_logs
    ./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_2 -Epath.data=/opt/module/esdata/node_2_data -Epath.logs=/opt/module/esdata/node_2_logs
    ./bin/elasticsearch -d -Ecluster.name=wxyzyu -Enode.name=node_3 -Epath.data=/opt/module/esdata/node_3_data -Epath.logs=/opt/module/esdata/node_3_logs
```

```shell
shell> curl -X GET "192.168.8.11:9200/"
{
  "name" : "node_2",
  "cluster_name" : "wxyzyu",
  "cluster_uuid" : "_na_",
  "version" : {
    "number" : "7.8.0",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "757314695644ea9a1dc2fecd26d1a43856725e65",
    "build_date" : "2020-06-14T19:35:50.234439Z",
    "build_snapshot" : false,
    "lucene_version" : "8.5.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

