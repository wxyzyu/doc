## Elasticsearch 7 创建带json（object）类型字段的索引

```shell
PUT /json_test
{
  "settings":{
    "index.analysis.analyzer.default.type": "ik_max_word",
    "index.analysis.analyzer.default_search.type": "ik_smart"
  },
  "mappings": {
    "properties": {
      "title" : {
        "type": "text"
      },
      "content" : {
        "type": "object"
      },
      "content_search":{
        "type": "text"
      },
      "content_version" : {
        "type": "keyword"
      }
    }
  }
}
```

## 测试数据

```shell
POST /json_test/_doc/1
{
  "title" : "测试标题",
  "content" :{
    "test" : "测试内容",
    "url" : "www.baidu.com",
    "age":"11",
    "desc": "《资本论》全书共三卷，以剩余价值为中心，对资本主义进行了彻底的批判。第一卷研究了资本的生产过程，分析了剩余价值的生产问题。第二卷在资本生产过程的基础上研究了资本的流通过程，分析了剩余价值的实现问题。第三卷讲述了资本主义生产的总过程，分别研究了资本和剩余价值的具体形式。这一卷讲述的内.达到了资本的生产过程、流通过程和分配过程的高度统一，分析了剩余价值的分配问题。"
  }
}

POST /json_test/_doc/2
{
  "title" : "测试标题",
  "content" :{
    "test" : "测试内容",
    "url" : "www.baidu.com",
    "age":"11",
    "desc": "desc"
  }
}

POST json_test/_update/2
{
  "doc" : {
    "content_version" : "123",
    "content":{
      "desc" :"测试描述"
    }
  }
}
```

## json类型字段Elasticsearch自动生成的索引信息

```shell
GET json_test
{
  "json_test" : {
    "aliases" : { },
    "mappings" : {
      "properties" : {
        "content" : {
          "properties" : {
            "age" : {
              "type" : "text",
              "fields" : {
                "keyword" : {
                  "type" : "keyword",
                  "ignore_above" : 256
                }
              },
              "analyzer" : "default",
              "search_analyzer" : "default_search"
            },
            "desc" : {
              "type" : "text",
              "fields" : {
                "keyword" : {
                  "type" : "keyword",
                  "ignore_above" : 256
                }
              },
              "analyzer" : "default",
              "search_analyzer" : "default_search"
            },
            "test" : {
              "type" : "text",
              "fields" : {
                "keyword" : {
                  "type" : "keyword",
                  "ignore_above" : 256
                }
              },
              "analyzer" : "default",
              "search_analyzer" : "default_search"
            },
            "url" : {
              "type" : "text",
              "fields" : {
                "keyword" : {
                  "type" : "keyword",
                  "ignore_above" : 256
                }
              },
              "analyzer" : "default",
              "search_analyzer" : "default_search"
            }
          }
        },
        "content_search" : {
          "type" : "text",
          "analyzer" : "default",
          "search_analyzer" : "default_search"
        },
        "content_version" : {
          "type" : "keyword"
        },
        "title" : {
          "type" : "text",
          "analyzer" : "default",
          "search_analyzer" : "default_search"
        }
      }
    },
    "settings" : {
      "index" : {
        "number_of_shards" : "1",
        "provided_name" : "json_test",
        "creation_date" : "1594198294322",
        "analysis" : {
          "analyzer" : {
            "default_search" : {
              "type" : "ik_smart"
            },
            "default" : {
              "type" : "ik_max_word"
            }
          }
        },
        "number_of_replicas" : "1",
        "uuid" : "PyG0E4O7RECGTyzYpYwULg",
        "version" : {
          "created" : "7080099"
        }
      }
    }
  }
}
```

## 测试全文检索json中的所有字段加全文检索外部字段

```shell
GET json_test/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "multi_match" : {
            "query":      "资本论",
            "fields":     [ "content.*" ]
          }
        },
        {
          "match": {
            "content_version": "123"
          }
        }
      ]
    }
  }
}

{
  "took" : 2,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 2,
      "relation" : "eq"
    },
    "max_score" : 0.5050415,
    "hits" : [
      {
        "_index" : "json_test4",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 0.5050415,
        "_source" : {
          "title" : "测试标题",
          "content" : {
            "test" : "测试内容",
            "url" : "www.baidu.com",
            "age" : "11",
            "desc" : "《资本论》全书共三卷，以剩余价值为中心，对资本主义进行了彻底的批判。第一卷研究了资本的生产过程，分析了剩余价值的生产问题。第二卷在资本生产过程的基础上研究了资本的流通过程，分析了剩余价值的实现问题。第三卷讲述了资本主义生产的总过程，分别研究了资本和剩余价值的具体形式。这一卷讲述的内.达到了资本的生产过程、流通过程和分配过程的高度统一，分析了剩余价值的分配问题。"
          }
        }
      },
      {
        "_index" : "json_test4",
        "_type" : "_doc",
        "_id" : "2",
        "_score" : 0.2876821,
        "_source" : {
          "title" : "测试标题",
          "content" : {
            "test" : "测试内容",
            "url" : "www.baidu.com",
            "age" : "11",
            "desc" : "测试描述"
          },
          "content_version" : "123"
        }
      }
    ]
  }
}

```

## 问题

```shell
# 创建索引
PUT /json_test8
{
  "settings":{
    "index.analysis.analyzer.default.type": "ik_max_word",
    "index.analysis.analyzer.default_search.type": "ik_smart"
  },
  "mappings": {
    "properties": {
      "content" : {
        "type": "object"
      }
    }
  }
}

# 查看动态索引信息
GET json_test8
{
  "json_test8" : {
    "aliases" : { },
    "mappings" : {
      "properties" : {
        "content" : {
          "type" : "object"
        }
      }
    },
    "settings" : {
      "index" : {
        "number_of_shards" : "1",
        "provided_name" : "json_test8",
        "creation_date" : "1594266984755",
        "analysis" : {
          "analyzer" : {
            "default_search" : {
              "type" : "ik_smart"
            },
            "default" : {
              "type" : "ik_max_word"
            }
          }
        },
        "number_of_replicas" : "1",
        "uuid" : "FwI98KYaTKalMW1U8LFx4w",
        "version" : {
          "created" : "7080099"
        }
      }
    }
  }
}

# 插入测试数据
PUT json_test8/_doc/1
{
  "content" :{
    "boolean":true,
    "float" : 123456.123,
    "double": 123456789.123456,
    "date" : "2020/07/09",
    "integer":1,
    "long" :789,
    "object" : {
      "objkey1": "123",
      "objkey2":456
    },
    "String" : "测试内容"
  }
}

# 查看自动更新过的索引信息
GET json_test8
{
  "json_test8" : {
    "aliases" : { },
    "mappings" : {
      "properties" : {
        "content" : {
          "properties" : {
            "String" : {
              "type" : "text",
              "fields" : {
                "keyword" : {
                  "type" : "keyword",
                  "ignore_above" : 256
                }
              },
              "analyzer" : "default",
              "search_analyzer" : "default_search"
            },
            "boolean" : {
              "type" : "boolean"
            },
            "date" : {
              "type" : "date",
              "format" : "yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"
            },
            "double" : {
              "type" : "float"
            },
            "float" : {
              "type" : "float"
            },
            "integer" : {
              "type" : "long"
            },
            "long" : {
              "type" : "long"
            },
            "object" : {
              "properties" : {
                "objkey1" : {
                  "type" : "text",
                  "fields" : {
                    "keyword" : {
                      "type" : "keyword",
                      "ignore_above" : 256
                    }
                  },
                  "analyzer" : "default",
                  "search_analyzer" : "default_search"
                },
                "objkey2" : {
                  "type" : "long"
                }
              }
            }
          }
        }
      }
    },
    "settings" : {
      "index" : {
        "number_of_shards" : "1",
        "provided_name" : "json_test8",
        "creation_date" : "1594266984755",
        "analysis" : {
          "analyzer" : {
            "default_search" : {
              "type" : "ik_smart"
            },
            "default" : {
              "type" : "ik_max_word"
            }
          }
        },
        "number_of_replicas" : "1",
        "uuid" : "FwI98KYaTKalMW1U8LFx4w",
        "version" : {
          "created" : "7080099"
        }
      }
    }
  }
}

# 执行json中全字段查询
GET json_test8/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "multi_match" : {
            "query":      123456.123,
            "fields":     [ "content.*" ]
          }
        }
      ]
    }
  }
}

# 报错，因为Elasticsearch会自动判定类型，查询的时候会根据类型对查询条件进行转换
{
  "error" : {
    "root_cause" : [
      {
        "type" : "query_shard_exception",
        "reason" : "failed to create query: Can't parse boolean value [123456.123], expected [true] or [false]",
        "index_uuid" : "FwI98KYaTKalMW1U8LFx4w",
        "index" : "json_test8"
      }
    ],
    "type" : "search_phase_execution_exception",
    "reason" : "all shards failed",
    "phase" : "query",
    "grouped" : true,
    "failed_shards" : [
      {
        "shard" : 0,
        "index" : "json_test8",
        "node" : "ZRFKAPeJTtSchPlh6JF1kg",
        "reason" : {
          "type" : "query_shard_exception",
          "reason" : "failed to create query: Can't parse boolean value [123456.123], expected [true] or [false]",
          "index_uuid" : "FwI98KYaTKalMW1U8LFx4w",
          "index" : "json_test8",
          "caused_by" : {
            "type" : "illegal_argument_exception",
            "reason" : "Can't parse boolean value [123456.123], expected [true] or [false]"
          }
        }
      }
    ]
  },
  "status" : 400
}
```

## 解决办法使用[dynamic_templates](https://www.elastic.co/guide/en/elasticsearch/reference/current/dynamic-templates.html)

```shell
# 使用dynamic_templates，通过设置path_match使content中除了string和object类型的字段之外的其他的都是用keyword保存，同时不影响整个文档中其他的字段
# Templates are processed in order — the first matching template wins.也就是说dynamic_templates部分template的顺序会影响对字段类型的判定。
# 如果把nonstring放在第一个则会出错
PUT /json_test9
{
  "settings":{
    "index.analysis.analyzer.default.type": "ik_max_word",
    "index.analysis.analyzer.default_search.type": "ik_smart"
  },
  "mappings": {
    "dynamic_templates": [
      {
        "strings": {
          "match_mapping_type": "string",
          "mapping": {
            "type": "text",
            "fields": {
              "keyword": {
                "type":  "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      },
      {
        "objects": {
          "match_mapping_type": "object",
          "mapping": {
            "type": "object"
          }
        }
      },
      {
        "nonstring": {
          "match_mapping_type": "*",
          "path_match":"content.*",
          "mapping": {
            "type": "keyword"
          }
        }
      }
    ],
    "properties": {
      "title" : {
        "type": "text"
      },
      "age":{
        "type": "long"
      },
      "content" : {
        "type": "object"
      },
      "content_search":{
        "type": "text"
      },
      "content_version" : {
        "type": "keyword"
      }
    }
  }
}

# 插入测试数据，注意这里给content的同级新增了height字段
PUT json_test9/_doc/1
{
  "title" : "测试标题",
  "content" :{
    "boolean":true,
    "float" : 123456.123,
    "double": 123456789.123456,
    "date" : "2020/07/09",
    "integer":1,
    "long" :789,
    "object" : {
      "objkey1": "123",
      "objkey2":456
    },
    "String" : "测试内容"
  },
  "age":11,
  "height":180
}

# 查看索引信息
GET json_test9/_mapping
{
  "json_test9" : {
    "mappings" : {
      "dynamic_templates" : [
        {
          "strings" : {
            "match_mapping_type" : "string",
            "mapping" : {
              "fields" : {
                "keyword" : {
                  "ignore_above" : 256,
                  "type" : "keyword"
                }
              },
              "type" : "text"
            }
          }
        },
        {
          "objects" : {
            "match_mapping_type" : "object",
            "mapping" : {
              "type" : "object"
            }
          }
        },
        {
          "nonstring" : {
            "path_match" : "content.*",
            "mapping" : {
              "type" : "keyword"
            }
          }
        }
      ],
      "properties" : {
        "age" : {
          "type" : "long"
        },
        "content" : {
          "properties" : {
            "String" : {
              "type" : "text",
              "fields" : {
                "keyword" : {
                  "type" : "keyword",
                  "ignore_above" : 256
                }
              },
              "analyzer" : "default",
              "search_analyzer" : "default_search"
            },
            "boolean" : {
              "type" : "keyword"
            },
            "date" : {
              "type" : "keyword"
            },
            "double" : {
              "type" : "keyword"
            },
            "float" : {
              "type" : "keyword"
            },
            "integer" : {
              "type" : "keyword"
            },
            "long" : {
              "type" : "keyword"
            },
            "object" : {
              "properties" : {
                "objkey1" : {
                  "type" : "text",
                  "fields" : {
                    "keyword" : {
                      "type" : "keyword",
                      "ignore_above" : 256
                    }
                  },
                  "analyzer" : "default",
                  "search_analyzer" : "default_search"
                },
                "objkey2" : {
                  "type" : "keyword"
                }
              }
            }
          }
        },
        "content_search" : {
          "type" : "text",
          "analyzer" : "default",
          "search_analyzer" : "default_search"
        },
        "content_version" : {
          "type" : "keyword"
        },
        "height" : {
          "type" : "long"
        },
        "title" : {
          "type" : "text",
          "analyzer" : "default",
          "search_analyzer" : "default_search"
        }
      }
    }
  }
}

# 执行查询，对于keyword类型的字段进行查询不会产生转换错误
GET json_test9/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "multi_match" : {
            "query":      123456.123,
            "fields":     [ "content.*" ]
          }
        }
      ]
    }
  }
}

{
  "took" : 100,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.2876821,
    "hits" : [
      {
        "_index" : "json_test9",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 0.2876821,
        "_source" : {
          "title" : "测试标题",
          "content" : {
            "boolean" : true,
            "float" : 123456.123,
            "double" : 1.23456789123456E8,
            "date" : "2020/07/09",
            "integer" : 1,
            "long" : 789,
            "object" : {
              "objkey1" : "123",
              "objkey2" : 456
            },
            "String" : "测试内容"
          }
        }
      }
    ]
  }
}

GET json_test9/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "multi_match" : {
            "query":      "内容",
            "fields":     [ "content.*" ]
          }
        }
      ]
    }
  }
}

{
  "took" : 30,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.2876821,
    "hits" : [
      {
        "_index" : "json_test9",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 0.2876821,
        "_source" : {
          "title" : "测试标题",
          "content" : {
            "boolean" : true,
            "float" : 123456.123,
            "double" : 1.23456789123456E8,
            "date" : "2020/07/09",
            "integer" : 1,
            "long" : 789,
            "object" : {
              "objkey1" : "123",
              "objkey2" : 456
            },
            "String" : "测试内容"
          }
        }
      }
    ]
  }
}
```

