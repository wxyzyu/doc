## 注意canal1.1.4只支持jdk8

## 创建mysql用户

```mysql
mysql> CREATE USER 'usr_replica'@'%'  IDENTIFIED BY 'mysql_password';
mysql> GRANT ALL ON json_test.* TO 'usr_replica';
mysql> GRANT RELOAD ON *.* to 'usr_replica';
mysql> GRANT REPLICATION CLIENT ON *.* to 'usr_replica';
mysql> GRANT REPLICATION SLAVE ON *.* to 'usr_replica';
mysql> FLUSH PRIVILEGES;
mysql> show GRANTS for 'usr_replica'@'%';
+---------------------------------------------------------------------------------+
| Grants for usr_replica@%                                                        |
+---------------------------------------------------------------------------------+
| GRANT RELOAD, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'usr_replica'@'%' |
| GRANT ALL PRIVILEGES ON `json_test`.* TO 'usr_replica'@'%'                      |
+---------------------------------------------------------------------------------+
2 rows in set (0.00 sec)
```

## 下载canal 和canal-adapter

```shell
shell> cd /opt/module/
shell> wget https://github.com/alibaba/canal/releases/download/canal-1.1.5-alpha-1/canal.deployer-1.1.5-SNAPSHOT.tar.gz
shell> mkdir canal-1.1.5
shell> tar -zxvf canal.deployer-1.1.5-SNAPSHOT.tar.gz -C canal-1.1.5
shell> wget https://github.com/alibaba/canal/releases/download/canal-1.1.5-alpha-1/canal.adapter-1.1.5-SNAPSHOT.tar.gz
shell> mkdir canal-adapter-1.1.5
shell> tar -zxvf canal.adapter-1.1.5-SNAPSHOT.tar.gz  -C canal-adapter-1.1.5

```

## 配置canal

```shell
shell> cd canal-1.1.5
shell> vim conf/example/instance.properties
## mysql serverId
canal.instance.mysql.slaveId = 1001
#position info，需要改成自己的数据库信息
canal.instance.master.address = 127.0.0.1:3306 
canal.instance.master.journal.name = 
canal.instance.master.position = 
canal.instance.master.timestamp = 
#canal.instance.standby.address = 
#canal.instance.standby.journal.name =
#canal.instance.standby.position = 
#canal.instance.standby.timestamp = 
#username/password，需要改成自己的数据库信息
canal.instance.dbUsername = username  
canal.instance.dbPassword = password
canal.instance.defaultDatabaseName =
canal.instance.connectionCharset = UTF-8
#table regex
canal.instance.filter.regex = .\*\\\\..\*
```

## 启动canal

```shell
shell> ./bin/startup.sh
shell> vim logs/canal/canal.log
2020-07-02 17:19:57.680 [main] INFO  com.alibaba.otter.canal.deployer.CanalLauncher - ## set default uncaught exception handler
2020-07-02 17:19:57.717 [main] INFO  com.alibaba.otter.canal.deployer.CanalLauncher - ## load canal configurations
2020-07-02 17:19:57.757 [main] INFO  com.alibaba.otter.canal.deployer.CanalStarter - ## start the canal server.
2020-07-02 17:19:57.792 [main] INFO  com.alibaba.otter.canal.deployer.CanalController - ## start the canal server[192.168.8.11(192.168.8.11):11111]
2020-07-02 17:20:00.652 [main] INFO  com.alibaba.otter.canal.deployer.CanalStarter - ## the canal server is running now ......
```

## 配置canal-adapter

```shell
shell> cd /opt/module/canal-adapter-1.1.5/conf
shell> vim application.yml 

server:
  port: 8081
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
    default-property-inclusion: non_null

canal.conf:
  mode: tcp # kafka rocketMQ
  canalServerHost: 127.0.0.1:11111
#  zookeeperHosts: slave1:2181
#  mqServers: 127.0.0.1:9092 #or rocketmq
#  flatMessage: true
  batchSize: 500
  syncBatchSize: 1000
  retries: 0
  timeout:
  accessKey:
  secretKey:
  username:
  password:
  vhost:
  srcDataSources:
    defaultDS:
      url: jdbc:mysql://127.0.0.1:3306/json_test?useUnicode=true
      username: usr
      password: pwd
  canalAdapters:
  - instance: example # canal instance Name or mq topic name
    groups:
    - groupId: g1
      outerAdapters:
      - name: logger
#      - name: rdb
#        key: mysql1
#        properties:
#          jdbc.driverClassName: com.mysql.jdbc.Driver
#          jdbc.url: jdbc:mysql://127.0.0.1:3306/mytest2?useUnicode=true
#          jdbc.username: root
#          jdbc.password: 121212
#      - name: rdb
#        key: oracle1
#        properties:
#          jdbc.driverClassName: oracle.jdbc.OracleDriver
#          jdbc.url: jdbc:oracle:thin:@localhost:49161:XE
#          jdbc.username: mytest
#          jdbc.password: m121212
#      - name: rdb
#        key: postgres1
#        properties:
#          jdbc.driverClassName: org.postgresql.Driver
#          jdbc.url: jdbc:postgresql://localhost:5432/postgres
#          jdbc.username: postgres
#          jdbc.password: 121212
#          threads: 1
#          commitSize: 3000
#      - name: hbase
#        properties:
#          hbase.zookeeper.quorum: 127.0.0.1
#          hbase.zookeeper.property.clientPort: 2181
#          zookeeper.znode.parent: /hbase
      - name: es7
        hosts: 192.168.8.11:9300 # 127.0.0.1:9200 for rest mode
        properties:
          mode: transport # or rest
          # security.auth: test:123456 #  only used for rest mode
          cluster.name: wxyzyu
          
# 修改es7配置文件
shell> cd es7
shell> vim 
dataSourceKey: defaultDS
destination: example
groupId: g1
esMapping:
  _index: inner_alarm
  _id: _id
#  upsert: true
#  pk: id
  sql: "SELECT a.id as _id, a.title, a.content, a.content_search, a.content_version FROM inner_alarm a"
#  objFields:
#    _labels: array:;
  commitBatch: 3000

```

## 创建索引

```shell
PUT /inner_alarm
{
  "settings":{
    "index.analysis.analyzer.default.type": "ik_max_word",
    "index.analysis.analyzer.default_search.type": "ik_smart"
  },
  "mappings": {
    "dynamic_templates": [
      {
        "strings": {
          "match_mapping_type": "string",
          "mapping": {
            "type": "text",
            "fields": {
              "keyword": {
                "type":  "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      },
      {
        "objects": {
          "match_mapping_type": "object",
          "mapping": {
            "type": "object"
          }
        }
      },
      {
        "nonstring": {
          "match_mapping_type": "*",
          "path_match":"content.*",
          "mapping": {
            "type": "keyword"
          }
        }
      }
    ],
    "properties": {
      "title" : {
        "type": "text"
      },
      "age":{
        "type": "long"
      },
      "content" : {
        "type": "object"
      },
      "content_search":{
        "type": "text"
      },
      "content_version" : {
        "type": "keyword"
      }
    }
  }
}
```

## 启动canal-adapter

```shell
shell> ./opt/module/anal-adapter-1.1.5/bin/startup.sh
```

## 查看同步日志

```shell
shell> tail -f /opt/module/canal-adapter-1.1.5/logs/adapter/adapter.log
2020-07-09 18:00:51.410 [main] INFO  org.apache.coyote.http11.Http11NioProtocol - Starting ProtocolHandler ["http-nio-8081"]
2020-07-09 18:00:51.416 [Thread-4] INFO  c.a.o.canal.adapter.launcher.loader.CanalAdapterWorker - =============> Start to connect destination: example <=============
2020-07-09 18:00:51.505 [main] INFO  org.apache.tomcat.util.net.NioSelectorPool - Using a shared selector for servlet write/read
2020-07-09 18:00:51.583 [main] INFO  o.s.boot.web.embedded.tomcat.TomcatWebServer - Tomcat started on port(s): 8081 (http) with context path ''
2020-07-09 18:00:51.617 [main] INFO  c.a.otter.canal.adapter.launcher.CanalAdapterApplication - Started CanalAdapterApplication in 28.633 seconds (JVM running for 29.951)
2020-07-09 18:00:53.015 [Thread-4] INFO  c.a.o.canal.adapter.launcher.loader.CanalAdapterWorker - =============> Start to subscribe destination: example <=============
2020-07-09 18:00:53.226 [Thread-4] INFO  c.a.o.canal.adapter.launcher.loader.CanalAdapterWorker - =============> Subscribe destination: example succeed <=============
2020-07-09 18:00:53.642 [pool-1-thread-1] INFO  c.a.o.canal.client.adapter.logger.LoggerAdapterExample - DML: {"data":[{"id":"-1","title":"title-1","create_time":0,"create_type":null,"create_user_id":null,"create_source_id":null,"ip":null,"content":null,"content_search":null,"content_version":null,"first_discover_time":0,"last_discover_time":0,"type":null,"status":null,"inner_status":0,"device_id":null,"software_id":null,"remark":null,"is_delete":0,"confirm_time":null,"conform_user_id":null,"ignore_time":null,"ignore_user_id":null,"handle_time":null,"handle_user_id":null,"is_expired":null,"version":null,"update_time":null,"update_user_id":null,"finish_time":null,"finish_user_id":null,"reopen_time":null,"reopen_type":null,"reopen_user_id":null,"reopen_source_id":null}],"database":"json_test","destination":"example","es":1594286838000,"groupId":null,"isDdl":false,"old":null,"pkNames":["id"],"sql":"","table":"inner_alarm","ts":1594288853319,"type":"INSERT"}
2020-07-09 18:00:54.625 [pool-1-thread-1] DEBUG c.a.o.canal.client.adapter.es.core.service.ESSyncService - DML: {"data":[{"id":"-1","title":"title-1","create_time":0,"create_type":null,"create_user_id":null,"create_source_id":null,"ip":null,"content":null,"content_search":null,"content_version":null,"first_discover_time":0,"last_discover_time":0,"type":null,"status":null,"inner_status":0,"device_id":null,"software_id":null,"remark":null,"is_delete":0,"confirm_time":null,"conform_user_id":null,"ignore_time":null,"ignore_user_id":null,"handle_time":null,"handle_user_id":null,"is_expired":null,"version":null,"update_time":null,"update_user_id":null,"finish_time":null,"finish_user_id":null,"reopen_time":null,"reopen_type":null,"reopen_user_id":null,"reopen_source_id":null}],"database":"json_test","destination":"example","es":1594286838000,"groupId":null,"isDdl":false,"old":null,"pkNames":["id"],"sql":"","table":"inner_alarm","ts":1594288853643,"type":"INSERT"} 
Affected indexes: inner_alarm 

```



