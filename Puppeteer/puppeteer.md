## 安装Puppeteer

```shell
#首先需要安装nodejs
shell> cd /opt/module
shell> mkdir puppeteer
shell> cd puppeteer
shell> npm init
shell> npm i puppeteer
> puppeteer@5.2.1 install /opt/module/puppeteer/node_modules/puppeteer
> node install.js

Downloading Chromium r782078 - 126.4 Mb [====================] 100% 0.0s 
Chromium (782078) downloaded to /opt/module/puppeteer/node_modules/puppeteer/.local-chromium/linux-782078
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN ws@7.3.1 requires a peer of bufferutil@^4.0.1 but none is installed. You must install peer dependencies yourself.
npm WARN ws@7.3.1 requires a peer of utf-8-validate@^5.0.2 but none is installed. You must install peer dependencies yourself.

shell> npm i bufferutil@^4.0.1
shell> npm i utf-8-validate@^5.0.2
shell> cd /opt/module/puppeteer/node_modules/puppeteer/.local-chromium/linux-782078/chrome-linux
# 查看需要安装的依赖
shell> ldd chrome | grep not
	libatk-bridge-2.0.so.0 => not found
	libXcomposite.so.1 => not found
	libXcursor.so.1 => not found
	libXdamage.so.1 => not found
	libXfixes.so.3 => not found
	libXi.so.6 => not found
	libXtst.so.6 => not found
	libcups.so.2 => not found
	libgbm.so.1 => not found
	libpangocairo-1.0.so.0 => not found
	libpango-1.0.so.0 => not found
	libcairo.so.2 => not found
	libatspi.so.0 => not found
	libXss.so.1 => not found
	libgtk-3.so.0 => not found
	libgdk-3.so.0 => not found
	libgdk_pixbuf-2.0.so.0 => not found
# 反查包名
shell> yum provides libatk-bridge-2.0.so.0
Loaded plugins: fastestmirror, langpacks
Loading mirror speeds from cached hostfile
 * base: mirrors.aliyun.com
 * extras: mirrors.aliyun.com
 * updates: mirrors.aliyun.com
at-spi2-atk-2.26.2-1.el7.i686 : A GTK+ module that bridges ATK to D-Bus at-spi
Repo        : base
Matched from:
Provides    : libatk-bridge-2.0.so.0


shell> sudo yum install at-spi2-atk
shell> sudo yum install libXcomposite
shell> sudo yum install libXcursor
shell> sudo yum install libXdamage
shell> sudo yum install libXfixes
shell> sudo yum install libXi
shell> sudo yum install mesa-libgbm
shell> sudo yum install pango
shell> sudo yum install libXScrnSaver
shell> sudo yum install gtk3

```

## 保存测试文件test.js

```javascript
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://wwww.baidu.com');
  await page.screenshot({path: 'example.png'}).catch((error) => {
      console.error(error);
  });

  await browser.close();
})();
```

## 测试

```shell
shell> node test.js
(node:3546) UnhandledPromiseRejectionWarning: Error: Failed to launch the browser process!
[0903/193451.388697:FATAL:zygote_host_impl_linux.cc(117)] No usable sandbox! Update your kernel or see https://chromium.googlesource.com/chromium/src/+/master/docs/linux/suid_sandbox_development.md for more information on developing with the SUID sandbox. If you want to live dangerously and need an immediate workaround, you can try using --no-sandbox.

```

## [开启sandbox](https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#setting-up-chrome-linux-sandbox)

```shell
# cd to the downloaded instance
shell> cd /opt/module/puppeteer/node_modules/puppeteer/.local-chromium/linux-782078/chrome-linux
shell> sudo chown root:root chrome_sandbox
shell> sudo chmod 4755 chrome_sandbox
# copy sandbox executable to a shared location
shell> sudo cp -p chrome_sandbox /usr/local/sbin/chrome-devel-sandbox
# export CHROME_DEVEL_SANDBOX env variable
shell> export CHROME_DEVEL_SANDBOX=/usr/local/sbin/chrome-devel-sandbox
```

```
You might want to export the CHROME_DEVEL_SANDBOX env variable by default. In this case, add the following to the ~/.bashrc or .zshenv:

export CHROME_DEVEL_SANDBOX=/usr/local/sbin/chrome-devel-sandbox
```

## 测试

```shell
shell> node test.js
```

![image-20200903125024645](puppeteer.assets/image-20200903125024645.png) 

```shell
# 解决乱码
shell> yum groupinstall "fonts" -y
```

![image-20200903125228242](puppeteer.assets/image-20200903125228242.png) 