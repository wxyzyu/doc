## Conductor Action

Conductor actions make it possible to build and invoke a series of actions, similar to sequences. However, whereas the components of a sequence action must be specified before invoking the sequence, conductor actions can decide the series of actions to invoke at run time.

In this document, we specify conductor actions and illustrate them with a simple example: a *tripleAndIncrement* action.

```shell
shell> vi triple.js
function main({ value }) { return { value: value * 3 } }
```

```shell
shell> wsk -i action create triple triple.js
ok: created action triple
```

```shell
shell> vi increment.js
function main({ value }) { return { value: value + 1 } }
```

```shell
shell> wsk -i action create increment increment.js
ok: created action increment
```

```shell
shell> vi tripleAndIncrement.js
function main(params) {
    let step = params.$step || 0
    delete params.$step
    switch (step) {
        case 0: return { action: 'triple', params, state: { $step: 1 } }
        case 1: return { action: 'increment', params, state: { $step: 2 } }
        case 2: return { params }
    }
}
```

```shell
shell> wsk -i action create tripleAndIncrement tripleAndIncrement.js -a conductor true
ok: created action tripleAndIncrement
```

A *conductor action* is an action with a *conductor* annotation with a value that is not *falsy*, i.e., a value that is different from zero, null, false, and the empty string.

At this time, the conductor annotation is ignored on sequence actions.

Because a conductor action is an action, it has all the attributes of an action (name, namespace, default parameters, limits...) and it can be managed as such, for instance using the `wsk action` CLI commands. It can be part of a package or be a web action.

In essence, the *tripleAndIncrement* action builds a sequence of two actions by encoding a program with three steps:

- step 0: invoke the *triple* action on the input dictionary,
- step 1: invoke the *increment* action on the output dictionary from step 1,
- step 2: return the output dictionary from step 2.

At each step, the conductor action specifies how to continue or terminate the execution by means of a *continuation*. We explain continuations after discussing invocation and activations.

## Invocation

```shell
shelol> wsk action invoke --help
invoke action
Usage:
  wsk action invoke ACTION_NAME [flags]

Flags:
  -b, --blocking          blocking invoke
  -h, --help              help for invoke
  -p, --param KEY VALUE   parameter values in KEY VALUE format
  -P, --param-file FILE   FILE containing parameter values in JSON format
  -r, --result            blocking invoke; show only activation result (unless there is a failure)

Global Flags:
      --apihost HOST         whisk API HOST
      --apiversion VERSION   whisk API VERSION
  -u, --auth KEY             authorization KEY
      --cert string          client cert
  -d, --debug                debug level output
  -i, --insecure             bypass certificate checking
      --key string           client key
  -v, --verbose              verbose output

```

```shell
shell> wsk -i action invoke tripleAndIncrement -r -p value 3
{
    "value": 10
}
shell> wsk -i activation list
Datetime            Activation ID                    Kind      Start Duration   Status          Entity
2021-05-21 15:52:31 6947032907c04ee587032907c06ee5ed nodejs:10 warm  6ms        success         guest/tripleAndIncrement:0.0.1
2021-05-21 15:52:31 afbf2d37e30f4ac8bf2d37e30f0ac8e0 nodejs:10 warm  4ms        success         guest/increment:0.0.1
2021-05-21 15:52:31 3e33a258f3d9462eb3a258f3d9562e78 nodejs:10 warm  4ms        success         guest/tripleAndIncrement:0.0.1
2021-05-21 15:52:31 169b7e68392348769b7e68392318767a nodejs:10 warm  3ms        success         guest/triple:0.0.1
2021-05-21 15:52:31 a5890b70174c4c89890b70174cdc896f nodejs:10 warm  3ms        success         guest/tripleAndIncrement:0.0.1
2021-05-21 15:52:31 19087b10244f4fe6887b10244f5fe6e7 sequence  warm  102ms      success         guest/tripleAndIncrement:0.0.1
...

shell> wsk -i action invoke tripleAndIncrement -p value 3
ok: invoked /_/tripleAndIncrement with id d78b1f9ea45646778b1f9ea456a67778
shell> wsk -i activation list
Datetime            Activation ID                    Kind      Start Duration   Status          Entity
2021-05-21 15:54:23 401e439b45444a789e439b4544da781b nodejs:10 warm  2ms        success         guest/tripleAndIncrement:0.0.1
2021-05-21 15:54:23 262f8592e2ba4c32af8592e2ba6c32f4 nodejs:10 warm  3ms        success         guest/increment:0.0.1
2021-05-21 15:54:23 dd4acaaaf6b346e98acaaaf6b386e93d nodejs:10 cold  41ms       success         guest/tripleAndIncrement:0.0.1
2021-05-21 15:54:23 c4a0ec72572a464da0ec72572a564d07 nodejs:10 warm  3ms        success         guest/triple:0.0.1
2021-05-21 15:54:23 20d4acbdfca84b5494acbdfca88b54b3 nodejs:10 warm  2ms        success         guest/tripleAndIncrement:0.0.1
2021-05-21 15:54:23 d78b1f9ea45646778b1f9ea456a67778 sequence  warm  238ms      success         guest/tripleAndIncrement:0.0.1
...
```
There are six activation records in this example, one matching the activation id returned on invocation (`d78b1f9ea45646778b1f9ea456a67778`) plus five additional records for activations *caused* by this invocation. The *primary* activation record is the last one in the list because it has the earliest start time.

A conductor action should return either an *error* dictionary, i.e., a dictionary with an *error* field, or a *continuation*, i.e., a dictionary with up to three fields `{ action, params, state }`. In essence, a continuation specifies what component action to invoke if any, as well as the parameters for this invocation, and the state to preserve until the next secondary activation of the conductor action.

The execution flow in our example is the following:

1. The *tripleAndIncrement* action is invoked on the input dictionary `{ value: 3 }`. It returns `{ action: 'triple', params: { value: 3 }, state: { $step: 1 } }` requesting that action *triple* be invoked on *params* dictionary `{ value: 3 }`.
2. The *triple* action is invoked on dictionary `{ value: 3 }` returning `{ value: 9 }`.
3. The *tripleAndIncrement* action is automatically reactivated. The input dictionary for this activation is `{ value: 9, $step: 1 }` obtained by combining the result of the *triple* action invocation with the *state* of the prior secondary *tripleAndIncrement* activation (see below for details). It returns `{ action: 'increment', params: { value: 9 }, state: { $step: 2 } }`.
4. The *increment* action is invoked on dictionary `{ value: 9 }` returning `{ value: 10 }`.
5. The *tripleAndIncrement* action is automatically reactivated on dictionary `{ value: 10, $step: 2 }` returning `{ params: { value: 10 } }`.
6. Because the output of the last secondary *tripleAndIncrement* activation specifies no further action to invoke, this completes the execution resulting in the recording of the primary activation. The result of the primary activation is obtained from the result of the last secondary activation by extracting the value of the *params* field: `{ value: 10 }`.

### Causality

We say the invocation of the conductor action is the *cause* of *component* action invocations as well as *secondary* activations of the conductor action. These activations are *derived* activations.

The cause field of the *derived* activation records is set to the id for the *primary* activation record.

### Primary activations

The primary activation record for the invocation of a conductor action is a synthetic record similar to the activation record of a sequence action. The primary activation record summarizes the series of derived activations:

- its result is the result of the last action in the series (possibly unboxed, see below),
- its logs are the ordered list of component and secondary activations,
- its duration is the sum of the durations of these activations,
- its start time is less or equal to the start time of the first derived activation in the series,
- its end time is greater or equal to the end time of the last derived activation in the series.

```shell
shell> wsk -i activation get d78b1f9ea45646778b1f9ea456a67778
ok: got activation d78b1f9ea45646778b1f9ea456a67778
{
    "namespace": "guest",
    "name": "tripleAndIncrement",
    "version": "0.0.1",
    "subject": "guest",
    "activationId": "d78b1f9ea45646778b1f9ea456a67778",
    "start": 1621583663536,
    "end": 1621583663774,
    "duration": 51,
    "statusCode": 0,
    "response": {
        "status": "success",
        "statusCode": 0,
        "success": true,
        "result": {
            "value": 10
        }
    },
    "logs": [
        "20d4acbdfca84b5494acbdfca88b54b3",
        "c4a0ec72572a464da0ec72572a564d07",
        "dd4acaaaf6b346e98acaaaf6b386e93d",
        "262f8592e2ba4c32af8592e2ba6c32f4",
        "401e439b45444a789e439b4544da781b"
    ],
    "annotations": [
        {
            "key": "topmost",
            "value": true
        },
        {
            "key": "path",
            "value": "guest/tripleAndIncrement"
        },
        {
            "key": "waitTime",
            "value": 6
        },
        {
            "key": "conductor",
            "value": true
        },
        {
            "key": "kind",
            "value": "sequence"
        },
        {
            "key": "limits",
            "value": {
                "concurrency": 1,
                "logs": 10,
                "memory": 256,
                "timeout": 60000
            }
        }
    ],
    "publish": false
}
```

