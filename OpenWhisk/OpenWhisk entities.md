## Namespaces and packages

OpenWhisk actions, triggers, and rules belong in a namespace, and optionally a package.

Packages can contain actions and feeds. A package cannot contain another package, so package nesting is not allowed. Also, entities do not have to be contained in a package.

In IBM Cloud, an organization+space pair corresponds to a OpenWhisk namespace. For example, the organization `BobsOrg` and space `dev` would correspond to the OpenWhisk namespace `/BobsOrg_dev`.

You can create your own namespaces if you're entitled to do so. The `/whisk.system` namespace is reserved for entities that are distributed with the OpenWhisk system.

## Fully qualified names

The fully qualified name of an entity is `/namespaceName[/packageName]/entityName`. Notice that `/` is used to delimit namespaces, packages, and entities.

If the fully qualified name has three parts: `/namespaceName/packageName/entityName`, then the namespace can be entered without a prefixed `/`; otherwise, namespaces must be prefixed with a `/`.

For convenience, the namespace can be left off if it is the user's *default namespace*.

For example, consider a user whose default namespace is `/myOrg`. Following are examples of the fully qualified names of a number of entities and their aliases.

| Fully qualified name          | Alias             | Namespace       | Package    | Name        |
| ----------------------------- | ----------------- | --------------- | ---------- | ----------- |
| `/whisk.system/cloudant/read` |                   | `/whisk.system` | `cloudant` | `read`      |
| `/myOrg/video/transcode`      | `video/transcode` | `/myOrg`        | `video`    | `transcode` |
| `/myOrg/filter`               | `filter`          | `/myOrg`        |            | `filter`    |

You will be using this naming scheme when you use the OpenWhisk CLI, among other places.

## Entity names

The names of all entities, including actions, triggers, rules, packages, and namespaces, are a sequence of characters that follow the following format:

- The first character must be an alphanumeric character, or an underscore.
- The subsequent characters can be alphanumeric, spaces, or any of the following: `_`, `@`, `.`, `-`.
- The last character can't be a space.

More precisely, a name must match the following regular expression (expressed with Java metacharacter syntax): `\A([\w]|[\w][\w@ .-]*[\w@.-]+)\z`.