## [单文件Python action](https://github.com/apache/openwhisk/blob/master/docs/actions-python.md)

The process of creating Python actions is similar to that of [other actions](https://github.com/apache/openwhisk/blob/master/docs/actions.md#the-basics). The following sections guide you through creating and invoking a single Python action, and demonstrate how to bundle multiple Python files and third party dependencies.

An example action Python action is simply a top-level function. For example, create a file called `hello.py` with the following source code:

```python
def main(args):
    name = args.get("name", "stranger")
    greeting = "Hello " + name + "!"
    print(greeting)
    return {"greeting": greeting}
```

Python actions always consume a dictionary and produce a dictionary. The entry method for the action is `main` by default but may be specified explicitly when creating the action with the `wsk` CLI using `--main`, as with any other action type.

You can create an OpenWhisk action called `helloPython` from this function as follows:

```shell
shell> wsk action create -i helloPython hello.py
ok: created action helloPython
```

The CLI automatically infers the type of the action from the source file extension. For `.py` source files, the action runs using a Python 3.6 runtime.

Action invocation is the same for Python actions as it is for any other actions:

```shell
shell> wsk action invoke -i --result helloPython --param name World
ok: invoked /_/helloPython, but the request has not yet finished, with id 747aa2de87ea4732baa2de87ea2732a0
shell> wsk activation  get -i  747aa2de87ea4732baa2de87ea2732a0
ok: got activation 747aa2de87ea4732baa2de87ea2732a0
{
    "namespace": "guest",
    "name": "helloPython",
    "version": "0.0.1",
    "subject": "guest",
    "activationId": "747aa2de87ea4732baa2de87ea2732a0",
    "start": 1621493324444,
    "end": 1621493324444,
    "duration": 0,
    "statusCode": 3,
    "response": {
        "status": "whisk internal error",
        "statusCode": 0,
        "success": false,
        "result": {
            "error": "Failed to run container with image 'openwhisk/actionloop-python-v3.7:1.16.0'."
        }
    },
    "logs": [],
    "annotations": [
        {
            "key": "path",
            "value": "guest/helloPython"
        },
        {
            "key": "waitTime",
            "value": 60514
        },
        {
            "key": "kind",
            "value": "python:3"
        },
        {
            "key": "timeout",
            "value": false
        },
        {
            "key": "limits",
            "value": {
                "concurrency": 1,
                "logs": 10,
                "memory": 256,
                "timeout": 60000
            }
        }
    ],
    "publish": false
}
# 运行时OpenWhisk会从docker hub（https://hub.docker.com/search?q=openwhisk&type=image）上拉取需要的镜像，上面这个问题就是拉取镜像超时了
# 对于正式系统的建议是1预先拉取镜像，2不使用镜像的latest标签防止镜像拉取失败

shell> wsk action invoke -i --result helloPython --param name World
{
    "greeting": "Hello World!"
}
```

Find out more about parameters in the [Working with parameters](https://github.com/apache/openwhisk/blob/master/docs/parameters.md) section.

## 多文件Python action

You can package a Python action and dependent modules in a zip file. The filename of the source file containing the entry point (e.g., `main`) must be `__main__.py`. For example, to create an action with a helper module called `ip_helper.py`, first create an archive containing your source files:

```shell
shell> vi __main__.py
import json

from ip_helper import get_local_ip


def main(args):
    name = args.get("name", "stranger")
    greeting = "Hello " + name + "!"
    ip = get_local_ip()
    print(greeting)
    print(ip)
    return {"greeting": greeting, "ip": ip}


if __name__ == "__main__":
    main(json.loads("{\"name\": \"测试用户\"}"))
```
```shell
shell> vi ip_helper.py
import socket


def get_local_ip():
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    return ip
```

```shell
shell> zip -r helloPythonM.zip __main__.py ip_helper.py
shell> wsk action create -i helloPythonM --kind python:3 helloPythonM.zip
ok: created action helloPythonM
# 如果要更新代码执行以下代码
shell> wsk action update -i helloPythonM --kind python:3 helloPythonM.zip
ok: updated action helloPythonM
shell> wsk action invoke -i --result helloPythonM --param name World
{
    "greeting": "Hello World!",
    "ip": "10.244.1.77"
}
```

## virtual environment Python action

Another way of packaging Python dependencies is using a virtual environment (`virtualenv`). This allows you to link additional packages that may be installed via [`pip`](https://packaging.python.org/installing/) for example. To ensure compatibility with the OpenWhisk container, package installations inside a virtualenv must be done in the target environment. So the docker image `openwhisk/python3action` should be used to create a virtualenv directory for your action.

As with basic zip file support, the name of the source file containing the main entry point must be `__main__.py`. In addition, the virtualenv directory must be named `virtualenv`. Below is an example scenario for installing dependencies, packaging them in a virtualenv, and creating a compatible OpenWhisk action.

1. Given a `requirements.txt` file that contains the `pip` modules and versions to install, run the following to install the dependencies and create a virtualenv using a compatible Docker image:

```shell
shell> vi requirements.txt
idna==3.1
```

```shell
shell> vi __main__.py
import json
import idna

from ip_helper import get_local_ip


def main(args):
    name = args.get("name", "stranger")
    greeting = "Hello " + name + "!"
    ip = get_local_ip()
    url = idna.encode('新浪.中国')
    print(greeting)
    print(ip)
    print(url)
    return {"greeting": greeting, "ip": ip, "url": url}


if __name__ == "__main__":
    main(json.loads("{\"name\": \"测试用户\"}"))

```

- 这一步是为了生成适配于Docker环境的虚拟环境目录

```shell
shell> docker run --rm -v "$PWD:/tmp" openwhisk/python3action bash \
>   -c "cd tmp && virtualenv virtualenv && source virtualenv/bin/activate && pip install -r requirements.txt"
Unable to find image 'openwhisk/python3action:latest' locally
latest: Pulling from openwhisk/python3action
c9b1b535fdd9: Pull complete 
2cc5ad85d9ab: Pull complete 
3ef3538413cf: Pull complete 
ca9525f635d9: Pull complete 
e99c5e32063e: Pull complete 
bfa994af960d: Pull complete 
cda00eb97034: Pull complete 
5c7c4feac90f: Pull complete 
b9252da03011: Pull complete 
43067ccc7a9d: Pull complete 
f2670cc7fa17: Pull complete 
9cc0a0d8e8dd: Pull complete 
fe84c31c6402: Pull complete 
d9cdf1c0a3cc: Pull complete 
8b800b49c4e4: Pull complete 
af66588d76b8: Pull complete 
0d5b028ebda0: Pull complete 
Digest: sha256:7e7bd46232f2e4d5778bbf13a8a8cd986f69eb4221c8ef70d5eb01393508f714
Status: Downloaded newer image for openwhisk/python3action:latest
Using base prefix '/usr/local'
New python executable in /tmp/virtualenv/bin/python
Installing setuptools, pip, wheel...done.
Collecting idna==3.1
  Downloading idna-3.1-py3-none-any.whl (58 kB)
Installing collected packages: idna
Successfully installed idna-3.1
shell> ll
total 16
-rw-r--r--. 1 root root 624 May 20 16:31 helloPythonM.zip
-rw-r--r--. 1 root root 126 May 20 16:27 ip_helper.py
-rw-r--r--. 1 root root 405 May 20 16:57 __main__.py
-rw-r--r--. 1 root root  10 May 20 16:55 requirements.txt
drwxr-xr-x. 5 root root  69 May 20 17:01 virtualenv
shell> ll virtualenv/
total 4
drwxr-xr-x. 2 root root 204 May 20 17:01 bin
drwxr-xr-x. 2 root root  24 May 20 17:00 include
drwxr-xr-x. 3 root root  23 May 20 17:00 lib
-rw-r--r--. 1 root root  61 May 20 17:01 pip-selfcheck.json
```

2. Archive the virtualenv directory and any additional Python files:

```shell
shell> zip -r helloPythonE.zip virtualenv __main__.py ip_helper.py
```

3. Create the action:

```shell
shell> wsk action create -i helloPythonE --kind python:3 helloPythonE.zip
ok: created action helloPythonE
shell> wsk action invoke -i --result helloPythonE --param name World
{
    "error": "command exited"
}
```

4. 解决问题

```shell
shell>  wsk activation
work with activations
Usage:
  wsk activation [command]

Available Commands:
  get         get activation
  list        list activations
  logs        get the logs of an activation
  poll        poll continuously for log messages from currently running actions
  result      get the result of an activation

Flags:
  -h, --help   help for activation

Global Flags:
      --apihost HOST         whisk API HOST
      --apiversion VERSION   whisk API VERSION
  -u, --auth KEY             authorization KEY
      --cert string          client cert
  -d, --debug                debug level output
  -i, --insecure             bypass certificate checking
      --key string           client key
  -v, --verbose              verbose output

Use "wsk activation [command] --help" for more information about a command.

shell> wsk activation -i  list
Datetime            Activation ID                    Kind      Start Duration   Status          Entity
2021-05-20 17:17:21 a015cea8038a488095cea8038a188087 python:3  cold  772ms      developer error guest/helloPythonE:0.0.1
2021-05-20 17:06:28 8db8e78ed6c948bdb8e78ed6c958bd7d python:3  cold  800ms      developer error guest/helloPythonE:0.0.1
2021-05-20 16:32:28 b2aa6026ea4341d3aa6026ea43b1d3a5 python:3  cold  128ms      success         guest/helloPythonM:0.0.2
2021-05-20 16:28:58 b64b2819c3a34ded8b2819c3a3cded79 python:3  cold  126ms      success         guest/helloPythonM:0.0.1
2021-05-20 15:07:39 6138ec6eed5f4b57b8ec6eed5f0b57ea python:3  cold  115ms      success         guest/helloPython:0.0.1

shell> wsk activation -i  logs a015cea8038a488095cea8038a188087
2021-05-20T09:17:21.882921988Z stdout: Hello World!
2021-05-20T09:17:21.882947654Z stdout: 10.244.1.79
2021-05-20T09:17:21.882954939Z stdout: b'xn--efvx5o.xn--fiqs8s'
2021-05-20T09:17:21.88373613Z  stdout: Traceback (most recent call last):
2021-05-20T09:17:21.883753098Z stdout:   File "exec__.py", line 67, in <module>
2021-05-20T09:17:21.883763014Z stdout:     out.write(json.dumps(res, ensure_ascii=False).encode('utf-8'))
2021-05-20T09:17:21.883768813Z stdout:   File "/usr/local/lib/python3.7/json/__init__.py", line 238, in dumps
2021-05-20T09:17:21.883773822Z stdout:     **kw).encode(obj)
2021-05-20T09:17:21.883779275Z stdout:   File "/usr/local/lib/python3.7/json/encoder.py", line 199, in encode
2021-05-20T09:17:21.883783899Z stdout:     chunks = self.iterencode(o, _one_shot=True)
2021-05-20T09:17:21.883789181Z stdout:   File "/usr/local/lib/python3.7/json/encoder.py", line 257, in iterencode
2021-05-20T09:17:21.883794499Z stdout:     return _iterencode(o, 0)
2021-05-20T09:17:21.883799336Z stdout:   File "/usr/local/lib/python3.7/json/encoder.py", line 179, in default
2021-05-20T09:17:21.883804015Z stdout:     raise TypeError(f'Object of type {o.__class__.__name__} '
2021-05-20T09:17:21.883820857Z stdout: TypeError: Object of type bytes is not JSON serializable
2021-05-20T09:17:21.891127509Z stderr: The action did not initialize or run as expected. Log data might be missing.
```

````shell
shell> vi __main__.py
import json
import idna

from ip_helper import get_local_ip


def main(args):
    name = args.get("name", "stranger")
    greeting = "Hello " + name + "!"
    ip = get_local_ip()
    url = str(idna.encode('新浪.中国'), encoding="utf-8")
    print(greeting)
    print(ip)
    print(url)
    return {"greeting": greeting, "ip": ip, "url": url}


if __name__ == "__main__":
    main(json.loads("{\"name\": \"测试用户\"}"))
````

```shell
shell> zip -r helloPythonE.zip virtualenv __main__.py ip_helper.py
shell> wsk action update -i helloPythonE --kind python:3 helloPythonE.zip
ok: updated action helloPythonE
```

```shell
shell> wsk action invoke -i --result helloPythonE --param name World
{
    "greeting": "Hello World!",
    "ip": "10.244.1.80",
    "url": "xn--efvx5o.xn--fiqs8s"
}
```

## Python 3 actions

Python 3 actions are executed using Python 3.6.1. This is the default runtime for Python actions, unless you specify the `--kind` flag when creating or updating an action. The following packages are available for use by Python actions, in addition to the Python 3.6 standard libraries.

- aiohttp v1.3.3
- appdirs v1.4.3
- asn1crypto v0.21.1
- async-timeout v1.2.0
- attrs v16.3.0
- beautifulsoup4 v4.5.1
- cffi v1.9.1
- chardet v2.3.0
- click v6.7
- cryptography v1.8.1
- cssselect v1.0.1
- Flask v0.12
- gevent v1.2.1
- greenlet v0.4.12
- httplib2 v0.9.2
- idna v2.5
- itsdangerous v0.24
- Jinja2 v2.9.5
- kafka-python v1.3.1
- lxml v3.6.4
- MarkupSafe v1.0
- multidict v2.1.4
- packaging v16.8
- parsel v1.1.0
- pyasn1 v0.2.3
- pyasn1-modules v0.0.8
- pycparser v2.17
- PyDispatcher v2.0.5
- pyOpenSSL v16.2.0
- pyparsing v2.2.0
- python-dateutil v2.5.3
- queuelib v1.4.2
- requests v2.11.1
- Scrapy v1.1.2
- service-identity v16.0.0
- simplejson v3.8.2
- six v1.10.0
- Twisted v16.4.0
- w3lib v1.17.0
- Werkzeug v0.12
- yarl v0.9.8
- zope.interface v4.3.3

##  available packages Python action

- 使用Docker镜像预置的package

```shell
shell> wsk -i action list
actions
/guest/helloPythonE                                                    private python:3
/guest/helloPythonM                                                    private python:3
/guest/helloPython                                                     private python:3
/guest/greetingjs                                                      private nodejs:10

shell> wsk -i action get /guest/helloPythonE
ok: got action helloPythonE
{
    "namespace": "guest",
    "name": "helloPythonE",
    "version": "0.0.2",
    "exec": {
        "kind": "python:3",
        "binary": true
    },
    "annotations": [
        {
            "key": "provide-api-key",
            "value": false
        },
        {
            "key": "exec",
            "value": "python:3"
        }
    ],
    "limits": {
        "timeout": 60000,
        "memory": 256,
        "logs": 10,
        "concurrency": 1
    },
    "publish": false,
    "updated": 1621503242125
}
```

- [openwhisk/python3action](openwhisk/python3action)

```shell
shell> zip -r helloPythonA.zip __main__.py ip_helper.py
  adding: __main__.py (deflated 34%)
  adding: ip_helper.py (deflated 35%)
shell> wsk action create -i helloPythonA --kind python:3 helloPythonA.zip
ok: created action helloPythonA
shell> wsk action create -i helloPythonA --kind python:3 helloPythonA.zip
ok: created action helloPythonA
# helloPythonA.zip中不包含virtualenv依旧可以执行
shell> wsk action invoke -i --result helloPythonA --param name World
{
    "greeting": "Hello World!",
    "ip": "10.244.1.81",
    "url": "xn--efvx5o.xn--fiqs8s"
}
shell>  unzip -l helloPythonA.zip 
Archive:  helloPythonA.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      428  05-20-2021 17:32   __main__.py
      126  05-20-2021 16:27   ip_helper.py
---------                     -------
      554                     2 files
```

