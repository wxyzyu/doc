## 安装Kubectl

```shell
shell> cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubectl
```

## 设置Go环境变量

```shell
shell> echo "export GO111MODULE=on" >> /etc/profile
shell> echo "export GOPROXY=https://goproxy.cn" >> /etc/profile
shell> echo export PATH="$(go env GOPATH)/bin:${PATH}" >> /etc/profile
shell> source /etc/profile
```

## 安装Kind

```shell
shell> GO111MODULE=on GOPROXY=https://goproxy.cn,direct go get sigs.k8s.io/kind@v0.10.0
go: downloading sigs.k8s.io/kind v0.10.0
go: downloading github.com/spf13/pflag v1.0.5
go: downloading github.com/spf13/cobra v1.0.0
go: downloading github.com/pkg/errors v0.9.1
go: downloading github.com/mattn/go-isatty v0.0.12
go: downloading k8s.io/apimachinery v0.19.2
go: downloading github.com/alessio/shellescape v1.2.2
go: downloading gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
go: downloading sigs.k8s.io/yaml v1.2.0
go: downloading github.com/BurntSushi/toml v0.3.1
go: downloading github.com/pelletier/go-toml v1.8.1
go: downloading golang.org/x/sys v0.0.0-20200928205150-006507a75852
go: downloading github.com/evanphx/json-patch v4.9.0+incompatible
go: downloading github.com/inconshreveable/mousetrap v1.0.0
go: downloading gopkg.in/yaml.v2 v2.2.8
go: downloading github.com/evanphx/json-patch/v5 v5.1.0
shell> kind create cluster
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.20.2) 🖼 
 ✓ Preparing nodes 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️ 
 ✓ Installing CNI 🔌 
 ✓ Installing StorageClass 💾 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Have a nice day! 👋

# 查看cluster状态
shell> kind get clusters
kind
shell> kubectl cluster-info --context kind-kind
Kubernetes control plane is running at https://127.0.0.1:39205
KubeDNS is running at https://127.0.0.1:39205/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## [使用Kind创建Kubernetes Cluster](https://github.com/apache/openwhisk-deploy-kube/blob/master/docs/k8s-kind.md)

```shell
shell> vi kind-cluster.yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
  extraPortMappings:
    - hostPort: 31001
      containerPort: 31001 
- role: worker
```

```shell
shell> kind delete cluster --name kind
Deleting cluster "kind" ...
shell> kind create cluster --config kind-cluster.yaml
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.20.2) 🖼
 ✓ Preparing nodes 📦 📦 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️ 
 ✓ Installing CNI 🔌 
 ✓ Installing StorageClass 💾 
 ✓ Joining worker nodes 🚜 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Have a nice day! 👋

shell> kubectl cluster-info --context kind-kind
Kubernetes control plane is running at https://127.0.0.1:36362
KubeDNS is running at https://127.0.0.1:36362/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## 给两个worker node打label

```
shell> kubectl label node kind-worker openwhisk-role=core
node/kind-worker labeled
shell> kubectl label node kind-worker2 openwhisk-role=invoker
node/kind-worker2 labeled
```

## 配置OpenWhisk

- ### 第一步，使用入下代码获取worker node的internalIP :

```shell
shell> kubectl describe node kind-worker | grep InternalIP: | awk '{print $2}'
172.18.0.3
```

- ### 第二步,新建mycluster.yaml:

```
shell> vi mycluster.yaml
whisk:
  ingress:
    type: NodePort
    apiHostName: 172.18.0.3
    apiHostPort: 31001

invoker:
  containerFactory:
    impl: "kubernetes"

nginx:
  httpsNodePort: 31001
```

## 安装Helm

```shell
shell> wget https://get.helm.sh/helm-v3.5.4-linux-amd64.tar.gz
shell> tar -zxvf helm-v3.5.4-linux-amd64.tar.gz
linux-amd64/
linux-amd64/helm
linux-amd64/LICENSE
linux-amd64/README.md
shell> mv linux-amd64/helm /usr/local/bin/helm
shell> helm help
The Kubernetes package manager

Common actions for Helm:

- helm search:    search for charts
- helm pull:      download a chart to your local directory to view
- helm install:   upload the chart to Kubernetes
- helm list:      list releases of charts
...
```

## [使用Helm 部署OpenWhisk](https://github.com/apache/openwhisk-deploy-kube#deploy-with-helm)

```shell
shell> helm repo add openwhisk https://openwhisk.apache.org/charts
"openwhisk" has been added to your repositories
shell> helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "openwhisk" chart repository
Update Complete. ⎈Happy Helming!⎈

shell> helm install owdev openwhisk/openwhisk -n openwhisk --create-namespace -f mycluster.yaml
NAME: owdev
LAST DEPLOYED: Mon May 17 22:35:41 2021
NAMESPACE: openwhisk
STATUS: deployed
REVISION: 1
NOTES:
Apache OpenWhisk
Copyright 2016-2020 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

To configure your wsk cli to connect to it, set the apihost property
using the command below:

  $ wsk property set --apihost 172.18.0.3:31001

Your release is named owdev.

To learn more about the release, try:

  $ helm status owdev
  $ helm get owdev

# owdev-install-packages 状态为Completed说明安装完成
Once the 'owdev-install-packages' Pod is in the Completed state, your OpenWhisk deployment is ready to be used.

Once the deployment is ready, you can verify it using: 

  $ helm test owdev --cleanup
  
```

## 检查状态

```shell
shell> kubectl get pods -n openwhisk --watch
NAME                                      READY   STATUS      RESTARTS   AGE
owdev-alarmprovider-5b86cb64ff-tmv82      1/1     Running     0          88m
owdev-apigateway-bccbbcd67-949z8          1/1     Running     0          88m
owdev-controller-0                        1/1     Running     0          88m
owdev-couchdb-584676b956-sjrs9            1/1     Running     0          88m
owdev-gen-certs-jblmp                     0/1     Completed   0          88m
owdev-init-couchdb-mfrhc                  0/1     Completed   0          88m
owdev-install-packages-7544b              0/1     Completed   0          54m
＃发现owdev-install-packages执行失败一直在重试
owdev-install-packages-hvj88              0/1     Error       0          88m
owdev-install-packages-v9fwf              0/1     Error       0          73m
owdev-install-packages-z94nr              0/1     Error       0          71m
owdev-invoker-0                           1/1     Running     0          88m
owdev-kafka-0                             1/1     Running     0          88m
owdev-kafkaprovider-5574d4bf5f-f8ldg      1/1     Running     0          88m
owdev-nginx-86749d59cb-9fl98              1/1     Running     0          88m
owdev-redis-d65649c5b-m8xpq               1/1     Running     0          88m
owdev-wskadmin                            1/1     Running     0          88m
owdev-zookeeper-0                         1/1     Running     0          88m
wskowdev-invoker-00-27-prewarm-nodejs10   1/1     Running     0          64m
wskowdev-invoker-00-28-prewarm-nodejs10   1/1     Running     0          64m
```

## 解决问题

```shell
shell>　kubectl logs job/owdev-install-packages --namespace=openwhisk
Found 4 pods, using pod/owdev-install-packages-hvj88
Cloning into 'openwhisk'...
fatal: unable to access 'https://github.com/apache/openwhisk/': gnutls_handshake() failed: The TLS connection was non-properly terminated.

shell> kubectl logs pod/owdev-install-packages-v9fwf --namespace=openwhisk
Cloning into 'openwhisk'...
fatal: unable to access 'https://github.com/apache/openwhisk/': gnutls_handshake() failed: The TLS connection was non-properly terminated.
# 解决git无法clone的问题

```

## 安装Wsk

```shell
shell> wget https://github.com/apache/openwhisk-cli/releases/download/1.2.0/OpenWhisk_CLI-1.2.0-linux-amd64.tgz
shell> tar -zxvf OpenWhisk_CLI-1.2.0-linux-amd64.tgz
shell> mv wsk /usr/local/bin/wsk
shell> wsk property set --apihost 172.18.0.3:31001
shell> wsk property set --auth 23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP
```

## 使用Helm chart中的基础测试suite验证OpenWhisk部署

```shell
shell>  helm test owdev -n openwhisk
NAME: owdev
LAST DEPLOYED: Tue May 18 10:15:54 2021
NAMESPACE: openwhisk
STATUS: deployed
REVISION: 1
TEST SUITE:     owdev-tests-package-checker
Last Started:   Tue May 18 11:51:27 2021
Last Completed: Tue May 18 11:51:32 2021
Phase:          Succeeded
TEST SUITE:     owdev-tests-smoketest
Last Started:   Tue May 18 11:51:32 2021
Last Completed: Tue May 18 11:51:43 2021
Phase:          Succeeded
NOTES:
Apache OpenWhisk
Copyright 2016-2020 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

To configure your wsk cli to connect to it, set the apihost property
using the command below:

  $ wsk property set --apihost 172.18.0.2:31001

Your release is named owdev.

To learn more about the release, try:

  $ helm status owdev
  $ helm get owdev

Once the 'owdev-install-packages' Pod is in the Completed state, your OpenWhisk deployment is ready to be used.

Once the deployment is ready, you can verify it using: 

  $ helm test owdev --cleanup
```

## 使用Node.js代码验证OpenWhisk部署

```shell
shell>　vi greeting.js
/**
 * @params is a JSON object with optional fields "name" and "place".
 * @return a JSON object containing the message in a field called "msg".
 */
function main(params) {
  // log the parameters to stdout
  console.log('params:', params);

  // if a value for name is provided, use it else use a default
  var name = params.name || 'stranger';

  // if a value for place is provided, use it else use a default
  var place = params.place || 'somewhere';

  // construct the message using the values for name and place
  return {msg:  'Hello, ' + name + ' from ' + place + '!'};
}

shell>　wsk action create -i greetingjs greeting.js
ok: created action greetingjs
shell>  wsk action invoke -i greetingjs --result --param name wxyzyu
{
    "msg": "Hello, wxyzyu from somewhere!"
}
```

## 工具命令

```shell
# 查看Kubernetes上的部署情况
shell>  kubectl get deployment -n openwhisk
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
owdev-alarmprovider   1/1     1            1           4h48m
owdev-apigateway      1/1     1            1           4h48m
owdev-couchdb         1/1     1            1           4h48m
owdev-kafkaprovider   1/1     1            1           4h48m
owdev-nginx           1/1     1            1           4h48m
owdev-redis           1/1     1            1           4h48m
# 删除某一个pod
shell> kubectl delete pod -n openwhisk owdev-tests-package-checker
# 卸载Helm chart
shell> helm -n openwhisk  uninstall owdev
```

## 总结

- 依赖Go、Docker、Kubectl、Helm，注意配置相关国内镜像
- 使用Kind安装Kubernetes Cluster
- 使用Helm安装OpenWhisk
- 使用Node.js代码验证部署
