## What is Apache OpenWhisk?

Apache OpenWhisk is an open source, distributed [Serverless](https://en.wikipedia.org/wiki/Serverless_computing) platform that executes functions (fx) in response to events at any scale. OpenWhisk manages the infrastructure, servers and scaling using Docker containers so you can focus on building amazing and efficient applications.

The OpenWhisk platform supports a programming model in which developers write functional logic (called [Actions](https://github.com/apache/openwhisk/blob/master/docs/actions.md#openwhisk-actions)), in any supported programming language, that can be dynamically scheduled and run in response to associated events (via [Triggers](https://github.com/apache/openwhisk/blob/master/docs/triggers_rules.md#creating-triggers-and-rules)) from external sources ([Feeds](https://github.com/apache/openwhisk/blob/master/docs/feeds.md#implementing-feeds)) or from HTTP requests. The project includes a REST API-based Command Line Interface (CLI) along with other tooling to support packaging, catalog services and many popular container deployment options.

![Events trigger the Apache OpenWhisk platform to run Actions or functions in various supported languages.](OpenWhisk%20%E7%AE%80%E4%BB%8B.assets/OW-Abstract%20Architecture%20Diagram.png)

## Deploys anywhere

Since Apache OpenWhisk builds its components using containers it easily supports many deployment options both locally and within Cloud infrastructures. Options include many of today's popular Container frameworks such as [Kubernetes and OpenShift](https://github.com/apache/openwhisk-deploy-kube/blob/master/README.md), [Mesos](https://github.com/apache/openwhisk-deploy-mesos/blob/master/README.md) and [Compose](https://github.com/apache/openwhisk-devtools/blob/master/docker-compose/README.md). In general, the community endorses deployment on Kubernetes using [Helm](https://helm.sh/Helm) charts since it provides many easy and convenient implementations for both Devlopers and Operators alike.

![Apache OpenWhisk can deploy on your favorite Cloud container framework.](OpenWhisk%20%E7%AE%80%E4%BB%8B.assets/OW-Deployments.png)

## Write functions in any language

Work with what you know and love. OpenWhisk supports a growing list of your favorite languages such as [Go](https://github.com/apache/openwhisk-runtime-go), [Java](https://github.com/apache/openwhisk-runtime-java), [NodeJS](https://github.com/apache/openwhisk-runtime-nodejs), [.NET](https://github.com/apache/openwhisk-runtime-dotnet), [PHP](https://github.com/apache/openwhisk-runtime-php), [Python](https://github.com/apache/openwhisk-runtime-python), [Ruby](https://github.com/apache/openwhisk-runtime-ruby), [Rust](https://github.com/apache/openwhisk-runtime-rust), [Scala](https://github.com/apache/openwhisk-runtime-java), [Swift](https://github.com/apache/openwhisk-runtime-swift) and even newer, cloud-native ones like [Ballerina](https://github.com/apache/openwhisk-runtime-ballerina). There is even an experimental runtime for [Deno](https://github.com/apache/openwhisk-runtime-deno) in-development.

If you need languages or libraries the current "out-of-the-box" runtimes do not support, you can create and customize your own executables as Zip Actions which run on the [Docker](https://github.com/apache/openwhisk-runtime-docker/blob/master/README.md) runtime by using the [Docker SDK](https://github.com/apache/openwhisk-runtime-docker/blob/master/sdk/docker/README.md). Some examples of how to support other languages using Docker Actions include a tutorial for [Rust](https://medium.com/openwhisk/openwhisk-and-rust-lang-24025734a834) and a completed project for [Haskell](https://github.com/rainbyte/openwhisk-wrapper).

Once you have your function written, use the [wsk CLI](https://openwhisk.apache.org/documentation.html#using-openwhisk), to target your Apache OpenWhisk instance, and run your first action in seconds.

![Node Runtime](OpenWhisk%20%E7%AE%80%E4%BB%8B.assets/OW-Runtimes.png)

##  Integrate easily with many popular services

OpenWhisk makes it simple for developers to integrate their Actions with many popular services using [Packages](https://github.com/apache/openwhisk/blob/master/docs/packages.md) that are provided either as independently developed projects under the OpenWhisk family or as part of our default [Catalog](https://github.com/apache/openwhisk-catalog).

Packages offer integrations with general services such as [Kafka](https://github.com/apache/openwhisk-package-kafka) message queues, databases including [Cloudant](https://github.com/apache/openwhisk-package-cloudant), [Push Notifications](https://github.com/apache/openwhisk-package-pushnotifications) from mobile applications, [Slack](https://github.com/apache/openwhisk-catalog/tree/master/packages/slack) messaging, and [RSS](https://github.com/apache/openwhisk-package-rss) feeds. Development pipelines can take advantage of integrations with [GitHub](https://github.com/apache/openwhisk-catalog/tree/master/packages/github), [JIRA](https://github.com/apache/openwhisk-package-jira), or easily connect with custom data services from the [Weather](https://github.com/apache/openwhisk-catalog/tree/master/packages/weather) company.

You can even use the [Alarms](https://github.com/apache/openwhisk-package-alarms) package to schedule times or recurring intervals to run your Actions.

![OpenWhisk's out-of-box packages easily integrate with our own services](OpenWhisk%20%E7%AE%80%E4%BB%8B.assets/OW-Integrations.png)

## Combine your functions into rich compositions

Code in different languages like JavaScript/NodeJS, Swift, Python, Java or run custom logic by packaging code with Docker. Invoke your code synchronously, asynchronously, or on a schedule. Use higher-level programming constructs like sequences to declaratively chain together multiple actions. Use parameter binding to avoid hardcoding service credentials in your code. And also, debug your code in realtime using variety of [Development Tools](https://openwhisk.apache.org/documentation.html#development_tools).

![OpenWhiks allows you to create compositions from Actions in any supported language.](OpenWhisk%20%E7%AE%80%E4%BB%8B.assets/OW-Runtime-Compositions.png)

## Scaling Per-Request & Optimal Utilization

Run your action ten thousand times in a fraction of a second, or once a week. Action instances scale to meet demand as needed, then disappear. Enjoy optimal utilization where you don't pay for idle resources.

![Apache OpenWhisk automatically scales and maximizes and server utilization as events trigger action functions.](OpenWhisk%20%E7%AE%80%E4%BB%8B.assets/OW-Utilization-Scaling.png)