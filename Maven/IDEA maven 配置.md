# 解决IDEA maven无法自动下载jar源码

## 安装maven(https://maven.apache.org/)

![image-20200523122200396](IDEA%20maven%20%E9%85%8D%E7%BD%AE.assets/image-20200523122200396.png)

### 参照官方文档安装maven

The installation of Apache Maven is a simple process of extracting the archive and adding the `bin` folder with the `mvn` command to the `PATH`.

Detailed steps are:

- Ensure `JAVA_HOME` environment variable is set and points to your JDK installation
- Extract distribution archive in any directory

```
unzip apache-maven-3.6.3-bin.zip
```

or

```
tar xzvf apache-maven-3.6.3-bin.tar.gz
```

Alternatively use your preferred archive extraction tool.

- Add the `bin` directory of the created directory `apache-maven-3.6.3` to the `PATH` environment variable
- Confirm with `mvn -v` in a new shell. The result should look similar to

```
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /opt/apache-maven-3.6.3
Java version: 1.8.0_45, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.8.5", arch: "x86_64", family: "mac"
```

### Windows Tips

- Check environment variable value e.g.

```
echo %JAVA_HOME% C:\Program Files\Java\jdk1.7.0_51
```

- Adding to `PATH`: Add the unpacked distribution’s bin directory to your user PATH environment variable by opening up the system properties (WinKey + Pause), selecting the “Advanced” tab, and the “Environment Variables” button, then adding or selecting the *PATH* variable in the user variables with the value `C:\Program Files\apache-maven-3.6.3\bin`. The same dialog can be used to set `JAVA_HOME` to the location of your JDK, e.g. `C:\Program Files\Java\jdk1.7.0_51`
- Open a new command prompt (Winkey + R then type `cmd`) and run `mvn -v` to verify the installation.

### Unix-based Operating System (Linux, Solaris and Mac OS X) Tips

- Check environment variable value

```
echo $JAVA_HOME
/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home
```

- Adding to `PATH`

```
export PATH=/opt/apache-maven-3.6.3/bin:$PATH
```

## 配置maven

- 修改maven安装目录下conf文件夹下的settings.xml配置文件，配置阿里云的maven镜像

```xml
 <!-- mirrors
   | This is a list of mirrors to be used in downloading artifacts from remote repositories.
   |
   | It works like this: a POM may declare a repository to use in resolving certain artifacts.
   | However, this repository may have problems with heavy traffic at times, so people have mirrored
   | it to several places.
   |
   | That repository definition will have a unique id, so we can create a mirror reference for that
   | repository, to be used as an alternate download site. The mirror site will be the preferred
   | server for that repository.
   |-->
<mirrors>
    <!-- mirror
     | Specifies a repository mirror site to use instead of a given repository. The repository that
     | this mirror serves has an ID that matches the mirrorOf element of this mirror. IDs are used
     | for inheritance and direct lookup purposes, and must be unique across the set of mirrors.
     |
     -->
    <mirror>
        <id>nexus-aliyun</id>
        <mirrorOf>*</mirrorOf>
        <name>Nexus aliyun</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
    </mirror>
</mirrors>
```

- 更改maven自动帮你下载的jar的存放目录(可选)

```xml
<!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ${user.home}/.m2/repository
-->
<localRepository>D:\Program Files\apache-maven-3.6.3\repository</localRepository>
```

## 配置IDEA

- 将maven配置应用到全局

![image-20200523124517156](IDEA%20maven%20%E9%85%8D%E7%BD%AE.assets/image-20200523124517156.png)

- 为IDEA配置本地maven和配置文件，并指定local repository目录

![image-20200523124351253](IDEA%20maven%20%E9%85%8D%E7%BD%AE.assets/image-20200523124351253.png)

至此，maven已经能自动下载jar的源码。