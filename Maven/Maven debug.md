## 服务器端首先找到Maven安装目录

```shell
$ which mvnDebug
/opt/module/java/apache-maven-3.8.4/bin/mvnDebug
```

## 修改服务器端mvnDebug配置

```shell
$ vi /opt/module/java/apache-maven-3.8.4/bin/mvnDebug
```

```shell
# 注释掉默认配置，注意0.0.0.0代表允许任意ip连接，需要根据实际环境配置
# MAVEN_DEBUG_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=${MAVEN_DEBUG_ADDRESS:-8000}"
MAVEN_DEBUG_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=0.0.0.0:8005"

echo Preparing to execute Maven in debug mode

env MAVEN_OPTS="$MAVEN_OPTS" MAVEN_DEBUG_OPTS="$MAVEN_DEBUG_OPTS" "`dirname "$0"`/mvn" "$@"

```

## 在客户端IDEA种添加远程debug配置

![image-20211228144207350](Maven%20debug.assets/image-20211228144207350.png) 

## 在服务器端执行命令等待连接

```shell
$ mvnDebug package
Preparing to execute Maven in debug mode
Listening for transport dt_socket at address: 8005
```

## 在客户端IDEA启动debug

- 选中刚才添加的远程debug配置，启动调试，如果提示connection refused，需要先检查防火墙等配置，可以用 telnet ip port命令测试

![image-20211228144934354](Maven%20debug.assets/image-20211228144934354.png)   



