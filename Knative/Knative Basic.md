# [Knative Tutorial](https://redhat-developer-demos.github.io/knative-tutorial/knative-tutorial/index.html)

## Tutorial Sources

```bash
$　https://github.com/redhat-developer-demos/knative-tutorial.git
$  export BOOK_HOME="$(pwd)/knative-tutorial"
```



## Tutorial Tools

The following CLI tools are required for running the exercises in this tutorial. Please have them installed and configured before you get started with any of the tutorial chapters.



|                   **Tool**                   |                          **Centos**                          |
| :------------------------------------------: | :----------------------------------------------------------: |
|                     Git                      |                       yum install git                        |
|                   `Docker`                   | [Install Docker Engine on CentOS](https://docs.docker.com/engine/install/centos/) |
|                  `kubectl`                   | [Install kubectl binary with curl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) |
|  [stern](https://github.com/wercker/stern)   | [Stern release page](https://github.com/wercker/stern/releases) |
| [yq v2.4.1](https://github.com/mikefarah/yq) | [Yq release page](https://github.com/mikefarah/yq/releases)  |
|        [httpie](https://httpie.org/)         |                     `pip install httpie`                     |
|     [hey](https://github.com/rakyll/hey)     | [Download](https://hey-release.s3.us-east-2.amazonaws.com/hey_linux_amd64) |
|                    watch                     |                     `yum install watch`                      |
|              kubectx and kubens              | [Kubectx release page](https://github.com/ahmetb/kubectx/releases) |
|                     helm                     |  [Helm release page](https://github.com/helm/helm/releases)  |
|                   minikube                   | [Install minikube on linux](https://minikube.sigs.k8s.io/docs/start/) |



## [Install kubectl binary with curl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

1. ### Download the latest release with the command:

   ```bash
   $ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
   100   154  100   154    0     0    205      0 --:--:-- --:--:-- --:--:--   205
   100 44.7M  100 44.7M    0     0  9922k      0  0:00:04  0:00:04 --:--:-- 11.8M
   ```

   > **Note:**
   >
   > To download a specific version, replace the `$(curl -L -s https://dl.k8s.io/release/stable.txt)` portion of the command with the specific version.
   >
   > For example, to download version v1.22.0 on Linux, type:
   >
   > ```bash
   > $ curl -LO https://dl.k8s.io/release/v1.22.0/bin/linux/amd64/kubectl
   > ```

2. ### Validate the binary (optional)

   Download the kubectl checksum file:

   ```bash
   $ curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
   100   154  100   154    0     0    224      0 --:--:-- --:--:-- --:--:--   224
   100    64  100    64    0     0     53      0  0:00:01  0:00:01 --:--:--    53
   ```

   Validate the kubectl binary against the checksum file:

   ```bash
   $ echo "$(<kubectl.sha256) kubectl" | sha256sum --check
   ```

   If valid, the output is:

   ```console
   kubectl: OK
   ```

   If the check fails, `sha256` exits with nonzero status and prints output similar to:

   ```bash
   kubectl: FAILED
   sha256sum: WARNING: 1 computed checksum did NOT match
   ```

   > **Note:** Download the same version of the binary and checksum.

3. ### Install kubectl

   ```bash
   $ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
   ```

   > **Note:**
   >
   > If you do not have root access on the target system, you can still install kubectl to the `~/.local/bin` directory:
   >
   > ```bash
   > $ chmod +x kubectl
   > $ mkdir -p ~/.local/bin/kubectl
   > $ mv ./kubectl ~/.local/bin/kubectl
   > # and then add ~/.local/bin/kubectl to $PATH
   > ```

4. ### Test to ensure the version you installed is up-to-date:

   ```bash
   $ kubectl version --client
   Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:38:50Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
   ```



## Install  stern

1. ### Download from [release page](https://github.com/wercker/stern/releases)

   ```bash
   $ wget https://github.com/wercker/stern/releases/download/1.11.0/stern_linux_amd64
   ```

2. ### Link to /usr/bin/stern

   ```bash
   $ sudo install -o root -g root -m 0755 stern_linux_amd64 /usr/local/bin/stern
   ```

3. ### Test

   ```bash
   $ stern --version
   stern version 1.11.0
   ```

   

## Install yq

1. ### Download from [binary release](https://github.com/mikefarah/yq/releases)

   ```bash
   $ wget https://github.com/mikefarah/yq/releases/download/2.4.1/yq_linux_amd64
   ```

2. ### Install yq

   ```bash
   $ sudo install -o root -g root -m 0755 yq_linux_amd64 /usr/local/bin/yq
   ```

3. ### Test

   ```bash
   $ yq --version
   yq version 2.4.1
   ```

   

## Install httpie

1. ### Install httpid with pip3

   ```bash
   $ pip install httpie
   ```

2. ### Test

   ```bash
   $ http --version
   2.5.0
   ```

3. ### Hello world

   ```bash
   $ https httpie.io/hello
   HTTP/1.1 200 OK
   Connection: keep-alive
   Content-Type: application/json; charset=utf-8
   age: 0
   cache-control: public, max-age=0, must-revalidate
   content-length: 261
   date: Mon, 27 Sep 2021 09:37:39 GMT
   etag: "105-tuZg7HTShsOZ9QXSZS0pqWYQ2Ac"
   server: Vercel
   strict-transport-security: max-age=63072000
   x-matched-path: /api/hello
   x-vercel-cache: MISS
   x-vercel-id: hnd1::iad1::hjgvp-1632735458397-7a173326e3ee
   
   {
       "ahoy": [
           "Hello, World! 👋 Thank you for trying out HTTPie 🥳",
           "We hope this will become a friendship."
       ],
       "links": {
           "discord": "https://httpie.io/chat",
           "github": "https://github.com/httpie",
           "homepage": "https://httpie.io",
           "twitter": "https://twitter.com/httpie"
       }
   }
   ```



## Install hey

1. ### Download from [binary release](https://hey-release.s3.us-east-2.amazonaws.com/hey_linux_amd64)

   ```bash
   $ wget https://hey-release.s3.us-east-2.amazonaws.com/hey_linux_amd64
   ```

2. ### Install yq

   ```bash
   $ sudo install -o root -g root -m 0755 hey_linux_amd64 /usr/local/bin/hey
   ```

3. ### Test

   ```bash
   $ hey
   Usage: hey [options...] <url>
   
   Options:
     -n  Number of requests to run. Default is 200.
     -c  Number of workers to run concurrently. Total number of requests cannot
         be smaller than the concurrency level. Default is 50.
     -q  Rate limit, in queries per second (QPS) per worker. Default is no rate limit.
     -z  Duration of application to send requests. When duration is reached,
         application stops and exits. If duration is specified, n is ignored.
         Examples: -z 10s -z 3m.
     -o  Output type. If none provided, a summary is printed.
         "csv" is the only supported alternative. Dumps the response
         metrics in comma-separated values format.
   ```

   

## Install  kubectx

1. ### Download from [release page](https://github.com/ahmetb/kubectx/releases)

   ```bash
   $ wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx_v0.9.4_linux_x86_64.tar.gz
   $ wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
   ```

2. ### Install  kubectx and  kubens

   ```bash
   $ sudo install -o root -g root -m 0755 kubectx /usr/local/bin/kubectx
   $ sudo install -o root -g root -m 0755 kubens /usr/local/bin/kubens
   ```

3. ### Test

   ```bash
   $ kubectx --help
   USAGE:
     kubectx                       : list the contexts
     kubectx <NAME>                : switch to context <NAME>
     kubectx -                     : switch to the previous context
     kubectx -c, --current         : show the current context name
     kubectx <NEW_NAME>=<NAME>     : rename context <NAME> to <NEW_NAME>
     kubectx <NEW_NAME>=.          : rename current-context to <NEW_NAME>
     kubectx -u, --unset           : unset the current context
     kubectx -d <NAME> [<NAME...>] : delete context <NAME> ('.' for current-context)
                                     (this command won't delete the user/cluster entry
                                      referenced by the context entry)
     kubectx -h,--help             : show this message
     kubectx -V,--version          : show version
   
   $ kubens --help
   USAGE:
     kubens                    : list the namespaces in the current context
     kubens <NAME>             : change the active namespace of current context
     kubens -                  : switch to the previous namespace in this context
     kubens -c, --current      : show the current namespace
     kubens -h,--help          : show this message
     kubens -V,--version       : show version
   ```

   

## Install  helm

1. ### Download from [release page](https://github.com/helm/helm/releases)

   ```bash
   $ wget https://get.helm.sh/helm-v3.7.0-linux-amd64.tar.gz
   ```

2. ### Install  helm

   ```bash
   $ tar -zxvf helm-v3.7.0-linux-amd64.tar.gz
   $ sudo install -o root -g root -m 0755 linux-amd64/helm /usr/local/bin/helm
   ```

3. ### Test

   ```bash
   $ helm version
   version.BuildInfo{Version:"v3.7.0", GitCommit:"eeac83883cb4014fe60267ec6373570374ce770b", GitTreeState:"clean", GoVersion:"go1.16.8"}
   ```

   

## Install  minikube

1. ### Download from [binary release ](https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64)

   ```bash
   $ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
   ```

2. ### Install  minikube

   ```bash
   $ sudo install -o root -g root -m 0755 minikube-linux-amd64 /usr/local/bin/minikube
   ```

3. ### Test

   ```bash
   $ minikube -h
   minikube provisions and manages local Kubernetes clusters optimized for development workflows.
   
   Basic Commands:
     start          Starts a local Kubernetes cluster
     status         Gets the status of a local Kubernetes cluster
     stop           Stops a running local Kubernetes cluster
     delete         Deletes a local Kubernetes cluster
     dashboard      Access the Kubernetes dashboard running within the minikube cluster
     pause          pause Kubernetes
     unpause        unpause Kubernetes
   ```

4. ### Start minikube

   ```bash
   $ $BOOK_HOME/bin/start-minikube.sh
   * [knativetutorial] minikube v1.23.2 on Centos 7.6.1810
   * Automatically selected the docker driver. Other choices: none, ssh
   ! For improved Docker performance, Upgrade Docker to a newer version (Minimum recommended version is 18.09.0)
   ! Your cgroup does not allow setting memory.
     - More information: https://docs.docker.com/engine/install/linux-postinstall/#your-kernel-does-not-support-cgroup-swap-limit-capabilities
   * The "docker" driver should not be used with root privileges.
   * If you are running minikube within a VM, consider using --driver=none:
   *   https://minikube.sigs.k8s.io/docs/reference/drivers/none/
   
   X Exiting due to DRV_AS_ROOT: The "docker" driver should not be used with root privileges.
   # 不允许以root用户运行minikube，请创建一个新用户，然后并确保将该用户添加到docker组
   
   $ useradd minikubelearn
   $ passwd minikubelearn
   $ su minikubelearn
   $ sudo gpasswd -a $USER docker
   [sudo] password for minikubelearn: 
   minikubelearn is not in the sudoers file.  This incident will be reported.
   $ exit
   #ps:这里说下你可以sudoers添加下面四行中任意一条
   # youruser            ALL=(ALL)                ALL
   # %youruser           ALL=(ALL)                ALL
   # youruser            ALL=(ALL)                NOPASSWD: ALL
   # %youruser           ALL=(ALL)                NOPASSWD: ALL
   
   # 第一行:允许用户youruser执行sudo命令(需要输入密码)
   # 第二行:允许用户组youruser里面的用户执行sudo命令(需要输入密码)
   # 第三行:允许用户youruser执行sudo命令,并且在执行的时候不输入密码
   # 第四行:允许用户组youruser里面的用户执行sudo命令,并且在执行的时候不输入密码
   
   $ visudo
   ......
   ## Allow root to run any commands anywhere
   root    ALL=(ALL)       ALL
   minikubelearn    ALL=(ALL)       ALL
   ......
   
   $ su minikubelearn
   $ sudo gpasswd -a $USER docker
   [sudo] password for minikubelearn: 
   Adding user minikubelearn to group docker
   $ sudo service docker restart
   $ newgrp docker
   $ docker ps
   CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
   
   $ $BOOK_HOME/bin/start-minikube.sh
   * [knativetutorial] minikube v1.23.2 on Centos 7.6.1810
   * Automatically selected the docker driver
   ! For improved Docker performance, Upgrade Docker to a newer version (Minimum recommended version is 18.09.0)
   ! Your cgroup does not allow setting memory.
     - More information: https://docs.docker.com/engine/install/linux-postinstall/#your-kernel-does-not-support-cgroup-swap-limit-capabilities
   * Starting control plane node knativetutorial in cluster knativetutorial
   * Pulling base image ...
   ......
   * Suggestion: A firewall is blocking Docker the minikube VM from reaching the image repository. You may need to select --image-repository, or use a proxy.
   * Documentation: https://minikube.sigs.k8s.io/docs/handbook/vpn_and_proxy/
   * Related issues:
     - https://github.com/kubernetes/minikube/issues/3898
     - https://github.com/kubernetes/minikube/issues/6070
   
   # 增加两项参数使用国内镜像
   # --registry-mirror=https://registry.docker-cn.com  --image-repository=registry.cn-hangzhou.aliyuncs.com/google_containers
   $ vim $BOOK_HOME/bin/start-minikube.sh
   #!/bin/bash
   
   set -eu
   
   PROFILE_NAME=${PROFILE_NAME:-knativetutorial}
   MEMORY=${MEMORY:-8192}
   CPUS=${CPUS:-6}
   
   unamestr=$(uname)
   
   if [ "$unamestr" == "Darwin" ];
   then
     minikube start --registry-mirror=https://registry.docker-cn.com --docker-env HTTP_PROXY=socks5://remoteaddr:10080 --docker-env HTTPS_PROXY=socks5://remoteaddr:10080  \
     --memory="$MEMORY" \
     --driver=hyperkit \
     --cpus="$CPUS" \
     --kubernetes-version=v1.19.0 \
     --disk-size=50g \
     --image-mirror-country=cn \
     --insecure-registry='10.0.0.0/24'
   else
     minikube start --registry-mirror=https://registry.docker-cn.com --docker-env HTTP_PROXY=socks5://remoteaddr:10080 --docker-env HTTPS_PROXY=socks5://remoteaddr:10080  \
     --memory="$MEMORY" \
     --cpus="$CPUS" \
     --kubernetes-version=v1.19.0 \
     --disk-size=50g \
     --image-mirror-country=cn \
     --insecure-registry='10.0.0.0/24'
   fi
   
   
   $ $BOOK_HOME/bin/start-minikube.sh
   ......
   * Done! kubectl is now configured to use "knativetutorial" cluster and "default" namespace by default
   ```
   
   

## Install  Internal Registry

1. ### Install  internal registry

   ```bash
   $ minikube addons enable registry
     - Using image registry:2.7.1
     - Using image gcr.io/google_containers/kube-registry-proxy:0.4
   * Verifying registry addon...
   * The 'registry' addon is enabled
   ```

2. ### Test

   ```bash
   $ kubectl get pods -n kube-system
   NAME                               READY   STATUS    RESTARTS   AGE
   coredns-78fcd69978-gtbhr           1/1     Running   0          2m42s
   etcd-minikube                      1/1     Running   0          2m54s
   kube-apiserver-minikube            1/1     Running   0          2m57s
   kube-controller-manager-minikube   1/1     Running   0          2m54s
   kube-proxy-bxxrd                   1/1     Running   0          2m43s
   kube-scheduler-minikube            1/1     Running   0          2m54s
   registry-proxy-glrhs               1/1     Running   0          2m38s
   registry-tbjst                     1/1     Running   0          2m38s
   storage-provisioner                1/1     Running   0          2m54s
   ```

   

## Configuring Container Registry Aliases

1. ### Use tutorial source

   ```bash
   $ cd $BOOK_HOME/apps/minikube-registry-helper
   ```

2. ### Apply config

   ```bash
   $ kubectl apply -n kube-system -f registry-aliases-config.yaml
   $ kubectl apply -n kube-system -f node-etc-hosts-update.yaml
   ```

3. ### Test

   ```bash
   $ watch minikube ssh -- sudo cat /etc/hosts
   Every 2.0s: minikube ssh -- sudo cat /etc/hosts                                                                                      
   
   127.0.0.1	localhost
   ::1     localhost ip6-localhost ip6-loopback
   fe00::0 ip6-localnet
   ff00::0 ip6-mcastprefix
   ff02::1 ip6-allnodes
   ff02::2 ip6-allrouters
   192.168.49.2    minikube
   192.168.49.1    host.minikube.internal
   192.168.49.2    control-plane.minikube.internal
   10.98.155.5     dev.local
   10.98.155.5     example.com
   
   # The IP for dev.local and example.com will match the CLUSTER-IP of the internal container registry. To verify this, run:
   $ kubectl get svc registry -n kube-system
   NAME       TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)          AGE
   registry   ClusterIP   10.98.155.5   <none>        80/TCP,443/TCP   8h
   ```

4. ### Patch  CoreDNS

   ```bash
   $ ./patch-coredns.sh
   configmap/coredns patched
   
   # The rewrite rule will resolve dev.local to the internal registry address registry.kube-system.svc.cluster.local
   $ kubectl get cm -n kube-system coredns -o yaml
   apiVersion: v1
   data:
     Corefile: |-
       .:53 {
           errors
           health {
            rewrite name dev.local  registry.kube-system.svc.cluster.local
            rewrite name example.com  registry.kube-system.svc.cluster.local
   
              lameduck 5s
           }
           ready
           kubernetes cluster.local in-addr.arpa ip6.arpa {
              pods insecure
              fallthrough in-addr.arpa ip6.arpa
              ttl 30
           }
           prometheus :9153
           hosts {
              192.168.49.1 host.minikube.internal
              fallthrough
           }
           forward . /etc/resolv.conf {
              max_concurrent 1000
           }
           cache 30
           loop
           reload
           loadbalance
       }
   kind: ConfigMap
   metadata:
     creationTimestamp: "2021-09-28T18:14:22Z"
     name: coredns
     namespace: kube-system
     resourceVersion: "4039"
     uid: 4316847e-a36b-4a26-95cf-1b8be9187c73
   ```

   

## Install  Istio

1. ### Install istio with install-istio.sh

   ```bash
   $ $BOOK_HOME/bin/install-istio.sh
   Using Istio Version: 1.3.6
   Downloading Istio
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
     0     0    0     0    0     0      0      0 --:--:--  0:00:20 --:--:--     0curl: (7) Failed to connect to 2001::1f0d:5721: Network is unreachable
    
   $ su
   $ vim /etc/profile
   .....
   export http_proxy=socks5://remoteaddr:10080
   export HTTPS_PROXY=socks5://remoteaddr:10080
   export ALL_PROXY=socks5://remoteaddr:10080
   
   $ $BOOK_HOME/bin/install-istio.sh
   Using Istio Version: 1.3.6
   Downloading Istio
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
     0     0    0     0    0     0      0      0 --:--:--  0:00:01 --:--:--     0
   100  4549  100  4549    0     0   3333      0  0:00:01  0:00:01 --:--:--  3333
   
   Downloading istio-1.3.6 from https://github.com/istio/istio/releases/download/1.3.6/istio-1.3.6-linux.tar.gz ...
   Istio 1.3.6 Download Complete!
   
   Istio has been successfully downloaded into the istio-1.3.6 folder on your system.
   
   Next Steps:
   See https://istio.io/latest/docs/setup/install/ to add Istio to your Kubernetes cluster.
   
   To configure the istioctl client tool for your workstation,
   add the /tmp/istio-1.3.6/bin directory to your environment path variable with:
   	 export PATH="$PATH:/tmp/istio-1.3.6/bin"
   
   Begin the Istio pre-installation check by running:
   	 istioctl x precheck 
   
   Need more information? Visit https://istio.io/latest/docs/setup/install/ 
   Creating Istio Custom Resource Definintions(CRD)
   Unable to connect to the server: proxyconnect tcp: dial tcp: lookup socks5h on 223.5.5.5:53: no such host
   ```

2. ### Install  helm

   ```bash
   $ tar -zxvf helm-v3.7.0-linux-amd64.tar.gz
   $ sudo install -o root -g root -m 0755 linux-amd64/helm /usr/local/bin/helm
   ```

3. ### Test

   ```bash
   $ helm version
   version.BuildInfo{Version:"v3.7.0", GitCommit:"eeac83883cb4014fe60267ec6373570374ce770b", GitTreeState:"clean", GoVersion:"go1.16.8"}
   ```

## Install Istio

1. ### Download istio from [release page](https://github.com/istio/istio/releases)
   
   Go to the [Istio release](https://github.com/istio/istio/releases/tag/1.11.3) page to download the installation file for your OS, or download and extract the latest release automatically (Linux or macOS):
   
   ```bash
   $ curl -L https://istio.io/downloadIstio | sh -
   ```
   
   The command above downloads the latest release (numerically) of Istio. You can pass variables on the command line to download a specific version or to override the processor architecture. For example, to download Istio 1.6.8 for the x86_64 architecture, run:
   
   ```bash
   $ curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.6.8 TARGET_ARCH=x86_64 sh -
   ```
   
   Move to the Istio package directory. For example, if the package is `istio-1.11.3`:
   
   ```bash
   $ cd istio-1.11.3
   ```
   
   The installation directory contains:
   
   - Sample applications in `samples/`
   - The [`istioctl`](https://istio.io/latest/docs/reference/commands/istioctl) client binary in the `bin/` directory.
   
   Add the `istioctl` client to your path (Linux or macOS):
   
   ```bash
   $ export PATH=$PWD/bin:$PATH
   ```

2. ## Install istio

   ### Change directory to the root of the release package and then follow the instructions below.

   ### Create a namespace `istio-system` for Istio components:

   ```bash
   $ kubectl create namespace istio-system
   ```

   ### Install the Istio base chart which contains cluster-wide resources used by the Istio control plane:

   ```bash
   $ helm install istio-base manifests/charts/base -n istio-system
   ```

   ### Install the Istio discovery chart which deploys the `istiod` service:

   ```bash
   $ helm install istiod manifests/charts/istio-control/istio-discovery \
       -n istio-system
   ```

   ### (Optional) Install the Istio ingress gateway chart which contains the ingress gateway components:

   ```bash
   $ helm install istio-ingress manifests/charts/gateways/istio-ingress \
       -n istio-system
   ```

   ### (Optional) Install the Istio egress gateway chart which contains the egress gateway components:

   ```bash
   $ helm install istio-egress manifests/charts/gateways/istio-egress \
       -n istio-system
   ```

3. ### Test

   ```bash
   $ kubectl get pods -n istio-system
   NAME                                   READY   STATUS    RESTARTS   AGE
   istio-egressgateway-566697b759-vqxrw   1/1     Running   0          42s
   istio-ingressgateway-7776dd578-9h65p   1/1     Running   0          59s
   istiod-b9c8c9487-xzl6p                 1/1     Running   0          90s
   
   $ kubectl api-resources --api-group=networking.istio.io
   NAME               SHORTNAMES   APIVERSION                     NAMESPACED   KIND
   destinationrules   dr           networking.istio.io/v1beta1    true         DestinationRule
   envoyfilters                    networking.istio.io/v1alpha3   true         EnvoyFilter
   gateways           gw           networking.istio.io/v1beta1    true         Gateway
   serviceentries     se           networking.istio.io/v1beta1    true         ServiceEntry
   sidecars                        networking.istio.io/v1beta1    true         Sidecar
   virtualservices    vs           networking.istio.io/v1beta1    true         VirtualService
   workloadentries    we           networking.istio.io/v1beta1    true         WorkloadEntry
   workloadgroups     wg           networking.istio.io/v1alpha3   true         WorkloadGroup
   
   
   ```

   
