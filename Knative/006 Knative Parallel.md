1. ```shell
   $ vi filters-transformers.yaml
   ```

   ```yaml
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: even-filter
   spec:
     template:
       spec:
         containers:
         - image: villardl/filter-nodejs@sha256:5fb7411381abafc5bb922b7bebd677437f7f0dbc2758f3c29cba4b97a6231598
           env:
           - name: FILTER
             value: |
               Math.round(Date.parse(event.time) / 60000) % 2 === 0
   ---
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: odd-filter
   spec:
     template:
       spec:
         containers:
         - image: villardl/filter-nodejs@sha256:5fb7411381abafc5bb922b7bebd677437f7f0dbc2758f3c29cba4b97a6231598
           env:
           - name: FILTER
             value: |
               Math.round(Date.parse(event.time) / 60000) % 2 !== 0
   ---
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: even-transformer
   spec:
     template:
       spec:
         containers:
         - image: villardl/transformer-nodejs@sha256:8e099950a3b14018f1ce98e89c865eebb5c8b18da1c348e795291b358abf8241
           env:
           - name: TRANSFORMER
             value: |
               ({"message": "we are even!"})
   
   ---
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: odd-transformer
   spec:
     template:
       spec:
         containers:
         - image: villardl/transformer-nodejs@sha256:8e099950a3b14018f1ce98e89c865eebb5c8b18da1c348e795291b358abf8241
           env:
           - name: TRANSFORMER
             value: |
               ({"message": "this is odd!"})
   ```

   ```shell
   $ kubectl apply -f filters-transformers.yaml  -n kn-event-test
   $ kn service list -n kn-event-test
   NAME               URL                                                             LATEST                   AGE     CONDITIONS   READY   REASON
   even-filter        http://even-filter.kn-event-test.192.168.30.200.sslip.io        even-filter-00002        11m     3 OK / 3     True
   even-transformer   http://even-transformer.kn-event-test.192.168.30.200.sslip.io   even-transformer-00003   11m     3 OK / 3     True
   event-display      http://event-display.kn-event-test.192.168.30.200.sslip.io      event-display-00001      3d3h    3 OK / 3     True
   first              http://first.kn-event-test.192.168.30.200.sslip.io              first-00002              46m     3 OK / 3     True
   odd-filter         http://odd-filter.kn-event-test.192.168.30.200.sslip.io         odd-filter-00002         11m     3 OK / 3     True
   odd-transformer    http://odd-transformer.kn-event-test.192.168.30.200.sslip.io    odd-transformer-00003    7m29s   3 OK / 3     True
   second             http://second.kn-event-test.192.168.30.200.sslip.io             second-00002             46m     3 OK / 3     True
   third              http://third.kn-event-test.192.168.30.200.sslip.io              third-00002              46m     3 OK / 3     True
   ```

   

2. ```shell
   $ vi parallel.yaml
   ```

   ```yaml
   apiVersion: flows.knative.dev/v1
   kind: Parallel
   metadata:
     name: odd-even-parallel
   spec:
     channelTemplate:
       apiVersion: messaging.knative.dev/v1beta1
       kind: KafkaChannel
     branches:
       - filter:
           ref:
             apiVersion: serving.knative.dev/v1
             kind: Service
             name: even-filter
         subscriber:
           ref:
             apiVersion: serving.knative.dev/v1
             kind: Service
             name: even-transformer
       - filter:
           ref:
             apiVersion: serving.knative.dev/v1
             kind: Service
             name: odd-filter
         subscriber:
           ref:
             apiVersion: serving.knative.dev/v1
             kind: Service
             name: odd-transformer
     reply:
       ref:
         apiVersion: serving.knative.dev/v1
         kind: Service
         name: event-display
   ```

   ```shell
   $ kubectl apply -f parallel.yaml -n kn-event-test
   $ kubectl get parallel.flows.knative.dev -n kn-event-test
   NAME                URL                                                                               AGE   READY   REASON
   odd-even-parallel   http://odd-even-parallel-kn-parallel-kn-channel.kn-event-test.svc.cluster.local   45s   True
   ```

3. ```shell
   $ vi ping-source.yaml
   ```

   ```yaml
   apiVersion: sources.knative.dev/v1
   kind: PingSource
   metadata:
     name: ping-source
   spec:
     schedule: "*/1 * * * *"
     contentType: "application/json"
     data: '{"message": "Even or odd?"}'
     sink:
       ref:
         apiVersion: flows.knative.dev/v1
         kind: Parallel
         name: odd-even-parallel
   ```

   ```shell
   $ kubectl apply -f ping-source.yaml -n kn-event-test
   $ kn source list -n kn-event-test
   NAME           TYPE          RESOURCE                           SINK                         READY
   kafka-source   KafkaSource   kafkasources.sources.knative.dev   broker:default               True
   ping-source    PingSource    pingsources.sources.knative.dev    parallel:odd-even-parallel   True
   ```

4. ```shell
   $  kubectl logs -l serving.knative.dev/service=event-display --tail=100 -c user-container -n kn-event-test
   ☁️  cloudevents.Event
   Context Attributes,
     specversion: 1.0
     type: dev.knative.sources.ping
     source: /apis/v1/namespaces/kn-event-test/pingsources/ping-source
     id: 693ca8f8-f310-479d-9082-2967bf29d5fd
     time: 2021-11-29T10:27:00.39881944Z
     datacontenttype: application/json; charset=utf-8
   Data,
     {
       "message": "this is odd!"
     }
   ☁️  cloudevents.Event
   Context Attributes,
     specversion: 1.0
     type: dev.knative.sources.ping
     source: /apis/v1/namespaces/kn-event-test/pingsources/ping-source
     id: ef07861b-c40d-4b26-93fa-108ca183b24d
     time: 2021-11-29T10:28:00.214872366Z
     datacontenttype: application/json; charset=utf-8
   Data,
     {
       "message": "we are even!"
     }
   ```