## Install Knative Eventing[¶](https://knative.dev/docs/install/eventing/install-eventing-with-yaml/#install-knative-eventing)

To install Knative Eventing:

1. Install the required custom resource definitions (CRDs) by running the command:

   ```shell
   $ kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.0.0/eventing-crds.yaml
   ```

2. Install the core components of Eventing by running the command:

   ```
   $ kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.0.0/eventing-core.yaml
   ```

   Info

   For information about the YAML files in Knative Eventing, see [Description Tables for YAML Files](https://knative.dev/docs/install/eventing/eventing-installation-files/).

## Verify the installation[¶](https://knative.dev/docs/install/eventing/install-eventing-with-yaml/#verify-the-installation)

Success

Monitor the Knative components until all of the components show a `STATUS` of `Running` or `Completed`. You can do this by running the following command and inspecting the output:

```shell
$ kubectl get pods -n knative-eventing
```

Example output:

```shell
NAME                                   READY   STATUS    RESTARTS   AGE
eventing-controller-7995d654c7-qg895   1/1     Running   0          2m18s
eventing-webhook-fff97b47c-8hmt8       1/1     Running   0          2m17s
```

## Install a default Channel (messaging) layer

The following tabs expand to show instructions for installing a default Channel layer. Follow the procedure for the Channel of your choice:

Apache Kafka Channel

1. Install [Strimzi](https://strimzi.io/quickstarts/).

   ```shell
   $ kubectl create namespace kafka
   $ kubectl create -f 'https://strimzi.io/install/latest?namespace=kafka' -n kafka
   ```

   Follow the deployment of the Strimzi Kafka operator:

   ```shell
   $ kubectl get pod -n kafka --watch
   ```

   You can also follow the operator’s log:

   ```shell
   $ kubectl logs deployment/strimzi-cluster-operator -n kafka -f
   ```

   Then we create a new Kafka custom resource, which will give us a small persistent Apache Kafka Cluster with one node for each - Apache Zookeeper and Apache Kafka:

   ```shell
   # Apply the `Kafka` Cluster CR file
   $ kubectl apply -f https://strimzi.io/examples/latest/kafka/kafka-persistent-single.yaml -n kafka 
   
   # 如果需要改变集群的默认实例数，可以直接修改文件
   $ wget https://strimzi.io/examples/latest/kafka/kafka-persistent-single.yaml
   $ vi kafka-persistent-single.yaml
   ```

   ```yaml
   
   apiVersion: kafka.strimzi.io/v1beta2
   kind: Kafka
   metadata:
     name: my-cluster
   spec:
     kafka:
       version: 3.0.0
       replicas: 3
       listeners:
         - name: plain
           port: 9092
           type: internal
           tls: false
         - name: tls
           port: 9093
           type: internal
           tls: true
       config:
         offsets.topic.replication.factor: 1
         transaction.state.log.replication.factor: 1
         transaction.state.log.min.isr: 1
         log.message.format.version: "3.0"
         inter.broker.protocol.version: "3.0"
       storage:
         type: jbod
         volumes:
         - id: 0
           type: persistent-claim
           size: 10Gi
           deleteClaim: false
     zookeeper:
       replicas: 1
       storage:
         type: persistent-claim
         size: 10Gi
         deleteClaim: false
     entityOperator:
       topicOperator: {}
       userOperator: {}
   ```

   We now need to wait while Kubernetes starts the required pods, services and so on:

   ```shell
   $ kubectl wait kafka/my-cluster --for=condition=Ready --timeout=300s -n kafka 
   ```

   The above command might timeout if you’re downloading images over a slow connection. If that happens you can always run it again.

   Once the cluster is running, you can run a simple producer to send messages to a Kafka topic (the topic will be automatically created):

   ```shell
   $ kubectl -n kafka run kafka-producer -ti --image=quay.io/strimzi/kafka:0.26.0-kafka-3.0.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list my-cluster-kafka-bootstrap:9092 --topic my-topic
   ```

   And to receive them in a different terminal you can run:

   ```shell
   $ kubectl -n kafka run kafka-consumer -ti --image=quay.io/strimzi/kafka:0.26.0-kafka-3.0.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server my-cluster-kafka-bootstrap:9092 --topic my-topic --from-beginning
   ```

2. Install the Apache Kafka Channel for Knative from the [`knative-sandbox` repository](https://github.com/knative-sandbox/eventing-kafka).

   **Note**: Replace REPLACE_WITH_CLUSTER_URL in the yaml with the URI to the Kafka Cluster (eg. `my-cluster-kafka-bootstrap.kafka:9092/`)

   ```shell
   # Install the Kafka Source
   $ kubectl apply -f https://storage.googleapis.com/knative-nightly/eventing-kafka/latest/source.yaml
   
   # Install the Kafka "Consolidated" Channel
   # 将REPLACE_WITH_CLUSTER_URL 替换为my-cluster-kafka-bootstrap.kafka:9092/
   $ wget https://storage.googleapis.com/knative-nightly/eventing-kafka/latest/channel-consolidated.yaml
   $ vi channel-consolidated.yaml
   $ kubectl apply -f channel-consolidated.yaml
   
   # Install the Kafka "Distributed" Channel
   # 将REPLACE_WITH_CLUSTER_URL 替换为my-cluster-kafka-bootstrap.kafka:9092/
   $ wget https://storage.googleapis.com/knative-nightly/eventing-kafka/latest/channel-distributed.yaml
   $ vi channel-distributed.yaml
   $ kubectl apply -f channel-distributed.yaml
   ```

## [Apache Kafka Broker](https://knative.dev/docs/eventing/broker/kafka-broker/)[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#apache-kafka-broker)

### Prerequisites[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#prerequisites)

1. [Installing Eventing using YAML files](https://knative.dev/docs/install/eventing/install-eventing-with-yaml/).
2. An Apache Kafka cluster (if you're just getting started you can follow [Strimzi Quickstart page](https://strimzi.io/quickstarts/)).

### Installation[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#installation)

1. Install the Kafka controller by entering the following command:

   ```shell
   $ kubectl apply --filename https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-v1.0.0/eventing-kafka-controller.yaml
   ```

2. Install the Kafka Broker data plane by entering the following command:

   ```shell
   $ kubectl apply --filename https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-v1.0.0/eventing-kafka-broker.yaml
   ```

3. Verify that `kafka-controller`, `kafka-broker-receiver` and `kafka-broker-dispatcher` are running, by entering the following command:

   ```shell
   $ kubectl get deployments.apps -n knative-eventing
   ```

   Example output:

   ```shell
   
   NAME                                READY   UP-TO-DATE   AVAILABLE   AGE
   eventing-controller                 1/1     1            1           46m
   eventing-kafka-channel-controller   1/1     1            1           7m36s
   eventing-webhook                    1/1     1            1           46m
   kafka-broker-dispatcher             1/1     1            1           3m32s
   kafka-broker-receiver               1/1     1            1           3m32s
   kafka-ch-controller                 1/1     1            1           7m41s
   kafka-controller                    1/1     1            1           3m40s
   kafka-controller-manager            1/1     1            1           9m11s
   kafka-webhook                       1/1     1            1           7m41s
   kafka-webhook-eventing              1/1     1            1           3m40s
   pingsource-mt-adapter               0/0     0            0           46m
   ```

4. 检查默认设置

   ```shell
   $ kubectl describe configmap kafka-broker-config -n knative-eventing
   Name:         kafka-broker-config
   Namespace:    knative-eventing
   Labels:       kafka.eventing.knative.dev/release=devel
   Annotations:  <none>
   
   Data
   ====
   bootstrap.servers:
   ----
   my-cluster-kafka-bootstrap.kafka:9092 # A comma separated list of bootstrap servers. (It can be in or out the k8s cluster)
   default.topic.partitions:
   ----
   10 # Number of topic partitions
   default.topic.replication.factor:
   ----
   3 # Replication factor of topic messages.
   
   BinaryData
   ====
   
   Events:  <none>
   ```

   This `ConfigMap` is installed in the cluster. You can edit the configuration or create a new one with the same values depending on your needs.

   

   **NOTE:** The `default.topic.replication.factor` value must be less than or equal to the number of Kafka broker instances in your cluster. For example, if you only have one Kafka broker, the `default.topic.replication.factor` value should not be more than `1`.

   

5. Create a Kafka Broker[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#create-a-kafka-broker)

   A Kafka Broker object looks like this:

   ```yaml
   apiVersion: eventing.knative.dev/v1
   kind: Broker
   metadata:
     annotations:
       # case-sensitive
       eventing.knative.dev/broker.class: Kafka
     name: default
     namespace: default
   spec:
     # Configuration specific to this broker.
     config:
       apiVersion: v1
       kind: ConfigMap
       name: kafka-broker-config
       namespace: knative-eventing
     # Optional dead letter sink, you can specify either:
     #  - deadLetterSink.ref, which is a reference to a Callable
     #  - deadLetterSink.uri, which is an absolute URI to a Callable (It can potentially be out of the Kubernetes cluster)
     delivery:
       deadLetterSink:
         ref:
           apiVersion: serving.knative.dev/v1
           kind: Service
           name: dlq-service
   ```

   ```shell
   $ vi knative-default-broker.yaml
   $  kubectl apply -f knative-default-broker.yaml
   broker.eventing.knative.dev/default created
   # 可以使用 kubectl get broker default -n default
   $ kn broker list
   NAME      URL   AGE   CONDITIONS   READY     REASON
   default         58s   3 OK / 6     Unknown                   # 可以看到broker不正常
   $ kubectl edit broker default
   # 删除如下部分，因为并没有部署死信服务
   delivery:
       deadLetterSink:
         ref:
           apiVersion: serving.knative.dev/v1
           kind: Service
           name: dlq-service
   $ kubectl get broker default -n default
   NAME      URL                                                                              AGE   READY   REASON
   default   http://kafka-broker-ingress.knative-eventing.svc.cluster.local/default/default   12m   True
   ```

6. Set as default broker implementation

   To set the Kafka broker as the default implementation for all brokers in the Knative deployment, you can apply global settings by modifying the `config-br-defaults` ConfigMap in the `knative-eventing` namespace.

   This allows you to avoid configuring individual or per-namespace settings for each broker, such as `metadata.annotations.eventing.knative.dev/broker.class` or `spec.config`.

   The following YAML is an example of a `config-br-defaults` ConfigMap using Kafka broker as the default implementation.

   ```yaml
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: config-br-defaults
     namespace: knative-eventing
   data:
     default-br-config: |
       clusterDefault:
         brokerClass: Kafka
         apiVersion: v1
         kind: ConfigMap
         name: kafka-broker-config
         namespace: knative-eventing
       namespaceDefaults:
         namespace1:
           brokerClass: Kafka
           apiVersion: v1
           kind: ConfigMap
           name: kafka-broker-config
           namespace: knative-eventing
         namespace2:
           brokerClass: Kafka
           apiVersion: v1
           kind: ConfigMap
           name: kafka-broker-config
           namespace: knative-eventing
   ```

   ```shell
   $ kubectl edit configmap config-br-defaults -n knative-eventing
   # Please edit the object below. Lines beginning with a '#' will be ignored,
   # and an empty file will abort the edit. If an error occurs while saving this file will be
   # reopened with the relevant failures.
   #
   apiVersion: v1
   data:
     default-br-config: |
       clusterDefault:
         brokerClass: Kafka # 切换Kafka
         apiVersion: v1
         kind: ConfigMap
         name: kafka-broker-config # 上文中的ConfigMap
         namespace: knative-eventing
   kind: ConfigMap
   metadata:
     annotations:
       kubectl.kubernetes.io/last-applied-configuration: |
         {"apiVersion":"v1","data":{"default-br-config":"clusterDefault:\n  brokerClass: MTChannelBasedBroker\n  apiVersion: v1\n  kind: ConfigMap\n  name: config-br-default-channel\n  namespace: knative-eventing\n"},"kind":"ConfigMap","metadata":{"annotations":{},"labels":{"app.kubernetes.io/part-of":"knative-eventing","app.kubernetes.io/version":"1.0.0","eventing.knative.dev/release":"v1.0.0"},"name":"config-br-defaults","namespace":"knative-eventing"}}
     creationTimestamp: "2021-11-25T06:57:24Z"
     labels:
       app.kubernetes.io/part-of: knative-eventing
       app.kubernetes.io/version: 1.0.0
       eventing.knative.dev/release: v1.0.0
     name: config-br-defaults
     namespace: knative-eventing
     resourceVersion: "2115808"
     uid: 4c5eee7b-daa3-4031-9841-3f3b267ad96b
     
     
   $ kn broker create test-default
   Broker 'test-default' successfully created in namespace 'default'.
   $ kn broker list
   # 可以看到新建的broker 也是默认使用了Kafka
   NAME           URL                                                                                   AGE   CONDITIONS   READY   REASON
   default        http://kafka-broker-ingress.knative-eventing.svc.cluster.local/default/default        21m   6 OK / 6     True
   test-default   http://kafka-broker-ingress.knative-eventing.svc.cluster.local/default/test-default   13s   6 OK / 6     True
   
   ```

7. Kafka Producer and Consumer configurations[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#kafka-producer-and-consumer-configurations)

   Knative exposes all available Kafka producer and consumer configurations that can be modified to suit your workloads.

   You can change these configurations by modifying the `config-kafka-broker-data-plane` `ConfigMap` in the `knative-eventing` namespace.

   Documentation for the settings available in this `ConfigMap` is available on the [Apache Kafka website](https://kafka.apache.org/documentation/), in particular, [Producer configurations](https://kafka.apache.org/documentation/#producerconfigs) and [Consumer configurations](https://kafka.apache.org/documentation/#consumerconfigs).

   Enable debug logging for data plane components[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#enable-debug-logging-for-data-plane-components)

   The following YAML shows the default logging configuration for data plane components, that is created during the installation step:

   ```yaml
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: kafka-config-logging
     namespace: knative-eventing
   data:
     config.xml: |
       <configuration>
         <appender name="jsonConsoleAppender" class="ch.qos.logback.core.ConsoleAppender">
           <encoder class="net.logstash.logback.encoder.LogstashEncoder"/>
         </appender>
         <root level="INFO">
           <appender-ref ref="jsonConsoleAppender"/>
         </root>
       </configuration>
   ```

   To change the logging level to `DEBUG`, you must:

   - Apply the following `kafka-config-logging` `ConfigMap` or replace `level="INFO"` with `level="DEBUG"` to the `ConfigMap` `kafka-config-logging`:

     ```yaml
     apiVersion: v1
     kind: ConfigMap
     metadata:
       name: kafka-config-logging
       namespace: knative-eventing
     data:
       config.xml: |
         <configuration>
           <appender name="jsonConsoleAppender" class="ch.qos.logback.core.ConsoleAppender">
             <encoder class="net.logstash.logback.encoder.LogstashEncoder"/>
           </appender>
           <root level="DEBUG">
             <appender-ref ref="jsonConsoleAppender"/>
           </root>
         </configuration>
     ```

   - Restart the `kafka-broker-receiver` and the `kafka-broker-dispatcher`, by entering the following commands:

     ```shell
     $ kubectl rollout restart deployment -n knative-eventing kafka-broker-receiver
     $ kubectl rollout restart deployment -n knative-eventing kafka-broker-dispatcher
     ```

8. Configuring the order of delivered events[¶](https://knative.dev/docs/eventing/broker/kafka-broker/#configuring-the-order-of-delivered-events)

   When dispatching events, the Kafka broker can be configured to support different delivery ordering guarantees.

   You can configure the delivery order of events using the `kafka.eventing.knative.dev/delivery.order` annotation on the `Trigger` object:

   ```yaml
   apiVersion: eventing.knative.dev/v1
   kind: Trigger
   metadata:
     name: my-service-trigger
     annotations:
        kafka.eventing.knative.dev/delivery.order: ordered
   spec:
     broker: my-kafka-broker
     subscriber:
       ref:
         apiVersion: serving.knative.dev/v1
         kind: Service
         name: my-service
   ```

   The supported consumer delivery guarantees are:

   - `unordered`: An unordered consumer is a non-blocking consumer that delivers messages unordered, while preserving proper offset management.
   - `ordered`: An ordered consumer is a per-partition blocking consumer that waits for a successful response from the CloudEvent subscriber before it delivers the next message of the partition.

   `unordered` is the default ordering guarantee, while **`ordered` is considered unstable, use with caution**.



## Apache Kafka Sink

### Prerequisites[¶](https://knative.dev/docs/eventing/sinks/kafka-sink/#prerequisites)

You must have access to a Kubernetes cluster with [Knative Eventing installed](https://knative.dev/docs/install/eventing/install-eventing-with-yaml/).

### Installation[¶](https://knative.dev/docs/eventing/sinks/kafka-sink/#installation)

1. Install the Kafka controller:

   ```shell
   $ kubectl apply -f https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-v1.0.0/eventing-kafka-controller.yaml
   ```

2. Install the KafkaSink data plane:

   ```shell
   $ kubectl apply -f https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-v1.0.0/eventing-kafka-sink.yaml
   ```

3. Verify that `kafka-controller` and `kafka-sink-receiver` Deployments are running:

   ```shell
   $ kubectl get deployments.apps -n knative-eventing
   ```

   Example output:

   ```shell
   
   NAME                                READY   UP-TO-DATE   AVAILABLE   AGE
   eventing-controller                 1/1     1            1           96m
   eventing-kafka-channel-controller   1/1     1            1           57m
   eventing-webhook                    1/1     1            1           96m
   kafka-broker-dispatcher             1/1     1            1           53m
   kafka-broker-receiver               1/1     1            1           53m
   kafka-ch-controller                 1/1     1            1           57m
   kafka-controller                    1/1     1            1           53m
   kafka-controller-manager            1/1     1            1           58m
   kafka-sink-receiver                 1/1     1            1           22s
   kafka-webhook                       1/1     1            1           57m
   kafka-webhook-eventing              1/1     1            1           53m
   pingsource-mt-adapter               0/0     0            0           96m
   ```

4. A KafkaSink object looks similar to the following:

   ```yaml
   apiVersion: eventing.knative.dev/v1alpha1
   kind: KafkaSink
   metadata:
     name: my-kafka-sink
     namespace: default
   spec:
     topic: knative-demo-topic
     bootstrapServers:
      - my-cluster-kafka-bootstrap.kafka:9092
   ```

   ```shell
   $ vi my-kafka-sink.yaml
   $ kubectl apply -f my-kafka-sink.yaml
   kafkasink.eventing.knative.dev/my-kafka-sink created
   ```

5. Output Topic Content Mode[¶](https://knative.dev/docs/eventing/sinks/kafka-sink/#output-topic-content-mode)

   The CloudEvent specification defines 2 modes to transport a CloudEvent: structured and binary.

   > A "structured-mode message" is one where the event is fully encoded using a stand-alone event format and stored in the message body.
   >
   > The structured content mode keeps event metadata and data together in the payload, allowing simple forwarding of the same event across multiple routing hops, and across multiple protocols.
   >
   > A "binary-mode message" is one where the event data is stored in the message body, and event attributes are stored as part of message meta-data.
   >
   > The binary content mode accommodates any shape of event data, and allows for efficient transfer and without transcoding effort.

   A KafkaSink object with a specified `contentMode` looks similar to the following:

   ```yaml
   apiVersion: eventing.knative.dev/v1alpha1
   kind: KafkaSink
   metadata:
     name: my-kafka-sink
     namespace: default
   spec:
     topic: mytopic
     bootstrapServers:
      - my-cluster-kafka-bootstrap.kafka:9092
   
     # CloudEvent content mode of Kafka messages sent to the topic.
     # Possible values:
     # - structured
     # - binary
     #
     # default: structured.
     #
     # CloudEvent spec references:
     # - https://github.com/cloudevents/spec/blob/v1.0/spec.md#message
     # - https://github.com/cloudevents/spec/blob/v1.0/kafka-protocol-binding.md#33-structured-content-mode
     # - https://github.com/cloudevents/spec/blob/v1.0/kafka-protocol-binding.md#32-binary-content-mode
     contentMode: binary # or structured
   ```

6. Kafka Producer configurations[¶](https://knative.dev/docs/eventing/sinks/kafka-sink/#kafka-producer-configurations)

   A Kafka Producer is the component responsible for sending events to the Apache Kafka cluster. You can change the configuration for Kafka Producers in your cluster by modifying the `config-kafka-sink-data-plane` ConfigMap in the `knative-eventing` namespace.

   Documentation for the settings available in this ConfigMap is available on the [Apache Kafka website](https://kafka.apache.org/documentation/), in particular, [Producer configurations](https://kafka.apache.org/documentation/#producerconfigs).

   

7. Enable debug logging for data plane components[¶](https://knative.dev/docs/eventing/sinks/kafka-sink/#enable-debug-logging-for-data-plane-components)

   To enable debug logging for data plane components change the logging level to `DEBUG` in the `kafka-config-logging` ConfigMap.

   1. Create the `kafka-config-logging` ConfigMap as a YAML file that contains the following:

      ```shell
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: kafka-config-logging
        namespace: knative-eventing
      data:
        config.xml: |
          <configuration>
            <appender name="jsonConsoleAppender" class="ch.qos.logback.core.ConsoleAppender">
              <encoder class="net.logstash.logback.encoder.LogstashEncoder"/>
            </appender>
            <root level="DEBUG">
              <appender-ref ref="jsonConsoleAppender"/>
            </root>
          </configuration>
      ```

   2. Apply the YAML file by running the command:

      ```shell
      $ kubectl apply -f <filename>.yaml
      ```

   3. Restart the `kafka-sink-receiver`:

      ```shell
      $ kubectl rollout restart deployment -n knative-eventing kafka-sink-receiver
      ```

## **Apache Kafka Source**

install the Apache Kafka Source by running the command:

```shell
$ kubectl apply -f https://github.com/knative-sandbox/eventing-kafka/releases/download/knative-v1.0.0/source.yaml
```

1. Create a knative-demo-topic.yaml file:

   ```yaml
   apiVersion: kafka.strimzi.io/v1beta2
   kind: KafkaTopic
   metadata:
     name: knative-demo-topic
     namespace: kafka
     labels:
       strimzi.io/cluster: my-cluster
   spec:
     partitions: 3
     replicas: 1
     config:
       retention.ms: 7200000
       segment.bytes: 1073741824
   ```

2. Deploy the `KafkaTopic` YAML file by running the command:

   

   ```shell
   $ kubectl apply -f knative-demo-topic.yaml
   ```

   Example output:

   ```
   kafkatopic.kafka.strimzi.io/knative-demo-topic created
   ```

3. Ensure that the `KafkaTopic` is running by running the command:

   ```shell
   $ kubectl -n kafka get kafkatopic.kafka.strimzi.io
   ```

   Example output:

   ```shell
   NAME                 AGE
   knative-demo-topic   16s
   ```

4. Create a Service[¶](https://knative.dev/docs/eventing/sources/kafka-source/#create-a-service)

   1. Create the `event-display.yaml` Service as a YAML file:

      ```yaml
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: event-display
        namespace: default
      spec:
        replicas: 1
        selector:
          matchLabels: &labels
            app: event-display
        template:
          metadata:
            labels: *labels
          spec:
            containers:
              - name: event-display
                image: gcr.io/knative-releases/knative.dev/eventing/cmd/event_display@sha256:1d6083a88816f6b4ed924c2640683ca6b0b994ec18e9935a3368779ec37dacf6
      
      ---
      
      kind: Service
      apiVersion: v1
      metadata:
        name: event-display
        namespace: default
      spec:
        selector:
          app: event-display
        ports:
        - protocol: TCP
          port: 80
          targetPort: 8080
      ```

   2. Apply the YAML file by running the command:

      

      ```shell
      $ kubectl apply -f event-display.yaml
      ```

   3. Ensure that the Service Pod is running, by running the command:

      ```shell
      $ kubectl get service
      NAME            TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
   event-display   ClusterIP   10.1.15.180   <none>        80/TCP    69m
      ```

5. Kafka event source[¶](https://knative.dev/docs/eventing/sources/kafka-source/#kafka-event-source)

   Create`event-source.yaml` accordingly with bootstrap servers, topics, and so on:

   ```yaml
   apiVersion: sources.knative.dev/v1beta1
   kind: KafkaSource
   metadata:
     name: kafka-source
     namespace: default
   spec:
     consumerGroup: knative-group
     bootstrapServers:
     - my-cluster-kafka-bootstrap.kafka:9092 # note the kafka namespace
     topics:
     - knative-demo-topic
     sink:
       ref:
         apiVersion: v1
         kind: Service
         name: event-display
   ```

   Deploy the event source:

   ```
   kubectl apply -f event-source.yaml
   ```

   Verify that the event source Pod is running:

   ```shell
   $ kubectl get kafkasource.sources.knative.dev kafka-source -n default
   NAME           TOPICS                   BOOTSTRAPSERVERS                            READY   REASON   AGE
   kafka-source   ["knative-demo-topic"]   ["my-cluster-kafka-bootstrap.kafka:9092"]   True             12m
   ```

6. Verify[¶](https://knative.dev/docs/eventing/sources/kafka-source/#verify)

   1. Produce a message (`{"msg": "This is a Kafka source test!"}`) to the Apache Kafka topic as in the following example:

      ```shell
      $ kubectl -n kafka run kafka-producer -ti --image=quay.io/strimzi/kafka:0.26.0-kafka-3.0.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list my-cluster-kafka-bootstrap:9092 --topic knative-demo-topic
      >{"msg": "This is a Kafka source test!"}
      ^C
      
      $ kubectl -n default logs -l app=event-display --tail=100
      ☁️  cloudevents.Event
      Context Attributes,
        specversion: 1.0
        type: dev.knative.kafka.event
        source: /apis/v1/namespaces/default/kafkasources/kafka-source#knative-demo-topic
        subject: partition:2#0
        id: partition:2/offset:0
        time: 2021-11-26T04:30:35.707Z
      Data,
        {"msg": "This is a Kafka source test!"}
      
      ```
   
   
   
   