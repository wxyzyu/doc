## 测试Knative Sequence

1. ```shell
   $ vi steps.yaml
   ```

   ```yaml
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: first
     namespace: kn-event-test
   spec:
     template:
       spec:
         containers:
           - image: gcr.io/knative-releases/knative.dev/eventing/cmd/appender@sha256:3a14ae8985ea2a278bcd148528d92111775440cbe4d78bbef4f5aa12e277ce9e
             env:
               - name: MESSAGE
                 value: " - Handled by 0"
   
   ---
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: second
     namespace: kn-event-test
   spec:
     template:
       spec:
         containers:
           - image: gcr.io/knative-releases/knative.dev/eventing/cmd/appender@sha256:3a14ae8985ea2a278bcd148528d92111775440cbe4d78bbef4f5aa12e277ce9e
             env:
               - name: MESSAGE
                 value: " - Handled by 1"
   ---
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: third
     namespace: kn-event-test
   spec:
     template:
       spec:
         containers:
           - image: gcr.io/knative-releases/knative.dev/eventing/cmd/appender@sha256:3a14ae8985ea2a278bcd148528d92111775440cbe4d78bbef4f5aa12e277ce9e
             env:
               - name: MESSAGE
                 value: " - Handled by 2"
   ---
   ```

   ```shell
   $ kubectl apply -f steps.yaml
   ```

2. ```shell
   $ vi sequence.yaml
   ```

   ```yaml
   apiVersion: flows.knative.dev/v1
   kind: Sequence
   metadata:
     name: sequence
     namespace: kn-event-test
   spec:
     channelTemplate:
       apiVersion: messaging.knative.dev/v1beta1
       kind: KafkaChannel
     steps:
       - ref:
           apiVersion: serving.knative.dev/v1
           kind: Service
           name: first
       - ref:
           apiVersion: serving.knative.dev/v1
           kind: Service
           name: second
       - ref:
           apiVersion: serving.knative.dev/v1
           kind: Service
           name: third
     reply:
       ref:
         kind: Service
         apiVersion: serving.knative.dev/v1
         name: event-display
         namespace: kn-event-test
   ```

   ```shell
   $  kubectl get sequence.flows.knative.dev  -n kn-event-test
   NAME       URL                                                                        AGE     READY   REASON
   sequence   http://sequence-kn-sequence-0-kn-channel.kn-event-test.svc.cluster.local   2m52s   True
   ```

   

3. ```shell
   $ vi ping-source.yaml
   ```

   ```yaml
   apiVersion: sources.knative.dev/v1
   kind: PingSource
   metadata:
     name: ping-source
     namespace: kn-event-test
   spec:
     schedule: "*/1 * * * *"
     contentType: "application/json"
     data: '{"message": "Hello world!"}'
     sink:
       ref:
         apiVersion: flows.knative.dev/v1
         kind: Sequence
         name: sequence
         namespace: kn-event-test
   ```

   ```shell
   $ kubectl apply -f ping-source.yaml
   kn source list -n kn-event-test
   NAME           TYPE          RESOURCE                           SINK                READY
   kafka-source   KafkaSource   kafkasources.sources.knative.dev   broker:default      True
   ping-source    PingSource    pingsources.sources.knative.dev    sequence:sequence   True
   ```

4. ```shell
   $ kubectl -n kn-event-test logs -l serving.knative.dev/service=event-display -c user-container --tail=-1
   ☁️  cloudevents.Event
   Context Attributes,
     specversion: 1.0
     type: dev.knative.sources.ping
     source: /apis/v1/namespaces/kn-event-test/pingsources/ping-source
     id: 61c012f4-5a89-409a-bfce-53f7b39c040c
     time: 2021-11-29T09:48:00.479273102Z
     datacontenttype: application/json
   Data,
     {
       "id": 0,
       "message": "Hello world! - Handled by 0 - Handled by 1 - Handled by 2"
     }
   ```