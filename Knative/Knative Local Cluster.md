## Install kind

1. ### Download from [binary release](https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64)

   ```bash
   shell> wget https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
   ```

2. ### Install kind

   ```bash
   shell> sudo install -o root -g root -m 0755 kind-linux-amd64 /usr/local/bin/kind
   ```

3. ### Test

   ```bash
   shell> kind --version
   kind version 0.11.1
   ```

   

## Install kn

1. ### Download from [release page](https://github.com/knative/client/releases)

   ```bash
   shell> wget https://github.com/knative/client/releases/download/v0.26.0/kn-linux-amd64
   ```

2. ### Install kind

   ```bash
   shell> sudo install -o root -g root -m 0755 kn-linux-amd64 /usr/local/bin/kn
   ```

3. ### Test

   ```bash
   shell> kn version
   Version:      v0.26.0
   Build Date:   2021-09-22 17:08:53
   Git Revision: 61b8a754
   Supported APIs:
   * Serving
     - serving.knative.dev/v1 (knative-serving v0.26.0)
   * Eventing
     - sources.knative.dev/v1 (knative-eventing v0.26.0)
     - eventing.knative.dev/v1 (knative-eventing v0.26.0)
   ```

   

## Install kn-plugin-quickstart

1. ### Download from [release page](https://github.com/knative-sandbox/kn-plugin-quickstart/releases)

   ```bash
   shell> wget https://github.com/knative-sandbox/kn-plugin-quickstart/releases/download/v0.4.1/kn-quickstart-linux-amd64
   ```

2. ### Install kind

   ```bash
   shell> sudo install -o root -g root -m 0755 kn-quickstart-linux-amd64 /usr/local/bin/kn-quickstart
   ```

3. ### Test

   ```bash
   shell> kn plugin list
   - kn-quickstart : /usr/local/bin/kn-quickstart
   ```

   

## Install local cluster

1. ### Install kind

   ```bash
   shell> kind create cluster
   Creating cluster "kind" ...
    ✓ Ensuring node image (kindest/node:v1.21.1) 🖼 
    ✓ Preparing nodes 📦  
    ✓ Writing configuration 📜 
    ✓ Starting control-plane 🕹️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️️ 
    ✓ Installing CNI 🔌 
    ✓ Installing StorageClass 💾 
   Set kubectl context to "kind-kind"
   You can now use your cluster with:
   
   kubectl cluster-info --context kind-kind
   
   Not sure what to do next? 😅  Check out https://kind.sigs.k8s.io/docs/user/quick-start/
   ```

2. ### Test

   ```bash
   shell> kubectl cluster-info --context kind-kind
   Kubernetes control plane is running at https://127.0.0.1:46253
   CoreDNS is running at https://127.0.0.1:46253/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
   
   To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
   ```

   
