## 使用Kafka Channel

1. 安装参考002 Install Knative Eventing

2. 设为默认Channel实现

   ```shell
   $ kubectl edit configmap default-ch-webhook -n knative-eventing
   ```

   ```yaml
   # Please edit the object below. Lines beginning with a '#' will be ignored,
   # and an empty file will abort the edit. If an error occurs while saving this file will be
   # reopened with the relevant failures.
   #
   apiVersion: v1
   data:
     default-ch-config: |
       clusterDefault:
         apiVersion: messaging.knative.dev/v1beta1 # 集群默认channel实现
         kind: KafkaChannel
       namespaceDefaults:
         some-namespace:
           apiVersion: messaging.knative.dev/v1beta1 # namespace默认channel实现
           kind: KafkaChannel
   kind: ConfigMap
   metadata:
     annotations:
       kubectl.kubernetes.io/last-applied-configuration: |
         {"apiVersion":"v1","data":{"default-ch-config":"clusterDefault:\n  apiVersion: messaging.knative.dev/v1\n  kind: InMemoryChannel\nnamespaceDefaults:\n  some-namespace:\n    apiVersion: messaging.knative.dev/v1\n    kind: InMemoryChannel\n"},"kind":"ConfigMap","metadata":{"annotations":{},"labels":{"app.kubernetes.io/part-of":"knative-eventing","app.kubernetes.io/version":"1.0.0","eventing.knative.dev/release":"v1.0.0"},"name":"default-ch-webhook","namespace":"knative-eventing"}}
     creationTimestamp: "2021-11-25T06:57:24Z"
     labels:
       app.kubernetes.io/part-of: knative-eventing
       app.kubernetes.io/version: 1.0.0
       eventing.knative.dev/release: v1.0.0
     name: default-ch-webhook
     namespace: knative-eventing
     resourceVersion: "13415139"
     uid: a68d047d-7218-4452-9f2a-7b839108a4e5
   
   ```

3. 创建测试channel

   ```shell
   $ vi kafka-channel.yaml
   ```

   ```yaml
   apiVersion: messaging.knative.dev/v1
   kind: Channel
   metadata:
     name: kafka-channel
     namespace: kn-event-test
   ```

   ```shell
   $ kubectl get channel.messaging.knative.dev kafka-channel -n kn-event-test
   NAME            URL   AGE     READY   REASON
   kafka-channel         3m39s   False   EmptyHostname
   $ kubectl describe  channel.messaging.knative.dev kafka-channel -n kn-event-test
   Name:         kafka-channel
   Namespace:    kn-event-test
   Labels:       <none>
   Annotations:  messaging.knative.dev/creator: kubernetes-admin
                 messaging.knative.dev/lastModifier: kubernetes-admin
                 messaging.knative.dev/subscribable: v1
   API Version:  messaging.knative.dev/v1
   Kind:         Channel
   Metadata:
     Creation Timestamp:  2021-11-29T08:17:47Z
     Generation:          1
     Managed Fields:
       API Version:  messaging.knative.dev/v1
       Fields Type:  FieldsV1
       fieldsV1:
         f:metadata:
           f:annotations:
             .:
             f:kubectl.kubernetes.io/last-applied-configuration:
       Manager:      kubectl-client-side-apply
       Operation:    Update
       Time:         2021-11-29T08:17:47Z
       API Version:  messaging.knative.dev/v1
       Fields Type:  FieldsV1
       fieldsV1:
         f:status:
           f:conditions:
       Manager:         controller
       Operation:       Update
       Time:            2021-11-29T08:17:48Z
     Resource Version:  13427524
     UID:               178da22e-804c-406d-b769-48d8254302ed
   Spec:
     Channel Template:
       API Version:  messaging.knative.dev/v1beta1
       Kind:         KafkaChannel
   Status:
     Channel:
       API Version:  messaging.knative.dev/v1beta1
       Kind:         KafkaChannel
       Name:         kafka-channel
       Namespace:    kn-event-test
     Conditions:
       Last Transition Time:  2021-11-29T08:17:43Z
       Message:               hostname is the empty string
       Reason:                EmptyHostname
       Status:                False
       Type:                  Addressable
       Last Transition Time:  2021-11-29T08:17:43Z
       Message:               Unable to build Kafka client for channel kafka-channel: kafka: invalid configuration (Net.SASL.User must not be empty when SASL is enabled)
       Reason:                InvalidConfiguration
       Status:                False
       Type:                  BackingChannelReady
       Last Transition Time:  2021-11-29T08:17:43Z
       Message:               No dead letter sink is configured.
       Reason:                DeadLetterSinkNotConfigured
       Status:                True
       Type:                  DeadLetterSinkResolved
       Last Transition Time:  2021-11-29T08:17:43Z
       Message:               hostname is the empty string
       Reason:                EmptyHostname
       Status:                False
       Type:                  Ready
     Observed Generation:     1
   Events:                    <none>
   
   
   # 测试Kafka集群没有配置sasl，禁用channel的配置,TLS和SASL设为false
   $ kubectl edit configmap config-kafka -n knative-eventing
   ```

   ```yaml
   # Please edit the object below. Lines beginning with a '#' will be ignored,
   # and an empty file will abort the edit. If an error occurs while saving this file will be
   # reopened with the relevant failures.
   #
   apiVersion: v1
   data:
     eventing-kafka: |
       cloudevents:
         maxIdleConns: 1000
         maxIdleConnsPerHost: 100
       kafka:
         authSecretName: kafka-cluster
         authSecretNamespace: knative-eventing
         brokers: my-cluster-kafka-bootstrap.kafka:9092
       channel:
         adminType: kafka # One of "kafka", "azure", "custom"
         dispatcher:
           cpuRequest: 100m
           memoryRequest: 50Mi
         receiver:
           cpuRequest: 100m
           memoryRequest: 50Mi
     sarama: |
       enableLogging: false
       config: |
         Version: 2.0.0 # Kafka Version Compatibility From Sarama's Supported List (Major.Minor.Patch)
         Admin:
           Timeout: 10000000000  # 10 seconds
         Net:
           KeepAlive: 30000000000  # 30 seconds
           MaxOpenRequests: 1 # Set to 1 for use with Idempotent Producer
           TLS:
             Enable: false
           SASL:
             Enable: false
             Version: 1
         Metadata:
           RefreshFrequency: 300000000000  # 5 minutes
         Consumer:
           Offsets:
             AutoCommit:
               Interval: 5000000000  # 5 seconds
             Retention: 604800000000000  # 1 week
         Producer:
           Idempotent: true  # Must be false for Azure EventHubs
           RequiredAcks: -1  # -1 = WaitForAll, Most stringent option for "at-least-once" delivery.
     version: 1.0.0
   kind: ConfigMap
   metadata:
     annotations:
       kubectl.kubernetes.io/last-applied-configuration: |
         {"apiVersion":"v1","data":{"eventing-kafka":"cloudevents:\n  maxIdleConns: 1000\n  maxIdleConnsPerHost: 100\nkafka:\n  authSecretName: kafka-cluster\n  authSecretNamespace: knative-eventing\n  brokers: my-cluster-kafka-bootstrap.kafka:9092/\nchannel:\n  adminType: kafka # One of \"kafka\", \"azure\", \"custom\"\n  dispatcher:\n    cpuRequest: 100m\n    memoryRequest: 50Mi\n  receiver:\n    cpuRequest: 100m\n    memoryRequest: 50Mi\n","sarama":"enableLogging: false\nconfig: |\n  Version: 2.0.0 # Kafka Version Compatibility From Sarama's Supported List (Major.Minor.Patch)\n  Admin:\n    Timeout: 10000000000  # 10 seconds\n  Net:\n    KeepAlive: 30000000000  # 30 seconds\n    MaxOpenRequests: 1 # Set to 1 for use with Idempotent Producer\n    TLS:\n      Enable: true\n    SASL:\n      Enable: true\n      Version: 1\n  Metadata:\n    RefreshFrequency: 300000000000  # 5 minutes\n  Consumer:\n    Offsets:\n      AutoCommit:\n        Interval: 5000000000  # 5 seconds\n      Retention: 604800000000000  # 1 week\n  Producer:\n    Idempotent: true  # Must be false for Azure EventHubs\n    RequiredAcks: -1  # -1 = WaitForAll, Most stringent option for \"at-least-once\" delivery.\n","version":"1.0.0"},"kind":"ConfigMap","metadata":{"annotations":{},"name":"config-kafka","namespace":"knative-eventing"}}
     creationTimestamp: "2021-11-25T07:36:10Z"
     name: config-kafka
     namespace: knative-eventing
     resourceVersion: "13476398"
     uid: abf447c2-065b-408f-965f-86a021c9f5db
   
   ```

   ```shell
   $ kubectl describe  channel.messaging.knative.dev kafka-channel -n kn-event-test
   Name:         kafka-channel
   Namespace:    kn-event-test
   Labels:       <none>
   Annotations:  messaging.knative.dev/creator: kubernetes-admin
                 messaging.knative.dev/lastModifier: kubernetes-admin
                 messaging.knative.dev/subscribable: v1
   API Version:  messaging.knative.dev/v1
   Kind:         Channel
   Metadata:
     Creation Timestamp:  2021-11-29T08:17:47Z
     Generation:          1
     Managed Fields:
       API Version:  messaging.knative.dev/v1
       Fields Type:  FieldsV1
       fieldsV1:
         f:metadata:
           f:annotations:
             .:
             f:kubectl.kubernetes.io/last-applied-configuration:
       Manager:      kubectl-client-side-apply
       Operation:    Update
       Time:         2021-11-29T08:17:47Z
       API Version:  messaging.knative.dev/v1
       Fields Type:  FieldsV1
       fieldsV1:
         f:status:
           f:conditions:
       Manager:         controller
       Operation:       Update
       Time:            2021-11-29T08:43:10Z
     Resource Version:  13477082
     UID:               178da22e-804c-406d-b769-48d8254302ed
   Spec:
     Channel Template:
       API Version:  messaging.knative.dev/v1beta1
       Kind:         KafkaChannel
   Status:
     Address:
       URL:  http://kafka-channel-kn-channel.kn-event-test.svc.cluster.local
     Channel:
       API Version:  messaging.knative.dev/v1beta1
       Kind:         KafkaChannel
       Name:         kafka-channel
       Namespace:    kn-event-test
     Conditions:
       Last Transition Time:  2021-11-29T08:42:54Z
       Status:                True
       Type:                  Addressable
       Last Transition Time:  2021-11-29T08:43:06Z
       Message:               The status of Dispatcher Deployment is False: MinimumReplicasUnavailable : Deployment does not have minimum availability.
       Reason:                DispatcherDeploymentFalse
       Status:                False
       Type:                  BackingChannelReady
       Last Transition Time:  2021-11-29T08:17:43Z
       Message:               No dead letter sink is configured.
       Reason:                DeadLetterSinkNotConfigured
       Status:                True
       Type:                  DeadLetterSinkResolved
       Last Transition Time:  2021-11-29T08:43:06Z
       Message:               The status of Dispatcher Deployment is False: MinimumReplicasUnavailable : Deployment does not have minimum availability.
       Reason:                DispatcherDeploymentFalse
       Status:                False
       Type:                  Ready
     Observed Generation:     1
   Events:                    <none>
   
   # 调查发现是channel对应的dispatcher没有创建，去github的源码搜索dispatcher，发现这个文件https://github.com/knative-sandbox/eventing-kafka/blob/2b185a9b59347cc4a16471ed0fb2ba3bdf6e1672/config/channel/consolidated/201-clusterrolebinding.yaml
   
   $ kubectl apply -f 201-clusterrolebinding.yaml
   $ kubectl describe  channel.messaging.knative.dev kafka-channel -n kn-event-test
   Name:         kafka-channel
   Namespace:    kn-event-test
   Labels:       <none>
   Annotations:  messaging.knative.dev/creator: kubernetes-admin
                 messaging.knative.dev/lastModifier: kubernetes-admin
                 messaging.knative.dev/subscribable: v1
   API Version:  messaging.knative.dev/v1
   Kind:         Channel
   Metadata:
     Creation Timestamp:  2021-11-29T08:17:47Z
     Generation:          1
     Managed Fields:
       API Version:  messaging.knative.dev/v1
       Fields Type:  FieldsV1
       fieldsV1:
         f:metadata:
           f:annotations:
             .:
             f:kubectl.kubernetes.io/last-applied-configuration:
       Manager:      kubectl-client-side-apply
       Operation:    Update
       Time:         2021-11-29T08:17:47Z
       API Version:  messaging.knative.dev/v1
       Fields Type:  FieldsV1
       fieldsV1:
         f:status:
           f:conditions:
       Manager:         controller
       Operation:       Update
       Time:            2021-11-29T09:19:09Z
     Resource Version:  13559612
     UID:               178da22e-804c-406d-b769-48d8254302ed
   Spec:
     Channel Template:
       API Version:  messaging.knative.dev/v1beta1
       Kind:         KafkaChannel
   Status:
     Address:
       URL:  http://kafka-channel-kn-channel.kn-event-test.svc.cluster.local
     Channel:
       API Version:  messaging.knative.dev/v1beta1
       Kind:         KafkaChannel
       Name:         kafka-channel
       Namespace:    kn-event-test
     Conditions:
       Last Transition Time:  2021-11-29T08:42:54Z
       Status:                True
       Type:                  Addressable
       Last Transition Time:  2021-11-29T09:19:05Z
       Status:                True
       Type:                  BackingChannelReady
       Last Transition Time:  2021-11-29T08:17:43Z
       Message:               No dead letter sink is configured.
       Reason:                DeadLetterSinkNotConfigured
       Status:                True
       Type:                  DeadLetterSinkResolved
       Last Transition Time:  2021-11-29T09:19:05Z
       Status:                True
       Type:                  Ready
     Observed Generation:     1
   Events:                    <none>
   ```

   