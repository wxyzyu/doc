## Install the Knative Serving component[¶](https://knative.dev/docs/install/serving/install-serving-with-yaml/#install-the-knative-serving-component)

To install the Knative Serving component:

1. Install the required custom resources by running the command:

   ```shell
   $ kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.0.0/serving-crds.yaml
   ```

2. Install the core components of Knative Serving by running the command:

   ```shell
   $ kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.0.0/serving-core.yaml
   ```

   Info

   For information about the YAML files in Knative Serving, see [Knative Serving installation files](https://knative.dev/docs/install/serving/serving-installation-files/).

## Install a networking layer[¶](https://knative.dev/docs/install/serving/install-serving-with-yaml/#install-a-networking-layer)

The following commands install Istio and enable its Knative integration.

1. Install a properly configured Istio by following the [Advanced Istio installation](https://knative.dev/docs/install/serving/installing-istio/) instructions or by running the command:

   ```shell
   $ kubectl apply -l knative.dev/crd-install=true -f https://github.com/knative/net-istio/releases/download/knative-v1.0.0/istio.yaml
   $ kubectl apply -f https://github.com/knative/net-istio/releases/download/knative-v1.0.0/istio.yaml
   ```

2. Install the Knative Istio controller by running the command:

   ```shell
   $ kubectl apply -f https://github.com/knative/net-istio/releases/download/knative-v1.0.0/net-istio.yaml
   ```

3. Fetch the External IP address or CNAME by running the command:

   ```shell
   $ kubectl --namespace istio-system get service istio-ingressgateway
   ```

   Tip

   Save this to use in the following [Configure DNS](https://knative.dev/docs/install/serving/install-serving-with-yaml/#configure-dns) section.

## Verify the installation[¶](https://knative.dev/docs/install/serving/install-serving-with-yaml/#verify-the-installation)

Success

Monitor the Knative components until all of the components show a `STATUS` of `Running` or `Completed`. You can do this by running the following command and inspecting the output:

```shell
$ kubectl get pods -n knative-serving
```

Example output:

```shell
NAME                                      READY   STATUS    RESTARTS   AGE
3scale-kourier-control-54cc54cc58-mmdgq   1/1     Running   0          81s
activator-67656dcbbb-8mftq                1/1     Running   0          97s
autoscaler-df6856b64-5h4lc                1/1     Running   0          97s
controller-788796f49d-4x6pm               1/1     Running   0          97s
domain-mapping-65f58c79dc-9cw6d           1/1     Running   0          97s
domainmapping-webhook-cc646465c-jnwbz     1/1     Running   0          97s
webhook-859796bc7-8n5g2                   1/1     Running   0          96s
```



## Configure DNS[¶](https://knative.dev/docs/install/serving/install-serving-with-yaml/#configure-dns)

Knative provides a Kubernetes Job called `default-domain` that configures Knative Serving to use [sslip.io](http://sslip.io/) as the default DNS suffix.

```shell
$ kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.0.0/serving-default-domain.yaml
```

Warning

This will only work if the cluster `LoadBalancer` Service exposes an IPv4 address or hostname, so it will not work with IPv6 clusters or local setups like minikube unless [`minikube tunnel`](https://minikube.sigs.k8s.io/docs/commands/tunnel/) is running.

In these cases, see the "Real DNS" or "Temporary DNS" tabs.



## Install optional Serving extensions[¶](https://knative.dev/docs/install/serving/install-serving-with-yaml/#install-optional-serving-extensions)

TLS with cert-manager

1. Install [cert-manager version v1.0.0 or later](https://knative.dev/docs/install/serving/installing-cert-manager/).

   ```shell
   $ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
   ```

2. Install the component that integrates Knative with `cert-manager` by running the command:

   ```shell
   $ kubectl apply -f https://github.com/knative/net-certmanager/releases/download/knative-v1.0.0/release.yaml
   ```

3. Configure Knative to automatically configure TLS certificates by following the steps in [Enabling automatic TLS certificate provisioning](https://knative.dev/docs/serving/using-auto-tls/).

