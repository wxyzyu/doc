## 前提条件

- 已经安装Knative Eventing
- 已经使用Strimzi安装了Kafka
- 已经安装了Knative Kafka Broker、Knative Kafka Sink、Knative Kafka Broker

## 安装

1. 整体流程图

   ![image-20211129114509948](003%20Knative%20Source%20Sink%20Broker%20Trigger.assets/image-20211129114509948.png)

   

2. 创建namespace

   ```shell
   $ kubectl create namespace kn-event-test
   namespace/kn-event-test created
   ```

3. 部署event-display服务

   ```shell
   $ vi event-display.yaml
   ```
   
   ```yaml
   apiVersion: serving.knative.dev/v1
   kind: Service
   metadata:
     name: event-display
     namespace: kn-event-test
   spec:
     template:
       spec:
         containers:
           - image: gcr.io/knative-releases/knative.dev/eventing/cmd/event_display@sha256:1d6083a88816f6b4ed924c2640683ca6b0b994ec18e9935a3368779ec37dacf6
             ports:
               - containerPort: 8080
   ```
   
   ```shell
   $ kubectl apply -f event-display.yaml
   $ kn service list -n kn-event-test
   NAME            URL                                                          LATEST                AGE   CONDITIONS   READY   REASON
   event-display   http://event-display.kn-event-test.192.168.30.200.sslip.io   event-display-00001   35s   3 OK / 3     True
   ```
   
4. 部署Kafka broker

   ```shell
   # Kafaka broker 安装参考002 Install Knative Eventing
   $ vi default-broker.yaml
   ```

   ````yaml
   apiVersion: eventing.knative.dev/v1
   kind: Broker
   metadata:
     annotations:
       # case-sensitive
       eventing.knative.dev/broker.class: Kafka
     name: default
     namespace: kn-event-test
   spec:
     # Configuration specific to this broker.
     config:
       apiVersion: v1
       kind: ConfigMap
       name: kafka-broker-config
       namespace: knative-eventing
   ````

   ```shell
   $ kubectl apply -f default-broker.yaml
   broker.eventing.knative.dev/default created
   $ kn broker list -n kn-event-test
   NAME      URL                                                                                    AGE   CONDITIONS   READY   REASON
   default   http://kafka-broker-ingress.knative-eventing.svc.cluster.local/kn-event-test/default   68s   6 OK / 6     True
   ```

5. 配置trigger

   ```shell
   $ vi trigger.yaml
   ```
   
   ```yaml
   apiVersion: eventing.knative.dev/v1
   kind: Trigger
   metadata:
     name: default-broker-trigger
     namespace: kn-event-test
     annotations:
        kafka.eventing.knative.dev/delivery.order: unordered
   spec:
     broker: default
     subscriber:
       ref:
         apiVersion: serving.knative.dev/v1
         kind: Service
         name: event-display
         namespace: kn-event-test
   ```
   
   ```shell
   $ kn trigger list -n kn-event-test
   NAME                     BROKER    SINK                 AGE   CONDITIONS   READY   REASON
   default-broker-trigger   default   ksvc:event-display   15s   6 OK / 6     True
   
   ```
   
6. 部署Kafka source

   ```shell
   # Kafaka source 安装参考002 Install Knative Eventing
   # 首先创建Kafka的Topic
   $ vi kafka-topic-1.yaml
   ```
   
   ```yaml
   apiVersion: kafka.strimzi.io/v1beta2
   kind: KafkaTopic
   metadata:
     name: kn-event-test-1
     namespace: kafka
     labels:
       strimzi.io/cluster: my-cluster
   spec:
     partitions: 3
     replicas: 1
     config:
       retention.ms: 7200000
       segment.bytes: 1073741824
   ```
   
   ```shell
   $ kubectl apply -f kafka-topic-1.yaml
   kafkatopic.kafka.strimzi.io/kn-event-test-1 created
   $ kubectl get kafkatopic.kafka.strimzi.io kn-event-test-1 -n kafka
   NAME              CLUSTER      PARTITIONS   REPLICATION FACTOR   READY
   kn-event-test-1   my-cluster   3            1                    True
   
   $ vi event-source.yaml
   ```
   
   ```yaml
   apiVersion: sources.knative.dev/v1beta1
   kind: KafkaSource
   metadata:
     name: kafka-source
     namespace: kn-event-test
   spec:
     consumerGroup: knative-group
     bootstrapServers:
     - my-cluster-kafka-bootstrap.kafka:9092 # note the kafka namespace
     topics:
     - kn-event-test-1
     sink:
       ref:
         apiVersion: eventing.knative.dev/v1
         kind: Broker
         name: default
         namespace: kn-event-test
   ```
   
   ```shell
   $ kn source list -n kn-event-test
   NAME           TYPE          RESOURCE                           SINK             READY
   kafka-source   KafkaSource   kafkasources.sources.knative.dev   broker:default   True
   
   $ kubectl get kafkasource.sources.knative.dev kafka-source -n kn-event-test
   NAME           TOPICS                BOOTSTRAPSERVERS                            READY   REASON   AGE
   kafka-source   ["kn-event-test-1"]   ["my-cluster-kafka-bootstrap.kafka:9092"]   True             4m29s
   
   $ kubectl -n kafka run kafka-producer -ti --image=quay.io/strimzi/kafka:0.26.0-kafka-3.0.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list my-cluster-kafka-bootstrap:9092 --topic kn-event-test-1
   
   If you don't see a command prompt, try pressing enter.
   >{"msg": "This is a Kafka source test!"}
   
   $ kubectl describe service event-display -n kn-event-test
   Name:              event-display
   Namespace:         kn-event-test
   Labels:            serving.knative.dev/route=event-display
                      serving.knative.dev/service=event-display
   Annotations:       serving.knative.dev/creator: kubernetes-admin
                      serving.knative.dev/lastModifier: kubernetes-admin
   Selector:          <none>
   Type:              ExternalName
   IP Families:       <none>
   IP:
   IPs:               <none>
   External Name:     knative-local-gateway.istio-system.svc.cluster.local
   Port:              http2  80/TCP
   TargetPort:        80/TCP
   Endpoints:         <none>
   Session Affinity:  None
   Events:            <none>
   
   
   $ kubectl logs --tail=100  --selector='serving.knative.dev/service=event-display' -c user-container -n kn-event-test
   ☁️  cloudevents.Event
   Context Attributes,
     specversion: 1.0
     type: dev.knative.kafka.event
     source: /apis/v1/namespaces/kn-event-test/kafkasources/kafka-source#kn-event-test-1
     subject: partition:1#0
     id: partition:1/offset:0
     time: 2021-11-26T08:12:32.095Z
   Data,
     {"msg": "This is a Kafka source test!"}
   ```
   
   
   
7. 部署Kafka sink

   ```shell
   # Kafaka sink 安装参考002 Install Knative Eventing
   $ vi kafka-sink.yaml
   ```
   
   ```yaml
   apiVersion: eventing.knative.dev/v1alpha1
   kind: KafkaSink
   metadata:
     name: kafka-sink
     namespace: kn-event-test
   spec:
     topic: kn-event-test-1
     bootstrapServers:
      - my-cluster-kafka-bootstrap.kafka:9092
   ```
   
   ```shell
   $ kubectl apply -f kafka-sink.yaml
   kafkasink.eventing.knative.dev/kafka-sink created
   $ kubectl get kafkasink.eventing.knative.dev kafka-sink -n kn-event-test
   NAME         URL                                                                                     AGE     READY   REASON
   kafka-sink   http://kafka-sink-ingress.knative-eventing.svc.cluster.local/kn-event-test/kafka-sink   2m35s   True
   ```
   
8. 部署ping source

   ```shell
   $ kn source ping create test-ping-source \
     --namespace kn-event-test \
     --schedule "*/1 * * * *" \
     --data '{"message": "Hello world from ping source -> kafka sin -> kafka source -> kafka broker -> trigget -> event-display!"}' \
     --sink http://kafka-sink-ingress.knative-eventing.svc.cluster.local/kn-event-test/kafka-sink
   
   
   Ping source 'test-ping-source' created in namespace 'kn-event-test'.
   
   $ kn source list -n kn-event-test
   NAME               TYPE          RESOURCE                           SINK                              READY
   kafka-source       KafkaSource   kafkasources.sources.knative.dev   broker:default                    True
   test-ping-source   PingSource    pingsources.sources.knative.dev    [...]/kn-event-test/kafka-sink    True
   
   $ kubectl logs -f  --selector='serving.knative.dev/service=event-display' -c user-container -n kn-event-test
   ☁️  cloudevents.Event
   Context Attributes,
     specversion: 1.0
     type: dev.knative.sources.ping
     source: /apis/v1/namespaces/kn-event-test/pingsources/test-ping-source
     id: 512eddaf-0343-4773-aace-bf28cb80ab6c
     time: 2021-11-26T08:40:00.120838357Z
   Data,
     {"message": "Hello world from ping source -> kafka sin -> kafka source -> kafka broker -> trigget -> event-display!"}
   ```
