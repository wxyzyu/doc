## 下载源代码

- git clone https://github.com/alibaba/canal.git

## 导入IDEA

![image-20200729164005122](Debug%20Canal.assets/image-20200729164005122.png) 

![image-20200729164119362](Debug%20Canal.assets/image-20200729164119362.png) 

## 添加canal server调试配置

- 在deployer模块中找到startup.sh

![image-20200729164346747](Debug%20Canal.assets/image-20200729164346747.png) 

- 在文件最后找到启动类

```shell
	echo LOG CONFIGURATION : $logback_configurationFile
	echo canal conf : $canal_conf 
	echo CLASSPATH :$CLASSPATH
	$JAVA $JAVA_OPTS $JAVA_DEBUG_OPT $CANAL_OPTS -classpath .:$CLASSPATH com.alibaba.otter.canal.deployer.CanalLauncher 1>>$base/logs/canal/canal_stdout.log 2>&1 &
	echo $! > $base/bin/canal.pid 
```

![image-20200729165150668](Debug%20Canal.assets/image-20200729165150668.png) 

- 按照上图配置，VM options复制startup.sh中的

```shell
-server -Xms2048m -Xmx3072m -Xmn1024m -XX:SurvivorRatio=2 -XX:PermSize=96m -XX:MaxPermSize=256m -Xss256k -XX:-UseAdaptiveSizePolicy -XX:MaxTenuringThreshold=15 -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:+HeapDumpOnOutOfMemoryError -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Dfile.encoding=UTF-8 -DappName=otter-canal
```

## 配置canal server

- 修改canal.properties

```
# 修改server 配置文件目录，默认值../conf 在debug的时候会找不到配置文件
canal.conf.dir = E:/projects/conf
```

- 修改instance.properties，并将修改完的文件放到{canal.conf.dir/example}，否则调试无法启动，参见SpringInstanceConfigMonitor.scan()

![image-20200729170227362](Debug%20Canal.assets/image-20200729170227362.png) 

```properties
# position info
canal.instance.master.address=192.168.8.11:3306

...

# username/password
canal.instance.dbUsername=usr_replica
canal.instance.dbPassword=password
canal.instance.defaultDatabaseName =json_test
canal.instance.connectionCharset = UTF-8
```

- 修改logback.xml

```xml
<logger name="com.alibaba.otter.canal.instance" additivity="false">
    <level value="INFO" />
    <appender-ref ref="CANAL-ROOT" />
    <appender-ref ref="STDOUT" />
</logger>
<logger name="com.alibaba.otter.canal.deployer" additivity="false">
    <level value="INFO" />
    <appender-ref ref="CANAL-ROOT" />
    <appender-ref ref="STDOUT" />
</logger>
<logger name="com.alibaba.otter.canal.meta.FileMixedMetaManager" additivity="false">
    <level value="INFO" />
    <appender-ref ref="CANAL-META" />
    <appender-ref ref="STDOUT" />
</logger>
<logger name="com.alibaba.otter.canal.connector.kafka" additivity="false">
    <level value="INFO" />
    <appender-ref ref="CANAL-ROOT" />
    <appender-ref ref="STDOUT" />
</logger>
<logger name="com.alibaba.otter.canal.connector.rocketmq" additivity="false">
    <level value="INFO" />
    <appender-ref ref="CANAL-ROOT" />
    <appender-ref ref="STDOUT" />
</logger>
<logger name="com.alibaba.otter.canal.connector.rabbitmq" additivity="false">
    <level value="INFO" />
    <appender-ref ref="CANAL-ROOT" />
    <appender-ref ref="STDOUT" />
</logger>
<root level="INFO">
    <appender-ref ref="CANAL-ROOT" />
    <appender-ref ref="STDOUT" />
</root>
```

## 添加canal adapter调试配置

- 在client-adapter模块找到startup.sh

![image-20200729171426393](Debug%20Canal.assets/image-20200729171426393.png) 

- 在文件最后找到启动类

```shell
echo CLASSPATH :$CLASSPATH
$JAVA $JAVA_OPTS $JAVA_DEBUG_OPT $ADAPTER_OPTS -classpath .:$CLASSPATH com.alibaba.otter.canal.adapter.launcher.CanalAdapterApplication 1>>/dev/null 2>&1 &
echo $! > $base/bin/adapter.pid
```

![image-20200729171256868](Debug%20Canal.assets/image-20200729171256868.png) 

- 按照上图配置启动配置，VM options复制startup.sh中的

```shell
-server -Xms2048m -Xmx3072m -Xmn1024m -XX:SurvivorRatio=2 -XX:PermSize=96m -XX:MaxPermSize=256m -Xss256k -XX:-UseAdaptiveSizePolicy -XX:MaxTenuringThreshold=15 -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:+HeapDumpOnOutOfMemoryError -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Dfile.encoding=UTF-8 -DappName=canal-adapter
```

## 配置canal adapter

![image-20200729171804432](Debug%20Canal.assets/image-20200729171804432.png) 

- 配置mysql 和elasticsearch信息

```properties
server:
  port: 8081
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
    default-property-inclusion: non_null

canal.conf:
  mode: tcp #tcp kafka rocketMQ rabbitMQ
#  flatMessage: true
#  zookeeperHosts:
  syncBatchSize: 1000
  retries: 0
  timeout:
  accessKey:
  secretKey:
  consumerProperties:
    # canal tcp consumer
    canal.tcp.server.host: 127.0.0.1:11111
    canal.tcp.zookeeper.hosts:
    canal.tcp.batch.size: 500
    canal.tcp.username:
    canal.tcp.password:
#    # kafka consumer
#    kafka.bootstrap.servers: 127.0.0.1:9092
#    kafka.enable.auto.commit: false
#    kafka.auto.commit.interval.ms: 1000
#    kafka.auto.offset.reset: latest
#    kafka.request.timeout.ms: 40000
#    kafka.session.timeout.ms: 30000
#    kafka.isolation.level: read_committed
#    kafka.max.poll.records: 1000
#    # rocketMQ consumer
#    rocketmq.namespace:
#    rocketmq.namesrv.addr: 127.0.0.1:9876
#    rocketmq.batch.size: 1000
#    rocketmq.enable.message.trace: false
#    rocketmq.customized.trace.topic:
#    rocketmq.access.channel:
#    rocketmq.subscribe.filter:
#    # rabbitMQ consumer
#    rabbitmq.host:
#    rabbitmq.virtual.host:
#    rabbitmq.username:
#    rabbitmq.password:
#    rabbitmq.resource.ownerId:

  srcDataSources:
    defaultDS:
      url: jdbc:mysql://192.168.8.11:3306/json_test?useUnicode=true
      username: usr_replica
      password: password
  canalAdapters:
  - instance: example # canal instance Name or mq topic name
    groups:
    - groupId: g1
      outerAdapters:
      - name: logger
#      - name: rdb
#        key: mysql1
#        properties:
#          jdbc.driverClassName: com.mysql.jdbc.Driver
#          jdbc.url: jdbc:mysql://127.0.0.1:3306/mytest2?useUnicode=true
#          jdbc.username: root
#          jdbc.password: 121212
#      - name: rdb
#        key: oracle1
#        properties:
#          jdbc.driverClassName: oracle.jdbc.OracleDriver
#          jdbc.url: jdbc:oracle:thin:@localhost:49161:XE
#          jdbc.username: mytest
#          jdbc.password: m121212
#      - name: rdb
#        key: postgres1
#        properties:
#          jdbc.driverClassName: org.postgresql.Driver
#          jdbc.url: jdbc:postgresql://localhost:5432/postgres
#          jdbc.username: postgres
#          jdbc.password: 121212
#          threads: 1
#          commitSize: 3000
#      - name: hbase
#        properties:
#          hbase.zookeeper.quorum: 127.0.0.1
#          hbase.zookeeper.property.clientPort: 2181
#          zookeeper.znode.parent: /hbase
      - name: es7
        hosts: 192.168.8.11:9200 # 127.0.0.1:9200 for rest mode
        properties:
          mode: rest # or rest
          # security.auth: test:123456 #  only used for rest mode
          cluster.name: wxyzyu
#        - name: kudu
#          key: kudu
#          properties:
#            kudu.master.address: 127.0.0.1 # ',' split multi address
```

- 配置elasticsearch7 信息

![image-20200729172112791](Debug%20Canal.assets/image-20200729172112791.png) 

```yaml
dataSourceKey: defaultDS
destination: example
groupId: g1
esMapping:
  _index: inner_alarm
  _id: _id
#  upsert: true
#  pk: id
  sql: "SELECT a.id as _id, a.title, a.content, a.content_search, a.content_version FROM inner_alarm a"
#  objFields:
#    _labels: array:;
# etlCondition: "where a.c_time>={}"
  commitBatch: 3000
```

## 启动canal server

![image-20200729172403373](Debug%20Canal.assets/image-20200729172403373.png) 

![image-20200729183325155](Debug%20Canal.assets/image-20200729183325155.png) 

## 启动canal adapter

- 需要手动把es7的配置文件复制到两个地方，否则无法调试

  ①\canal-master\client-adapter\launcher\target\conf\es7\

  ②\canal-master\client-adapter\launcher\target\classes\es7

![image-20200729184031365](Debug%20Canal.assets/image-20200729184031365.png) 

- 修改mysql中数据,canal adapter输入如下

```shell
2020-07-29 18:40:10.844 [pool-2-thread-1] DEBUG c.a.o.canal.client.adapter.es.core.service.ESSyncService - DML: {"data":[{"id":"-10","title":"10101010","create_time":0,"create_type":null,"create_user_id":null,"create_source_id":null,"ip":null,"content":null,"content_search":null,"content_version":null,"first_discover_time":0,"last_discover_time":0,"type":null,"status":null,"inner_status":0,"device_id":null,"software_id":null,"remark":null,"is_delete":0,"confirm_time":null,"conform_user_id":null,"ignore_time":null,"ignore_user_id":null,"handle_time":null,"handle_user_id":null,"is_expired":null,"version":null,"update_time":null,"update_user_id":null,"finish_time":null,"finish_user_id":null,"reopen_time":null,"reopen_type":null,"reopen_user_id":null,"reopen_source_id":null}],"database":"json_test","destination":"example","es":1596019210000,"groupId":"g1","isDdl":false,"old":null,"pkNames":["id"],"sql":"","table":"inner_alarm","ts":1596019210843,"type":"INSERT"} 
Affected indexes: inner_alarm
```

- 查看elasticsearch中的数据

```shell
GET inner_alarm/_doc/-10
{
  "_index" : "inner_alarm",
  "_type" : "_doc",
  "_id" : "-10",
  "_version" : 1,
  "_seq_no" : 6,
  "_primary_term" : 6,
  "found" : true,
  "_source" : {
    "title" : "10101010",
    "content" : null,
    "content_search" : null,
    "content_version" : null
  }
}
```

## 总结

- 参考https://github.com/alibaba/canal/wiki/DevGuide

![img](Debug%20Canal.assets/687474703a2f2f646c2e69746579652e636f6d2f75706c6f61642f6174746163686d656e742f303038322f353138372f63383936663831632d623563642d336164362d383034362d3436346664333865346436632e6a7067) 

![image-20200729184834532](Debug%20Canal.assets/image-20200729184834532.png) 

工程中的每个模块和第一张图的模块划分一一对应。入口点可以从startup.sh中的启动类开始，如果想看某一个具体步骤，比如解析mysql bin log，可以查看parse模块

![image-20200729190627247](Debug%20Canal.assets/image-20200729190627247.png) 